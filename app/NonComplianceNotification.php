<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NonComplianceNotification extends Model {

	protected $table = 'non_compliance_notification';
	protected $fillable = ['user_code'];
}
