<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cumulative extends Model
{
    //
     protected $table = 'cumulative_record';
     protected $primaryKey = 'cum_id';
}
