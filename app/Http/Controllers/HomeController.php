<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }
    public function announcement()
    {
        if(Auth::user()->status == 'professor'){
            return view('referral');
        }

        if(Auth::user()->status == 'admin' ){
            return view('student');
        }
        return view('announcement');
    }
    public function cumulative()
    {

        return view('cumulative', ['user' => Auth::user()]);
    }

    public function cumulativeWithID($id)
    {

        return view('cumulative', ['user' => User::where('code',$id)->first()]);
    }


    public function inbox()
    {
        return view('inbox');
    }

    public function changePass()
    {
        return view('changePass');
    }

    public function editAccount()
    {
        return view('editAccount');
    }

    public function account($id)
    {

        if($id == 1){
        return view('changePass');
        }
        else if($id == 2){
        return view('addAccount',['isSuccessful' => false]);
        }
        else{
        return view('modifyInfrastructure');
        }
    }
     
    public function about()
    {
        return view('about');
    }

    public function referral()
    {
        return view('referral');
    }

    public function college()
    {
        return view('college');
    }

    public function course()
    {
        return view('course');
    }

    public function cumulativeCriteria()
    {
        return view('cumulativeCriteria');
    }


    public function studentProfiling()
    {
        return view('report.studentProfiling');
    }

    public function nonCompliance()
    {
        return view('report.nonCompliance');
    }

    public function commonProb()
    {
        return view('report.commonProb');
    }

    public function frequentlyReferred()
    {
        return view('report.frequentlyReferred');
    }

    public function student()
    {
        return view('student');
    }
    public function calendar()
    {
        return view('mycalendar');
    }

}
