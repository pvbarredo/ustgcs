<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\User;
use App\Student;
use App\Guidance_councelor;

class StudentController extends Controller
{

    public function getAllStudents(){
        $student = User::where('is_active', 1)
                    ->where('status', 'student')
                    ->join('student', 'users.code', '=', 'student.code')
                    ->join('colleges', 'student.college_id', '=', 'colleges.id')
                    ->join('courses', 'student.course_id', '=', 'courses.id')
                    ->select('users.*', 'colleges.name as college_name', 'courses.name as course_name', 'student.*');
                    

        if(Auth::user()->status === 'guidance'){
            $guidance = Guidance_councelor::where('code',Auth::user()->code)->first();

            $student->where('student.college_id', $guidance->college_id);
        }
        
        return $student->get();            
    }

    public function softDeleteStudent($id){
        User::where('is_active', 1)
            ->where('code', $id)
            ->update(['is_active' => 0]);

        return "Success deletion";
    }
}
