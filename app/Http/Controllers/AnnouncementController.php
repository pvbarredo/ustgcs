<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Announcement;
use Auth;
use App\Student;
use App\Professor;
use App\Announcement_flag;
use Response;
use App\User;
use App\Announcement_permission;
use App\Notification_badge;
use App\Guidance_councelor;
use App\Inbox;

class AnnouncementController extends Controller
{
    
    public function checker($data , $dataToCompare){
        $dataArray = explode(",", $data);
        $isMatch = false;
        foreach ($dataArray as $datas) {
            if($datas == $dataToCompare){
                $isMatch = true;
                break;
            }
        }

        if($isMatch){
            return true;
        }else{
            return false;
        }
        
    }

    public function getAllAnnouncementForUser()
    {
        
        $user = Auth::user();
    	$user_status = $user->status;

    	//if user is a student
    	if($user_status == "student"){
    		$student = Student::where('code', $user->code)->first();
    		
            $year = $student->year;
            $college_id = $student->college_id;
            $course_id = $student->course_id;

            // $announcements = Announcement::orderBy('created_at', 'desc')->get();
            $final_announcement_permissions = [];

             $announcements_permissions = Announcement_permission::orderBy('created_at', 'desc')->get();
            foreach ($announcements_permissions as $anc_perm) {

                //check if all first
                if($anc_perm->is_all){
                    array_push($final_announcement_permissions,$anc_perm);
                    continue;
                }else{

                    //theres always a year
                    $check_year = $this->checker($anc_perm->year, $year);

                    if(!$check_year){
                        continue;
                    }

                    if( !($anc_perm->college) && !($anc_perm->course)){

                        //if course and college is null , considered. all of the year
                        array_push($final_announcement_permissions,$anc_perm);
                        continue;
                    }

                    //if it reach here, it means the year is valid
                    $check_college = $this->checker($anc_perm->college,$college_id);

                    //check course
                    $check_course = $this->checker($anc_perm->course,$course_id);

                    if($check_college && $check_course){
                        //automatic lagay agad sa final
                        array_push($final_announcement_permissions,$anc_perm);
                        continue;
                    }

                    

                    // if($check_course){
                    //     array_push($final_announcement_permissions,$anc_perm);
                    //     continue;
                    // }
                }
                
            }



            //get announcement details
            $announcements = [];
            foreach ($final_announcement_permissions as $anc_perm) {
                $announcement  = Announcement::find($anc_perm->announcement_id);
                array_push($announcements,$announcement);
            }



            $flag = [];
            $flag_count = 0;
            
            foreach ($announcements as $announcement) {
                $id = $announcement->id;
                $value = (Announcement_flag::where('announcement_id', $id)->where('user_id', $user->code)->first()) ? 1:0;
                $created_by = User::where('code', $announcement->created_by)->first();


                $announcement = [
                    "id" => $id,
                    "value" => $value,
                    "created_by" => strtoupper($created_by->status) . " : " . $created_by->firstname . " " .$created_by->lastname 
                ];
                array_push($flag,$announcement);
                if(!$value){
                    $flag_count++;
                }
                
            }

            $response = [
                'announcements' => $announcements,
                'total_unread' => $flag_count,
                'flag' => $flag
            ];

            $inbox_count = Inbox::where('is_read', 0)
                                ->where('student_id', Auth::user()->code)
                                ->count();

             
            $notification = Notification_badge::where("user_code", Auth::user()->code)->first();
            if($notification){
                $notification->announcement = $flag_count;
                $notification->inbox = $inbox_count;
                $notification->save();

            }else{
                $notification = new Notification_badge;
                $notification->user_code = Auth::user()->code;
                $notification->announcement = $flag_count;
                $notification->inbox = $inbox_count; 
                $notification->save();
            }


			return Response::json($response);
    	}
    	//if the user is a professor
    	else if($user_status == "professor"){
    		
         $final_announcement_permissions = [];

         $announcements_permissions = Announcement_permission::orderBy('created_at', 'desc')->get();

           foreach ($announcements_permissions as $anc_perm) {
                if($anc_perm->is_all){
                    array_push($final_announcement_permissions,$anc_perm);
                    continue;
                }else{
                    if($anc_perm->professor){
                        array_push($final_announcement_permissions,$anc_perm);
                        continue;
                    }
                }
           }
            
            $announcements = [];
           foreach ($final_announcement_permissions as $anc_perm) {
                $announcement  = Announcement::find($anc_perm->announcement_id);
                array_push($announcements,$announcement);
            }


            $flag = [];
            $flag_count = 0;
            
            foreach ($announcements as $announcement) {
                $id = $announcement->id;
                $value = (Announcement_flag::where('announcement_id', $id)->where('user_id', $user->code)->first()) ? 1:0;
                $created_by = User::where('code', $announcement->created_by)->first();
                $announcement = [
                    "id" => $id,
                    "value" => $value,
                    "created_by" => strtoupper($created_by->status) . " : " . $created_by->firstname . " " .$created_by->lastname 
                
                ];
                array_push($flag,$announcement);
                if(!$value){
                    $flag_count++;
                }
                
            }

            $response = [
                'announcements' => $announcements,
                'total_unread' => $flag_count,
                'flag' => $flag

            ];
            $notification = Notification_badge::where("user_code", Auth::user()->code)->first();
            if($notification){
                $notification->announcement = $flag_count;
                $notification->save();

            }else{
                $notification = new Notification_badge;
                $notification->user_code = Auth::user()->code;
                $notification->announcement = $flag_count;
                $notification->save();
            }


    		return Response::json($response);
    	}
    	//if the user is a director
    	else if($user_status == "director"){
    		 $final_announcement_permissions = [];

            $announcements_permissions = Announcement_permission::orderBy('created_at', 'desc')->get();

           foreach ($announcements_permissions as $anc_perm) {
                if($anc_perm->is_all){
                    array_push($final_announcement_permissions,$anc_perm);
                    continue;
                }else{
                    if($anc_perm->director){
                        array_push($final_announcement_permissions,$anc_perm);
                        continue;
                    }
                }
           }
            
            $announcements = [];
           foreach ($final_announcement_permissions as $anc_perm) {
                $announcement  = Announcement::find($anc_perm->announcement_id);
                array_push($announcements,$announcement);
            }
            
            $flag = [];
            $flag_count = 0;
            
            foreach ($announcements as $announcement) {
                $id = $announcement->id;
                $value = (Announcement_flag::where('announcement_id', $id)->where('user_id', $user->code)->first()) ? 1:0;
                $created_by = User::where('code', $announcement->created_by)->first();
                $announcement = [
                    "id" => $id,
                    "value" => $value,
                    "created_by" => strtoupper($created_by->status) . " : " . $created_by->firstname . " " .$created_by->lastname 
                
                ];
                array_push($flag,$announcement);
                if(!$value){
                    $flag_count++;
                }
                
            }

            $response = [
                'announcements' => $announcements,
                'total_unread' => $flag_count,
                'flag' => $flag

            ];

            $notification = Notification_badge::where("user_code", Auth::user()->code)->first();
            if($notification){
                $notification->announcement = $flag_count;
                $notification->save();

            }else{
                $notification = new Notification_badge;
                $notification->user_code = Auth::user()->code;
                $notification->announcement = $flag_count;
                $notification->save();
            }


            return Response::json($response);
    	}
    	//if the user is a guidance
    	else if($user_status == "guidance"){
    		 $final_announcement_permissions = [];

            $announcements_permissions = Announcement_permission::orderBy('created_at', 'desc')->get();

           foreach ($announcements_permissions as $anc_perm) {
                if($anc_perm->is_all){
                    array_push($final_announcement_permissions,$anc_perm);
                    continue;
                }else{
                    if($anc_perm->guidance){
                        array_push($final_announcement_permissions,$anc_perm);
                        continue;
                    }
                }
           }
            
            $announcements = [];
           foreach ($final_announcement_permissions as $anc_perm) {
                $announcement  = Announcement::find($anc_perm->announcement_id);
                $guidance_announcement_creator = Guidance_councelor::where('code', $announcement->created_by)->first();
                if($guidance_announcement_creator){
                    $user_guidance_info = Guidance_councelor::where('code', Auth::user()->code)->first();
                    if($guidance_announcement_creator->college_id == $user_guidance_info->college_id){
                        array_push($announcements,$announcement);
                    }

                }else{
                    array_push($announcements,$announcement);
                }
                
            }
            
            $flag = [];
            $flag_count = 0;
            
            foreach ($announcements as $announcement) {
                $id = $announcement->id;
                $value = (Announcement_flag::where('announcement_id', $id)->where('user_id', $user->code)->first()) ? 1:0;
                $created_by = User::where('code', $announcement->created_by)->first();
                $announcement = [
                    "id" => $id,
                    "value" => $value,
                    "created_by" => strtoupper($created_by->status) . " : " . $created_by->firstname . " " .$created_by->lastname 
                
                ];
                array_push($flag,$announcement);
                if(!$value){
                    $flag_count++;
                }
                
            }

            $response = [
                'announcements' => $announcements,
                'total_unread' => $flag_count,
                'flag' => $flag

            ];

            $notification = Notification_badge::where("user_code", Auth::user()->code)->first();
            if($notification){
                $notification->announcement = $flag_count;
                $notification->save();

            }else{
                $notification = new Notification_badge;
                $notification->user_code = Auth::user()->code;
                $notification->announcement = $flag_count;
                $notification->save();
            }
            return Response::json($response);
    	}

    }
    public function newAnnouncement(Request $request)
    {
	    $announcement = new Announcement;

	    $announcement->title = $request->input('title');
        $announcement->message = $request->input('message');
        $announcement->created_by = Auth::user()->code;

        $announcement->save();


        $announcement_permission = new Announcement_permission;
	    $announcement_permission->announcement_id = $announcement->id;
        $announcement_permission->year = $request->input('year');
        $announcement_permission->college = $request->input('college');
        $announcement_permission->course = $request->input('course');
        $announcement_permission->professor = ($request->input('professor')) ? $request->input('professor') : (Auth::user()->status == 'professor') ? 1:0 ;
        $announcement_permission->guidance = ($request->input('guidance')) ? $request->input('guidance') : (Auth::user()->status == 'guidance') ? 1:0 ;
        $announcement_permission->director = ($request->input('director')) ? $request->input('director') : (Auth::user()->status == 'director') ? 1:0 ;
        $announcement_permission->is_all = $request->input('isCheckedAll');
        $announcement_permission->save();

        $notification = Notification_badge::where("user_code", Auth::user()->code)->first();
            if($notification){
                $notification->announcement = (($notification->announcement) + 1) ;
                $notification->save();

            }else{
                $notification = new Notification_badge;
                $notification->user_code = Auth::user()->code;
                $notification->announcement = 1;
                $notification->save();
            }


	    return "Success creating new Announcement";
    }
   

    public function show($id){

        
        $announcement = Announcement::where('id', $id)->first();
        if (!(Announcement_flag::where('announcement_id', $announcement->id )->where('user_id',Auth::user()->code)->exists())){
            $announcement_flag = new Announcement_flag;
            $announcement_flag->announcement_id = $announcement->id;
            $announcement_flag->user_id = Auth::user()->code;
            $announcement_flag->save();


            $notification = Notification_badge::where("user_code", Auth::user()->code)->first();
            if($notification){
                $notification->announcement = (($notification->announcement) - 1) ;
                $notification->save();

            }
        }
        $created_by = User::where('code', $announcement->created_by)->first();

        

        return view('viewAnnouncement', ['announcement' => $announcement, 'created_by' => strtoupper($created_by->status) . " : " . $created_by->firstname . " " .$created_by->lastname ]);
    }


    public function getNotification(){
        return Notification_badge::where('user_code',Auth::user()->code)->first();
    }

    public function updateInboxNotification($id){
        if(Auth::user()->status == 'student'){
            $notif = Notification_badge::where('user_code', Auth::user()->code)->first();
            $notif->inbox = ($notif->inbox == 0) ? 0 : ($notif->inbox ) - 1  ;
            $notif->save(); 

            $inbox = Inbox::find($id);
            $inbox->is_read = 1;
            $inbox->save();

        }   
        return 'Success';
    }

    
}
