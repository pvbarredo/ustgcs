<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Course;
use App\College;
use Auth;
use Response;
use App\Guidance_councelor;

class CourseController extends Controller
{
    //
    public function getAllCourses($id = null){
        if ($id == null) {
            $courses =  Course::orderBy('id', 'asc')->get();

            $response_courses = [];

            foreach ($courses as $course){
                $college = College::find($course->college_id);

                $course_object = [
                    "id" => $course->id,
                    "name" => $course->name,
                    "max_years" => $course->max_years,
                    "college_name" => $college->name,
                    "college_id" => $college->id

                ];
                array_push($response_courses, $course_object);
            }
            return $response_courses;
        } else {
            return $this->show_courses($id);

        }
    }

    public function getAllCoursesGuidance(){

        $user  = Auth::user();
        $guidance_councelor = Guidance_councelor::where('code', $user->code)->first();
        $courses = Course::where('college_id', $guidance_councelor->college_id)->get();

        return Response::json($courses);


    }

    public function account_showCourses($id){
       $courses = Course::where('college_id', $id)->get();
       $college = College::find($id);
       $response_courses = [];
            foreach ($courses as $course){
                $course_object = [
                    "id" => $course->id,
                    "name" => $course->name,
                    "college_name" => $college->name

                ];
                array_push($response_courses, $course_object);
            }
            return $response_courses;

    }

     public function show_courses($id) {

       // $courses = Course::where('college_id', $id)->get();
       // $response_courses = [];

       //      foreach ($courses as $course){
       //          // $college = College::find($course->college_id);

       //          $course_object = [
       //              "id" => $course->id,
       //              "name" => $course->name
                    
       //              // "college_name" => $college->name

       //          ];
       //          array_push($response_courses, $course_object);
       //      }
            // return $response_courses;

        // return Course::find($id);
        //peter change
        $response_courses = [];
        $course = Course::find($id);
        $college = College::find($course->college_id);
        $course_object = [
                    "id" => $course->id,
                    "name" => $course->name,
                    "max_years" => $course->max_years,
                    "college_name" => $college->name,
                    "college_id" => $college->id

                ];
         array_push($response_courses, $course_object);
         return $response_courses;

    }

    public function show_courseYear($id){
        $course = Course::find($id);

        $response_course =[
            "max_years" => $course->max_years
        ];

        return $response_course;
    }


    public function store_course(Request $request) {

    	$course = new Course;

    	$course->name = $request->input('name');
        $course->max_years = $request->input('max_years');
    	$course->college_id = $request->input('college_id');

    	$course->save();

    }

    public function update_course(Request $request, $id) {

    	$course = Course::find($id);
        var_dump($request->input('max_years'));
    	$course->name = $request->input('name');
        $course->max_years = $request->input('max_years');
    	$course->college_id = $request->input('college_id');

    	$course->save();



    }

    public function delete_course(Request $request, $id) {
        $course = Course::find($id);

        $course->delete();

    }
}
