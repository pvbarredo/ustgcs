<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App;
use PDF;
use Lava;
use App\User;
use App\Cumulative;
use App\Guidance_councelor;
use Auth;
use App\Referral;
use DB;
use App\NonComplianceNotification;


class ReportController extends Controller
{
     public function generatePDF()
	 {
	       // return PDF::loadFile('http://www.github.com')->inline('github.pdf');
	 	 //   	$pdf = App::make('snappy.pdf.wrapper');
			// $pdf->loadHTML('<h1>Test</h1>');
			// return $pdf->inline();
			$pdf = PDF::loadView('announcement');
			return $pdf->inline();
	 }

	 public function sendNotification(Request $request){

	 	$data = $request->input('students');

	 	foreach ($data as $value) {
	 		$nonComplianceNotification = NonComplianceNotification::firstOrCreate(['user_code' => $value['code']]);
	 		$nonComplianceNotification->save();
	 	}
	 	
	 }
	 public function deleteNonComplianceNotificationRecord($code){
	 	$nonComplianceNotification = NonComplianceNotification::where('user_code', $code);
	 	$nonComplianceNotification->delete();

	 	return 'Success deleting record';
	 }

	 public function checkNonComplianceNotification($code){
	 	return NonComplianceNotification::where('user_code', $code)->exists() ? 'true': 'false' ; 
	 }

	 public function generateNonComplianceReport()
	 {
	 	

	 	//get all student from the user table
	 	$students = User::where('status', 'student')
	 				->where('is_active', 1)
	 				->join('student', 'users.code', '=', 'student.code')
	 				->join('colleges', 'student.college_id', '=', 'colleges.id')
	 				->join('courses', 'student.course_id', '=', 'courses.id')
	 				->select('users.*', 'colleges.name as college_name', 'courses.name as course_name', 'student.*')				
	 				->orderBy('student.year', 'asc')
	 				->orderBy('student.college_id', 'asc')
	 				->orderBy('student.course_id', 'asc')
	 				->get();
	 				
	 				



	 	// $distinct_years = $students
	 	// 				->select('student.year')				
	 	// 				->groupBy('student.year')
	 	// 				->get();
	 	

	 	// $distinct_college_per_year = $students
	 	// 				->select('student.college_id')				
	 	// 				->groupBy('student.college_id')
	 	// 				->get();

	 	// dd($distinct_college_per_year);

	 				// ->join('cumulative_record', 'users.code', '=', 'cumulative_record.student_id')
	 				// ->select('users.*', 'colleges.name as college_name', 'courses.name as course_name', 'student.*')				
	 				// ->orderBy('student.year', 'asc')
	 				// ->orderBy('student.college_id', 'asc')
	 				// ->orderBy('student.course_id', 'asc')
	 				// ->get();

	 	//check if they have record in cumulative
// ->groupBy('status')
	 	// dd($students);
	 	$nonCompliantStudents = [];
	 	foreach ($students as $student) {
	 		$cumulative = Cumulative::where('student_id',$student->code)->first();

	 		if($cumulative){
	 			//put code in the future
	 		}else{
	 			array_push($nonCompliantStudents,$student);
	 		}

	 	}

	 	return $nonCompliantStudents;

	 }
	 public function studentProfiling(Request $request)
	 {
	 	$students = $this->filterStudent($request);
	 	return $students;
	 }

	 public function studentProfiling1(Request $request)
	 {
	 	
	 	//display filter
	 	$displayGender = false;
	 	$displayReligion = false;
	 	$displayCity = false;
	 	$displayCivil = false;
	 	$displayLivingArrangements = false;
	 	$displayHealths = false;
	 	$displayAssessments = false;
	 	$displayCounselings= false;
	 	$displayMaritals = false;
	 	$displayAbroads = false;
	 	$displayDeceased = false;
	 	$displayMotivation = false;
	 	$displayHelp = false;




	 	$datas = $request->input('data');
	 	if($datas){
	 		foreach ($datas as $key => $value) {
	 		if($key == 'gender'){
		 			$displayGender = true;
		 		}

		 		if($key == 'religion'){
		 			$displayReligion = true;
		 		}

		 		if($key == 'civil'){
		 			$displayCivil = true;
		 		}

		 		if($key == 'city'){
		 			$displayCity = true;
		 		}

		 		if($key == 'living'){
		 			$displayLivingArrangements = true;
		 		}
		 	}
	 	}
	 	

	 	$health_infos =  $request->input('health');
	 	 
	 	if($health_infos){
	 		foreach ($health_infos as $key => $value) {
	 		if($key == 'limitation'){
		 			$displayHealths = true;
		 		}
		 		if($key == 'assessment'){
		 			$displayAssessments = true;
		 		}
		 		if($key == 'counseling'){
		 			$displayCounselings= true;
		 		}
		 	}
	 	}
	 	

	 	$family =  $request->input('family');
	 	if($family){
	 		foreach ($family as $key => $value) {
	 		if($key == 'maritalStatus'){
		 			$displayMaritals = true;
		 		}
		 		if($key == 'abroad'){
		 			$displayAbroads = true;
		 		}
		 		if($key == 'deceased'){
		 			$displayDeceased= true;
		 		}
		 		if($key == 'motivation'){
		 			$displayMotivation= true;
		 		}
		 		if($key == 'help'){
		 			$displayHelp= true;
		 		}
		 	}
	 	}

		

	 	$displays = [
	 				'displayGender' => $displayGender, 
	 				'displayReligion' => $displayReligion,
	 				'displayCity' => $displayCity,
	 				'displayLivingArrangements' => $displayLivingArrangements,
	 				'displayHealths' => $displayHealths,
	 				'displayAssessments' => $displayAssessments,
	 				'displayCounselings' => $displayCounselings,
	 				'displayMaritals' => $displayMaritals,
	 				'displayAbroads' => $displayAbroads,
	 				'displayDeceased' => $displayDeceased,
	 				'displayMotivation' => $displayMotivation,
	 				'displayHelp' => $displayHelp,

	 				 ];


	 	// dd($request->all());
	 	$students = $this->filterStudent($request);
	    
	    
	    
	    $males = 0;
	    $females = 0;
	    $religions = [];
	    $city = [];
	    $city_curr = [];

	    $living_arrangements = [];
	    $healths = [];
	    $assessments = [];
	    $counselings = [];
	    $maritals = [];
	    $abroads = [];
	    $deceased = [];
	    $motivations = [];
	    $reasons = [];

	    foreach ($students as $student) {
	    	if($student->gender == 'Male'){
	    		$males++;
	    	}
	    	if($student->gender == 'Female'){
	    		$females++;
	    	}

	    	array_push($religions, strtoupper($student->religion) );
	    	array_push($city, strtoupper($student->perm_city) );
	    	array_push($city_curr, strtoupper($student->curr_city) );


	    	array_push($living_arrangements, strtoupper($student->living_arrangement) );
	    	array_push($healths, strtoupper($student->acad_health_prob) );


	    	array_push($assessments, strtoupper($student->psychiatric_help) );
	    	array_push($counselings,strtoupper($student->counseling) );

	    	array_push($maritals,strtoupper($student->parent_marital_status) );
	    	array_push($abroads,strtoupper($student->parent_marital_status_spec1) );
	    	array_push($deceased,strtoupper($student->parent_marital_status_spec2) );
	    	// array_push($motivations,$student->counseling);
	    	// array_push($reasons,$student->counseling);


	    }
	    // dd($city);
	    // dd($living_arrangements);

	    //---------------------GENDER----------------------------------------
	    $genderChart = Lava::DataTable();

		$genderChart->addStringColumn('Gender')
		        ->addNumberColumn('Percent')
		        ->addRow(['Male', $males])
		        ->addRow(['Female', $females]);

		Lava::PieChart('Gender', $genderChart, [
		    'title'  => 'Gender',
		    'is3D'   => true,
		    'slices' => [
		        ['offset' => 0.2],
		        ['offset' => 0.25],
		        ['offset' => 0.3]
		    ]
		]);
		

		//---------------------END OF GENDER----------------------------------------

		//---------------------RELIGION----------------------------------------
		$religionChart = Lava::DataTable();

		$religionChart->addStringColumn('Religion')
						->addNumberColumn('Percent');
		       

		// dd($this->groupArray($religions));
		 foreach ($this->groupArray($religions) as $key => $value) {
		 	$religionChart->addRow([$key, $value]);
		 }

		// Lava::BarChart('Religion', $religionChart);
		Lava::PieChart('Religion', $religionChart, [
		    'title'  => 'Religion',
		    'is3D'   => true,
		    'slices' => [
		        ['offset' => 0.2],
		        ['offset' => 0.25],
		        ['offset' => 0.3]
		    ]
		]);



		//---------------------END RELIGION----------------------------------------



		//---------------------CIVIL STATUS----------------------------------------






		//---------------------CITY----------------------------------------

		$cityChart = Lava::DataTable();

		$cityChart->addStringColumn('City')
						->addNumberColumn('Percent');
		       

		// dd($this->groupArray($city));

		 foreach ($this->groupArray($city) as $key => $value) {
		 	$cityChart->addRow([$key, $value]);
		 }

		// Lava::BarChart('Religion', $religionChart);
		Lava::PieChart('Permanent City', $cityChart, [
		    'title'  => 'Permanent City',
		    'is3D'   => true,
		    'slices' => [
		        ['offset' => 0.2],
		        ['offset' => 0.25],
		        ['offset' => 0.3]
		    ]
		]);

		$curryCityChart = Lava::DataTable();

		$curryCityChart->addStringColumn('City')
						->addNumberColumn('Percent');
		       

		// dd($this->groupArray($city));

		 foreach ($this->groupArray($city_curr) as $key => $value) {
		 	$curryCityChart->addRow([$key, $value]);
		 }

		// Lava::BarChart('Religion', $religionChart);
		Lava::PieChart('Current City', $curryCityChart, [
		    'title'  => 'Current City',
		    'is3D'   => true,
		    'slices' => [
		        ['offset' => 0.2],
		        ['offset' => 0.25],
		        ['offset' => 0.3]
		    ]
		]);


		//---------------------END CITY----------------------------------------
		
		

		//---------------------END CIVIL STATUS----------------------------------------

		//---------------------Living arrangement----------------------------------------
		
		$livingArrangementChart = Lava::DataTable();

		$livingArrangementChart->addStringColumn('Living Arrangement')
						->addNumberColumn('Percent');
		       

		// dd($this->groupArray($city));

		 foreach ($this->groupArray($living_arrangements) as $key => $value) {
		 	$livingArrangementChart->addRow([$key, $value]);
		 }

		// Lava::BarChart('Religion', $religionChart);
		Lava::PieChart('Living Arrangement', $livingArrangementChart, [
		    'title'  => 'Living Arrangement',
		    'is3D'   => true,
		    'slices' => [
		        ['offset' => 0.2],
		        ['offset' => 0.25],
		        ['offset' => 0.3]
		    ]
		]);
		

		//---------------------END Living arrangement----------------------------------------


		//---------------------Health problems----------------------------------------
		
		$healthChart = Lava::DataTable();

		$healthChart->addStringColumn('Health Problems or Physical Limitation')
						->addNumberColumn('Percent');
		       

		// dd($this->groupArray($city));

		 foreach ($this->groupArray($healths) as $key => $value) {
		 	$healthChart->addRow([$key, $value]);
		 }

		// Lava::BarChart('Religion', $religionChart);
		Lava::PieChart('Health Chart', $healthChart, [
		    'title'  => 'Health Problems or Physical Limitation',
		    'is3D'   => true,
		    'slices' => [
		        ['offset' => 0.2],
		        ['offset' => 0.25],
		        ['offset' => 0.3]
		    ]
		]);
		

		//---------------------END Health problems----------------------------------------

		//---------------------ASSESSMENTS----------------------------------------
		
		$assessmentChart = Lava::DataTable();

		$assessmentChart->addStringColumn('Psychiatric assessment or help')
						->addNumberColumn('Percent');
		       

		// dd($this->groupArray($city));

		 foreach ($this->groupArray($assessments) as $key => $value) {
		 	if($key == '0'){
		 		$key = 'No';
		 	}else{
		 		$key = 'Yes';
		 	}

		 	$assessmentChart->addRow([$key, $value]);
		 }

		// Lava::BarChart('Religion', $religionChart);
		Lava::PieChart('Assessment Chart', $assessmentChart, [
		    'title'  => 'Psychiatric assessment or help',
		    'is3D'   => true,
		    'slices' => [
		        ['offset' => 0.2],
		        ['offset' => 0.25],
		        ['offset' => 0.3]
		    ]
		]);
		

		//---------------------END ASSESSMENT----------------------------------------


		//---------------------COUNCELING----------------------------------------
		
		$councelingChart = Lava::DataTable();

		$councelingChart->addStringColumn('Undergone Counseling')
						->addNumberColumn('Percent');
		       

		// dd($this->groupArray($city));

		 foreach ($this->groupArray($counselings) as $key => $value) {
		 	if($key == '0'){
		 		$key = 'No';
		 	}else{
		 		$key = 'Yes';
		 	}
		 	$councelingChart->addRow([$key, $value]);
		 }

		// Lava::BarChart('Religion', $religionChart);
		Lava::PieChart('Counceling Chart', $councelingChart, [
		    'title'  => 'Undergone Counseling',
		    'is3D'   => true,
		    'slices' => [
		        ['offset' => 0.2],
		        ['offset' => 0.25],
		        ['offset' => 0.3]
		    ]
		]);
		

		//---------------------END COUNCELING----------------------------------------

		//---------------------MARTITAL----------------------------------------
		
		$maritalChart = Lava::DataTable();

		$maritalChart->addStringColumn('Marital Status of Parents')
						->addNumberColumn('Percent');
		       

		// dd($this->groupArray($city));

		 foreach ($this->groupArray($maritals) as $key => $value) {
		 	$maritalChart->addRow([$key, $value]);
		 }

		// Lava::BarChart('Religion', $religionChart);
		Lava::PieChart('Marital Chart', $maritalChart, [
		    'title'  => 'Marital Status of Parents',
		    'is3D'   => true,
		    'slices' => [
		        ['offset' => 0.2],
		        ['offset' => 0.25],
		        ['offset' => 0.3]
		    ]
		]);
		

		//---------------------END MARTITAL----------------------------------------


		//---------------------ABROAD----------------------------------------
		
		$abroadChart = Lava::DataTable();

		$abroadChart->addStringColumn('Parents Working Abroad')
						->addNumberColumn('Percent');
		       

		// dd($this->groupArray($city));

		 foreach ($this->groupArray($abroads) as $key => $value) {
		 	$abroadChart->addRow([$key, $value]);
		 }

		// Lava::BarChart('Religion', $religionChart);
		Lava::PieChart('Abroad Chart', $abroadChart, [
		    'title'  => 'Parents Working Abroad',
		    'is3D'   => true,
		    'slices' => [
		        ['offset' => 0.2],
		        ['offset' => 0.25],
		        ['offset' => 0.3]
		    ]
		]);
		

		//---------------------END ABROAD----------------------------------------


		//---------------------DECEASED----------------------------------------
		
		$deceasedChart = Lava::DataTable();

		$deceasedChart->addStringColumn('Deceased Parent')
						->addNumberColumn('Percent');
		       

		// dd($this->groupArray($city));

		 foreach ($this->groupArray($deceased) as $key => $value) {
		 	$deceasedChart->addRow([$key, $value]);
		 }

		// Lava::BarChart('Religion', $religionChart);
		Lava::PieChart('Deceased Chart', $deceasedChart, [
		    'title'  => 'Deceased Parent',
		    'is3D'   => true,
		    'slices' => [
		        ['offset' => 0.2],
		        ['offset' => 0.25],
		        ['offset' => 0.3]
		    ]
		]);
		

		//---------------------END DECEASED----------------------------------------




		// dd($displays);
	    return view('generated.generatedProfiling', ['displays' => $displays]);
	 }


	 public function commonProblem(Request $request){
	 	

	 	$students = $this->filterStudentWithoutCumulative($request);
	 	$student_codes = [];
	 	foreach ($students as $student) {
	 		array_push($student_codes,$student->code);
	 	}
	 	
	 	

	 	$referrals= Referral::whereIn('student_id', $student_codes)
                    ->get();
             // dd($referrals);       
	 	// $referralMessage = [];
	 	// foreach ($referrals as $referral) {
	 	// 	$messages = $referral->message;
	 	// 	$messages = explode(",", $messages);
	 	// 	foreach ($messages as $message) {
	 	// 		array_push($referralMessage,$message);
	 	// 	}

	 	// }

	 // 	$commonProblemChart = Lava::DataTable();

		// $commonProblemChart->addStringColumn('Common Problems')
		// 				->addNumberColumn('Percent');
		       

		// // dd($referralMessage);
		// // dd($this->groupArray($referralMessage));
		//  foreach ($this->groupArray($referralMessage) as $key => $value) {
		//  	$commonProblemChart->addRow([$key, $value]);
		//  }

		// // Lava::BarChart('Religion', $religionChart);
		// Lava::PieChart('Common Problems', $commonProblemChart, [
		//     'title'  => 'Common Problems',
		//     'is3D'   => true,
		//     'slices' => [
		//         ['offset' => 0.2],
		//         ['offset' => 0.25],
		//         ['offset' => 0.3]
		//     ]
		// ]);

		// Lava::BarChart('Common Problems', $commonProblemChart);
		

	 	
	 	return $referrals;
	 }

	 public function filterStudentWithoutCumulative($request){
	 	$isYearChecked = false;
	 	$isCollegeChecked = false;
	 	$isCourseChecked = false;

	 	// $students = Cumulative::where('is_active', 1)->join('users', 'users.code', '=', 'cumulative_record.student_id')
	 	// 			->join('student', 'student.code', '=', 'cumulative_record.student_id')
	 	// 			->get();

	 	$students = User::where('is_active' ,'=', '1')
	 				->where('status','=','student')
	 				->join('student', 'student.code', '=', 'users.code')
	 				->get();



	 	$years = $request->input('year');
	    
	     if($years){
	     	$isYearChecked = true;
	     	$tmpStudents= array();
	     	foreach ($years as $key => $value) {
		    	//filter students
		    	foreach($students as $student){
		    		if($student->year == $value){
		    				
		    			array_push($tmpStudents,$student);
		    		}
		    	}
		    }
		   	 $students = $tmpStudents;
	     }

	    
	    $user = Auth::user();
	     if($user->status = "guidance"){
	     	$guidance_councelor = Guidance_councelor::where('code', $user->code)->first();
	     	$isCollegeChecked = true;
	    	$tmpStudents= array();
	     	foreach($students as $student){
	    		if($student->college_id ==  $guidance_councelor->college_id){
	    			array_push($tmpStudents,$student);
	    		}
	    	}

	    	$students = $tmpStudents;

	    	}else{
	     	$colleges = $request->input('college');
	
		    if($colleges){
		    	$isCollegeChecked = true;
		    	$tmpStudents= array();
		    	foreach ($colleges as $key => $value) {
			    	//filter students
			    	foreach($students as $student){
			    		if($student->college_id ==  $value){
			    			array_push($tmpStudents,$student);
			    		}
			    	}
			    }
			    $students = $tmpStudents;
		    }

	     }


	    $courses = $request->input('course');

	    if($courses){
	    	$isCourseChecked = true;
	    	$tmpStudents= array();
	    	foreach ($courses as $key => $value) {
		    	foreach($students as $student){
		    		if($student->course_id ==  $value){
		    			array_push($tmpStudents,$student);
		    		}
		    	}
		    }
		     $students = $tmpStudents;
	    }


	    if( !($isCourseChecked) && !($isYearChecked) && !($isCollegeChecked) ){
	    	$students = array();
	    }

	    return $students;
	 }



	 public function filterStudent($request){
	 	$isYearChecked = false;
	 	$isCollegeChecked = false;
	 	$isCourseChecked = false;

	 	$students = Cumulative::where('is_active', 1)->join('users', 'users.code', '=', 'cumulative_record.student_id')
	 				->join('student', 'student.code', '=', 'cumulative_record.student_id')
	 				->get();

	 	$years = $request->input('year');
	    
	     if($years){
	     	$isYearChecked = true;
	     	$tmpStudents= array();
	     	foreach ($years as $key => $value) {
		    	//filter students
		    	foreach($students as $student){
		    		if($student->year == $value){
		    			array_push($tmpStudents,$student);
		    		}
		    	}
		    }
		   	 $students = $tmpStudents;
	     }

	     $user = Auth::user();
	     if($user->status = "guidance"){
	     	$guidance_councelor = Guidance_councelor::where('code', $user->code)->first();
	     	$isCollegeChecked = true;
	    	$tmpStudents= array();
	     	foreach($students as $student){
	    		if($student->college_id ==  $guidance_councelor->college_id){
	    			array_push($tmpStudents,$student);
	    		}
	    	}

	    	$students = $tmpStudents;
	     }else{
	     	$colleges = $request->input('college');
	
		    if($colleges){
		    	$isCollegeChecked = true;
		    	$tmpStudents= array();
		    	foreach ($colleges as $key => $value) {
			    	//filter students
			    	foreach($students as $student){
			    		if($student->college_id ==  $value){
			    			array_push($tmpStudents,$student);
			    		}
			    	}
			    }
			    $students = $tmpStudents;
		    }

	     }
	 	
	    


	    $courses = $request->input('course');

	    if($courses){
	    	$isCourseChecked = true;
	    	$tmpStudents= array();
	    	foreach ($courses as $key => $value) {
		    	foreach($students as $student){
		    		if($student->course_id ==  $value){
		    			array_push($tmpStudents,$student);
		    		}
		    	}
		    }
		     $students = $tmpStudents;
	    }


	    if( !($isCourseChecked) && !($isYearChecked) && !($isCollegeChecked) ){
	    	$students = array();
	    }

	    return $students;
	 }
	 public function groupArray($dataArray){

	 	$uniqueArray = array_unique($dataArray);
	 	$groupedArray = [];

	 	foreach ($uniqueArray as $unique) {
	 		$dataCount = 0;
	 		foreach ($dataArray as $data) {
	 			if($data == $unique){
	 				$dataCount++;
	 			}
	 		}

	 		// array_push($groupedArray, [$unique => $dataCount ]);
	 		$groupedArray[$unique] = $dataCount;
	 	}
		
	 	return $groupedArray;
	 }

	 public function frequentlyReferred(){

 		$referral = Referral::join('users', 'referral.student_id', '=', 'users.code')
 					->select('users.*', 'referral.*', DB::raw('count(student_id) as total'))
 					->groupBy('users.code')
 					->get();
 		return $referral;
	 }
}
