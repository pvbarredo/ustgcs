<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Inbox;
use App\Student;
use App\Guidance_councelor;
use Auth;
use Response;
use App\User;
use App\Inbox_reply;
use DB;

class InboxController extends Controller
{


    public function getEmailReplies($id){

        $inbox_replies = DB::table('inbox_reply')
                            ->join('users', 'inbox_reply.sender_code', '=','users.code')
                            ->where('inbox_id',$id)
                            ->orderBy('inbox_reply.created_at', 'desc')
                            ->get();
        return Response::json($inbox_replies);

    }


    public function sendEmailReply(Request $request){
        $inbox_reply = new Inbox_reply;
        $inbox_reply->inbox_id = $request->input('inbox_id');
        $inbox_reply->sender_code = $request->input('sender_code');
        $inbox_reply->status = $request->input('status');
        $inbox_reply->message = $request->input('message');

        $inbox_reply->save();
    }

    public function newMessage(Request $request)
    {
    	$inbox = new Inbox;
    	$inbox->student_id = $request->input('student.id');
    	$inbox->created_by = Auth::user()->code;
    	$inbox->title = $request->input('title');
    	$inbox->message = $request->input('message');

    	$inbox->save();

    	return "SUCESS CREATING MESSAGE";
    }

    public function getMessages(){
    	$auth = Auth::user();
    	$status = $auth->status;
    	$user_id = $auth->code;


    	$inboxes = Inbox::where('created_by', $user_id)->orWhere('student_id',$user_id)->orderBy('created_at', 'desc')->get();
    	$response_messages = [];

    	if($status == 'guidance'){

    		foreach ($inboxes as $inbox ) {
    			$user = User::where('code',$inbox->student_id)->first();
	    		$response_message = [
	    			"guidance_name" => Auth::user()->firstname . " " . Auth::user()->lastname,
	    			"student_name" => $user->firstname . " ". $user->lastname,
                    "id" => $inbox->id,
	    			"title" => $inbox->title,
	    			"message" => $inbox->message,
                    "is_read" => $inbox->is_read,
                    "created_at" => $inbox->created_at
	    		];

	    		array_push($response_messages,$response_message);
    		}
    		
    		return Response::json($response_messages);

    	}
    	if($status == 'student'){

    		foreach ($inboxes as $inbox ) {
    			$user = User::where('code',$inbox->created_by)->first();
	    		$response_message = [
	    			"guidance_name" => $user->firstname . " " . $user->lastname,
	    			"student_name" => Auth::user()->firstname . " ". Auth::user()->lastname,
                    "id" => $inbox->id,
	    			"title" => $inbox->title,
	    			"message" => $inbox->message,
                    "is_read" => $inbox->is_read,
                    "created_at" => $inbox->created_at
	    		];

	    		array_push($response_messages,$response_message);
    		}
    		
    		return Response::json($response_messages);

    	}

    }
}
