<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Input;
use League\Csv\Reader;
use Response;
use App\User;
use App\Student;
use App\Guidance_councelor;
use App\Professor;
use App\Director;
use Auth;
use App\Image;

class UploadController extends Controller
{
    public function uploadCSV(Request $request){

    	$file = $request->file('csv');
    	$file_ext = $file->getClientOriginalExtension();
    	User::where('is_active', 1)
            ->update(['is_active' => 0]);
        User::where('status', 'admin')
            ->update(['is_active' => 1]);
    	
    	if ($file_ext === 'csv') {
    		//process file 
    		$reader = Reader::createFromPath($file);
			$results = $reader->fetch();
			$count=0;
			foreach ($results as $row) {
			    if($count){
			    	//check first if its already in the database
			    	$code = $row[0];
			    	$user = User::where('code', $code)->first();
			    	if ($user) {
				    	$user->firstname = $row[1];
				    	$user->middlename = $row[2];
				    	$user->lastname = $row[3];
				    	
				    	$time = strtotime($row[4]);
						$newformat = date('Y-m-d',$time);
				    	$user->birthday = $newformat;

				    	$user->gender = $row[5];
				    	$user->email = $row[6];
				    	$user->status = $row[7];

				    	$user->is_active = 1;
				    	$user->is_first = 1;

				    	$accountType = $user->status;

				    	if ($accountType === 'student'){
				    		$stud = Student::where('code', $code) -> first();
				    		$stud->college_id = $row[8];
				    		$stud->course_id = $row[9];
				    		$stud->year = $row[10];
				    		$stud->save();
				    	}
				    	else if($accountType === 'professor'){
				    		$prof = Professor::where('code', $code) -> first();
				    		$prof->college_id = $row[8];
				    		$prof->save();
				    	}
				    	else if($accountType === 'guidance'){
				    		$guide = Guidance_councelor::where('code', $code) -> first();
				    		$guide->college_id = $row[8];
				    		$guide->save();

				    	}
				    	else{
				    		$dir = Director::where('code', $code) -> first();
				    		$dir->save();
				    	}

				    	
				    	$user->save();



			    	}else{
			    		$user = new User;
				    	$user->code = $row[0];
				    	$user->firstname = $row[1];
				    	$user->middlename = $row[2];
				    	$user->lastname = $row[3];
				    	$time = strtotime($row[4]);
						$newformat = date('Y-m-d',$time);
				    	$user->birthday = $newformat;
				    	$user->gender = $row[5];
				    	$user->email = $row[6];
				    	$user->status = $row[7];
				    	$user->is_active =1;
				    	$user->is_first = 1;
				    	$user->password = bcrypt("admin123");
				    	$user->created_by = Auth::user()->code;
				    	$user->creation_mode = 'Batch';
				    	$user->save();

				    	$accountType = $user->status;

				    	if ($accountType === 'student'){
				    		$stud = new Student;
				    		$stud->code = $row[0];
				    		$stud->college_id = $row[8];
				    		$stud->course_id = $row[9];
				    		$stud->year = $row[10];
				    		$stud->save();
				    	}
				    	else if($accountType === 'professor'){
				    		$prof = new Professor;
				    		$prof->code = $row[0];
				    		$prof->college_id = $row[8];
				    		$prof->save();
				    	}
				    	else if($accountType === 'guidance'){
				    		$guide = new Guidance_councelor;
				    		$guide->code = $row[0];
				    		$guide->college_id = $row[8];
				    		$guide->save();

				    	}
				    	else{
				    		$dir = new Director;
				    		$dir->code = $row[0];
				    		$dir->save();
				    	}

				    	
			    	}

			    }
			    $count++;
			}

			return view('addAccount', ['isSuccessful' => true]);
    	}


    }

    public function uploadPicture(Request $request){

    	$file = $request->file('dp');
    	$file_ext = $file->getClientOriginalExtension();

    	// //for the meantime JPG muna accepted
    	if (strtoupper($file_ext) == 'JPG') {

    		$user_code = Auth::user()->code;
    		$file_name = $user_code . "." . $file_ext;
    		$path = public_path().'/img';
    		$file->move($path,$file_name);


    		// return view('cumulative',['user' => Auth::user()]);
    	    return redirect()->action('HomeController@cumulative');

    	}else{
    		// return redirect()->route('cumulative');
    		return redirect()->action('HomeController@cumulative');
    	}
    } 
}
