<?php

namespace App\Http\Controllers;

use App\Cumulative;
use App\Cumulative_record_additional;
use Illuminate\Http\Request;
use App\Http\Requests;
use Faker\Factory as Faker;
use Auth;
use App\User;
use Response;

class CumulativeController extends Controller
{

    public function edit(Request $request)
    {

        $additional = Cumulative_record_additional::find($request->input('id'));
        $additional->field = $request->input('field');
        $additional->save();


        return 'Update Done';
    }
    
    public function answer(Request $request){
        $additional = Cumulative_record_additional::find($request->input('id'));
        $additional->value = $request->input('value');
        $additional->save();

        return 'Success answering';
    }

    public function getStudentByName($firstname)
     {
          $users = User::where('firstname','like',$firstname .'%')->where('status','student')->get();
          
          $response_student = [];

          foreach ($users as $user) {
            $student = Student::where('code',$user->code)->first();
            $student_object = [
                "id" => $student->code,
                "firstname" => $user->firstname,
                "middlename" => $user->middlename,
                "lastname" => $user->lastname,
                "email" => $user->email,
                "year" => $student->year,
                "course" => $student->course
            ];

            array_push($response_student,$student_object);
          }
          return $response_student;
     }

    public function view_cumulative() {
    	$user = Auth::user();
        $user_id = $user->code;
        // return Cumulative::where('student_id',$user_id)
        //         ->join('users', 'users.code', '=', 'cumulative_record.student_id')
        //         ->first();

        return User::where('code',$user_id)
                ->leftJoin('cumulative_record','cumulative_record.student_id', '=','users.code')
                ->first();
    }
    
    public function cumulativeView($id){
        return Cumulative::where('student_id',$id)
                ->join('users', 'users.code', '=', 'cumulative_record.student_id')
                ->first();
    }

    public function autoPopulate(){

        $faker = Faker::create();
        $response_cumulative = [
            "student_id" => $faker->randomNumber,

            "firstname" => Auth::user()->firstname,
            "middlename" => Auth::user()->middlename,
            "lastname" => Auth::user()->lastname,

            "auxiliary_name" => "x",
            "student_no" => $faker->randomNumber,
            "nickname" => $faker->firstname,
            "gender" => "Female",
            "birthdate" => $faker->dateTimeThisCentury->format('Y-m-d'),
            "age" => $faker->randomDigit,

            "nationality" => "Filipino",
            "religion" => "christian",
            "email" => $faker->email,
            "curr_address" => $faker->address,
            "curr_city" => $faker->city,
            "perm_address" => $faker->address,
            "perm_city" => $faker->city,
            "mobile_no" => $faker->phoneNumber,
            "home_phone_no" => $faker->phoneNumber,
            "living_arrangement" => "Family",
            "acad_health_prob" => $faker->boolean($chanceOfGettingTrue = 50),

            "acad_health_prob_spec" => $faker->name,
            "curricular_health_prob" => $faker->boolean($chanceOfGettingTrue = 50),
            "curricular_health_prob_spec" => $faker->name,
            "psychiatric_help" => $faker->boolean($chanceOfGettingTrue = 50),
            "father_name" => $faker->name,
            "father_address" => $faker->address,
            "father_occupation" => "Driver",
            "father_contact" => $faker->phoneNumber,
            "mother_name" => $faker->name,
            "mother_address" => $faker->address,


            "mother_occupation" => "Housewife",
            "mother_contact" => $faker->phoneNumber,
            "parent_marital_status" => $faker->name,
            "parent_marital_status_spec" => $faker->name,
            "parent_marital_status_other" => $faker->name,
            "guardian_name" => $faker->name,
            "guardian_relation" => $faker->name,
            "guardian_contact" => $faker->name,
            "family_life_perception" => $faker->name,

            // "close_mother" => $faker->boolean($chanceOfGettingTrue = 50),
            // "close_father" => $faker->boolean($chanceOfGettingTrue = 50),

            // // "close_old_sibling" => $faker->boolean($chanceOfGettingTrue = 50),
            // "close_young_sibling" => $faker->boolean($chanceOfGettingTrue = 50),
            // "close_other" => $faker->boolean($chanceOfGettingTrue = 50),
            // "close_other_spec" => $faker->name,


            "motivate_family" => $faker->boolean($chanceOfGettingTrue = 50),
            "motivate_friends" => $faker->boolean($chanceOfGettingTrue = 50),
            "motivate_job" => $faker->boolean($chanceOfGettingTrue = 50),
            "motivate_significant_other" => $faker->name,
            "motivate_career" => $faker->boolean($chanceOfGettingTrue = 50),
            "motivate_fulfillment" => $faker->boolean($chanceOfGettingTrue = 50),

            "motivate_prestige" => $faker->boolean($chanceOfGettingTrue = 50),
            "motivate_passion" => $faker->boolean($chanceOfGettingTrue = 50),
            "motivate_interest" => $faker->boolean($chanceOfGettingTrue = 50),
            "counselor_info" => $faker->boolean($chanceOfGettingTrue = 50),
            "counselor_personal" => $faker->boolean($chanceOfGettingTrue = 50),
            "counselor_academics" => $faker->boolean($chanceOfGettingTrue = 50),
            "counselor_relationships" => $faker->boolean($chanceOfGettingTrue = 50),
            "counselor_others" => $faker->boolean($chanceOfGettingTrue = 50),
            "counselor_others_spec" => $faker->name,

            "counseling" => $faker->boolean($chanceOfGettingTrue = 50),
            

        ];

        return Response::json($response_cumulative);
    }

    public function store_cumulative(Request $request) {
        $cum_record = new Cumulative;

        $user = Auth::user();
        $user_id = $user->code;

        $user_info = User::where('code', $user_id)->first();

        $cum_record->student_id = $user_id;

        $cum_record->auxiliary_name = $request->input('auxiliary_name');
        $cum_record->nickname = $request->input('nickname');

        $cum_record->nationality = $request->input('nationality');
        $cum_record->religion = $request->input('religion');
        $cum_record->email = $request->input('email');
        $cum_record->curr_address = $request->input('curr_address');
        $cum_record->curr_city = $request->input('curr_city');
        $cum_record->perm_address = $request->input('perm_address');
        $cum_record->perm_city = $request->input('perm_city');
        $cum_record->mobile_no = $request->input('mobile_no');
        $cum_record->home_phone_no = $request->input('home_phone_no');
        $cum_record->living_arrangement = $request->input('living_arrangement');
        $cum_record->acad_health_prob = $request->input('finalAcad_health_prob');
       
       	$cum_record->curricular_health_prob = $request->input('finalCurricular_health_prob');
      

       	$cum_record->psychiatric_help = $request->input('psychiatric_help');
       	$cum_record->counseling = $request->input('counseling');
       	$cum_record->father_name = $request->input('father_name');
       	$cum_record->father_address = $request->input('father_address');
       	$cum_record->father_occupation = $request->input('father_occupation');
 		$cum_record->father_contact = $request->input('father_contact');
 		$cum_record->mother_name = $request->input('mother_name');
       
       	$cum_record->mother_address = $request->input('mother_address');
       	$cum_record->mother_occupation = $request->input('mother_occupation');
 		$cum_record->mother_contact = $request->input('mother_contact');
 		$cum_record->parent_marital_status = $request->input('parent_marital_status');
 		$cum_record->parent_marital_status_spec1 = $request->input('parent_marital_status_spec1');
 		$cum_record->parent_marital_status_spec2 = $request->input('parent_marital_status_spec2');
 		$cum_record->parent_marital_status_other = $request->input('parent_marital_status_other');
 		$cum_record->guardian_name = $request->input('guardian_name');
 		$cum_record->guardian_relation = $request->input('guardian_relation');
 		$cum_record->guardian_contact = $request->input('guardian_contact');
 		$cum_record->family_life_perception = $request->input('family_life_perception');
 		
 		$cum_record->close_family = $request->input('finalCloseFamilyCheckbox');


 		// $cum_record->motivate_family = $request->input('motivate_family');
 		// $cum_record->motivate_friends = $request->input('motivate_friends');
 		// $cum_record->motivate_job = $request->input('motivate_job');
 		
   //      $cum_record->motivate_significant_other = $request->input('motivate_significant_other');
 		// $cum_record->motivate_career = $request->input('motivate_career');
 		// $cum_record->motivate_fulfillment = $request->input('motivate_fulfillment');
 		// $cum_record->motivate_prestige = $request->input('motivate_prestige');
 		// $cum_record->motivate_passion = $request->input('motivate_passion');
 		// $cum_record->motivate_interest = $request->input('motivate_interest');

        $cum_record->motivate_factor = $request->input('finalMotivateFactors');

        $cum_record->counselor_help = $request->input('finalCounselorHelp');

 		// $cum_record->counselor_info = $request->input('counselor_info');
 		// $cum_record->counselor_personal = $request->input('counselor_personal');
 		// $cum_record->counselor_academics = $request->input('counselor_academics');
 		// $cum_record->counselor_relationships = $request->input('counselor_relationships');
 		
   //      $cum_record->counselor_others = $request->input('counselor_others');
 		// $cum_record->counselor_others_spec = $request->input('counselor_others_spec');

        $cum_record->save();


        return 'Cumulative record successfully created with id ' . $cum_record->cum_id;
    }

    

    public function update_cumulative(Request $request, $id) {
        $cum_record = Cumulative::where('student_id', $id)->first();
        
        $user_info = User::where('code', Auth::user()->code)->first();


        $cum_record->auxiliary_name = $request->input('auxiliary_name');
        $cum_record->nickname = $request->input('nickname');
  

        $cum_record->nationality = $request->input('nationality');
        $cum_record->religion = $request->input('religion');
        $cum_record->email = $request->input('email');
        $cum_record->curr_address = $request->input('curr_address');
        $cum_record->curr_city = $request->input('curr_city');
        $cum_record->perm_address = $request->input('perm_address');
        $cum_record->perm_city = $request->input('perm_city');
        $cum_record->mobile_no = $request->input('mobile_no');
        $cum_record->home_phone_no = $request->input('home_phone_no');
        $cum_record->living_arrangement = $request->input('living_arrangement');
        $cum_record->acad_health_prob = $request->input('finalAcad_health_prob');
       

        $cum_record->curricular_health_prob = $request->input('finalCurricular_health_prob');
       
        $cum_record->psychiatric_help = $request->input('psychiatric_help');
        $cum_record->counseling = $request->input('counseling');
        $cum_record->father_name = $request->input('father_name');
        $cum_record->father_address = $request->input('father_address');
        $cum_record->father_occupation = $request->input('father_occupation');
        $cum_record->father_contact = $request->input('father_contact');
        $cum_record->mother_name = $request->input('mother_name');
       
        $cum_record->mother_address = $request->input('mother_address');
        $cum_record->mother_occupation = $request->input('mother_occupation');
        $cum_record->mother_contact = $request->input('mother_contact');
        $cum_record->parent_marital_status = $request->input('parent_marital_status');
        $cum_record->parent_marital_status_spec1 = $request->input('parent_marital_status_spec1');
        $cum_record->parent_marital_status_spec2 = $request->input('parent_marital_status_spec2');
        $cum_record->parent_marital_status_other = $request->input('parent_marital_status_other');
        $cum_record->guardian_name = $request->input('guardian_name');
        $cum_record->guardian_relation = $request->input('guardian_relation');
        $cum_record->guardian_contact = $request->input('guardian_contact');
        $cum_record->family_life_perception = $request->input('family_life_perception');
        
        
        $cum_record->close_family = $request->input('finalCloseFamilyCheckbox');


        // $cum_record->motivate_family = $request->input('motivate_family');
        // $cum_record->motivate_friends = $request->input('motivate_friends');
        // $cum_record->motivate_job = $request->input('motivate_job');
        
        // $cum_record->motivate_significant_other = $request->input('motivate_significant_other');
        // $cum_record->motivate_career = $request->input('motivate_career');
        // $cum_record->motivate_fulfillment = $request->input('motivate_fulfillment');
        // $cum_record->motivate_prestige = $request->input('motivate_prestige');
        // $cum_record->motivate_passion = $request->input('motivate_passion');
        // $cum_record->motivate_interest = $request->input('motivate_interest');
        // $cum_record->counselor_info = $request->input('counselor_info');
        // $cum_record->counselor_personal = $request->input('counselor_personal');
        // $cum_record->counselor_academics = $request->input('counselor_academics');
        // $cum_record->counselor_relationships = $request->input('counselor_relationships');
        
        // $cum_record->counselor_others = $request->input('counselor_others');
        // $cum_record->counselor_others_spec = $request->input('counselor_others_spec');

         $cum_record->motivate_factor = $request->input('finalMotivateFactors');

        $cum_record->counselor_help = $request->input('finalCounselorHelp');

        $cum_record->save();

        return "Successfully updated record of student #" . $cum_record->student_no;
    }


}
