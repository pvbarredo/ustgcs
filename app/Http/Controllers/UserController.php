<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Student;
use App\Guidance_councelor;
use App\Professor;
use App\Director;
use Auth;
use Hash;
use App\College;
use App\Course;

class UserController extends Controller
{
    public function getStudentUsingFirstname($firstname)
	 {
	      $users = User::where('firstname','like',$firstname .'%')
	      				->orWhere('lastname','like',$firstname. '%')
	      				->orWhere('code','like',$firstname.'%')
	      				->where('status','student')
	      				->where('is_active', 1)
	      				->get();
	      


	      $response_student = [];

	      foreach ($users as $user) {
	      	$student = Student::where('code',$user->code)->first();

	      	$college = College::find($student->college_id);
	      	$course = Course::find($student->course_id);

	      	$student_object = [
	      		"id" => $student->code,
	      		"firstname" => $user->firstname,
	      		"middlename" => $user->middlename,
	      		"lastname" => $user->lastname,
	      		"email" => $user->email,
	      		"year" => $student->year,
	      		"college" => $college->name,
	      		"course" => $course->name,
	      		"college_id" => $college->id
	      	];

	      	array_push($response_student,$student_object);
	      }
	      return $response_student;
	 }

	 public function getGuidanceUsingFirstname($firstname)
	 {
	      $users = User::where('firstname','like',$firstname .'%')
	      			->where('status','guidance')
	      			->where('is_active', 1)
	      			->get();
	      $response_guidance = [];

	      foreach ($users as $user) {
	      	$guidance = Guidance_councelor::where('code',$user->code)->first();
	      	$guidance_object = [
	      		"id" => $guidance->code,
	      		"firstname" => $user->firstname,
	      		"middlename" => $user->middlename,
	      		"lastname" => $user->lastname,
	      		"email" => $user->email,
	      		"college" => $guidance->college
	      	];

	      	array_push($response_guidance,$guidance_object);
	      }
	      return $response_guidance;
	 }


	 public function changePassword(Request $request){
	 	$newPass = $request->input('newPass');
	 	$user = User::where('code',Auth::user()->code)->first();
	 	$user->password = bcrypt($newPass);
	 	$user->save();

	 	return "Success changing password";
	 	

	 }

	 public function compareInputPassToCurrent(Request $request){
	 	$current_pass = Auth::user()->password;
	 	$inputted_old_pass = $request->input('oldPass');

	 	if(Hash::check($inputted_old_pass, $current_pass)){
	 		return 'true';
	 	}else{
	 		return 'false';
	 	}
	 }

	 public function editAccountInfo(Request $request){
	 	$firstname = $request->input('firstname');
	 	$middlename = $request->input('middlename');
	 	$lastname = $request->input('lastname');
	 	$email = $request->input('email');
	 	$college = $request->input('college_id');
	 	$course = $request->input('course_id');
	 	$year = $request->input('year');


	 		

		$code = Auth::user()->code;
		
		$user = User::where('code',$code)->first();

		$accountType = $user->status;
	 	


	 	if($accountType == 'student' ){
	 		$stud = Student::where('code', $code)->first();
	 		$stud->college_id = $college;
	 		$stud->course_id = $course;
	 		$stud->year = $year;
	 		$stud->save();
	 	}
	 	else if($accountType == 'professor'){
	 		$prof = Professor::where('code', $code)->first();
	 		$prof->college_id = $college;
	 		$prof->save();
	 	}
	 	else if($accountType == 'guidance'){
	 		$guidance = Guidance_councelor::where('code', $code)->first();
	 		$guidance->college_id = $college;
	 		$guidance->save();


	 	}
	 	else if($accountType == 'director'){
	 		$dir = Director::where('code', $code)->first();
	 		$dir->save();
	 	}
	 	



	 	$user->firstname = $firstname;
	 	$user->middlename = $middlename;
	 	$user->lastname = $lastname;
	 	$user->email = $email;
	 	$user->save();


	 	return "Successful editing profile";
	 }

	 public function addAccountInfo(Request $request){

        $user = Auth::user();
        $user_id = $user->code;

	 	$accountType = $request->input('accountType');
	 	$code = $request->input('code');
	 	$firstname = $request->input('firstname');
	 	$middlename = $request->input('middlename');
	 	$lastname = $request->input('lastname');
	 	$email = $request->input('email');
	 	$birthday  = $request->input('birthday');
	 	$gender = $request->input('gender');
	 	$pass = 'admin123';
	 	$college = $request->input('college');
	 	$course = $request->input('course');
	 	$year = $request->input('year');
	 	$contact = $request->input('contact');


	 	$user = new User;

	 	if($accountType == 'student'){
	 		$stud = new Student;
	 		$stud->code = $code;
	 		$stud->year = $year;
	 		$stud->college_id = $college;
	 		$stud->course_id = $course;
	 		$stud->save();
	 		
	 	}
	 	else if($accountType == 'professor'){
	 		$prof = new Professor;
	 		$prof->code=$code;
	 		// $prof->college_id = 0;
	 		$prof->save();
	 		$user->code = Professor::orderBy('id', 'asc')->get()->last()->id;

	 	}
	 	else if($accountType == 'guidance'){
	 		$guide = new Guidance_councelor;
	 		$guide->code=$code;
	 		$guide->college_id = $college;
	 		$guide->save();
	 	
	 	}
	 	else{
	 		$dir = new Director;
	 		$dir->code=$code;
	 		$dir->save();
	 	}

	 	
	 
	 	$user->firstname = $firstname;
	 	$user->code = $code;
	 	$user->middlename = $middlename;
	 	$user->lastname = $lastname;
	 	$user->email = $email;
	 	$user->status = $accountType;
	 	$user->birthday =$birthday;
	 	$user->gender = $gender;
	 	$user->is_active = 1;
	 	$user->is_first = 1;
	 	$user->password = bcrypt($pass);
	 	$user->created_by = $user_id;
	 	$user->creation_mode = 'single';
	 	$user->save();


	 	return "Successful adding profile";
	 }

	 public function getAuthUser(){
	 	return Auth::user();
	 }

	 public function getBasicInfo(){
	 	//this will be expanded
	 	$user = Auth::user();
	 	$accountType = $user->status;
	 	$code = $user->code;
	 	$response_basicInfo =[];
	 	if($accountType == 'student' ){
	 		$stud = Student::where('code', $code)->first();

		 	$response_basicInfo = [
		 		"firstname" => $user->firstname,
		 		"middlename" => $user->middlename,
		 		"lastname" => $user->lastname,
		 		"email" => $user->email,
		 		"college_id" => $stud->college_id,
		 		"course_id" => $stud->course_id,
		 		"year" => $stud->year
		 	];
	 	}
	 	else if($accountType == 'professor'){
	 		$prof = Professor::where('code', $code)->first();

		 	$response_basicInfo = [
		 		"firstname" => $user->firstname,
		 		"middlename" => $user->middlename,
		 		"lastname" => $user->lastname,
		 		"email" => $user->email,
		 		"college_id" => $prof->college_id

		 	];
	 	}
	 	else if($accountType == 'guidance'){
	 		$guide = Guidance_councelor::where('code', $code)->first();
	 		$response_basicInfo = [
		 		"firstname" => $user->firstname,
		 		"middlename" => $user->middlename,
		 		"lastname" => $user->lastname,
		 		"email" => $user->email,
		 		"college_id" => $guide->college_id

		 	];

	 	}
	 	else{
	 		$dir = Director::where('code', $code)->first();
	 		$response_basicInfo = [
		 		"firstname" => $user->firstname,
		 		"middlename" => $user->middlename,
		 		"lastname" => $user->lastname,
		 		"email" => $user->email,
		 

		 	];
	 	}

	 	return $response_basicInfo;

	 }
}
