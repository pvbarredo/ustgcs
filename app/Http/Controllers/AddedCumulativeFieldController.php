<?php

 namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cumulative_record_additional;

class AddedCumulativeFieldController extends Controller {

	/**
	 * Display a listing of the resource.
	 * GET /addedcumulativefield
	 *
	 * @return Response
	 */
	public function index()
	{
		
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /addedcumulativefield/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /addedcumulativefield
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$added = new Cumulative_record_additional;

		$added->user_code = $request->input('user_code');
		$added->field = $request->input('field');
		$added->value = $request->input('value');
		$added->created_by = $request->input('created_by');

		$added->save();

		return 'Done creating new additional question';
	}

	/**
	 * Display the specified resource.
	 * GET /addedcumulativefield/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return  Cumulative_record_additional::where('user_code', $id )->orderBy('created_at', 'asc')->get();
	}

	

	/**
	 * Update the specified resource in storage.
	 * PUT /addedcumulativefield/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit( Request $request )
	{
		$cumulative_record_additional = Cumulative_record_additional::find($request->input('id'));
		$cumulative_record_additional-> $request->input('field');
		$cumulative_record_additional->save();


		return 'Update Done';
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /addedcumulativefield/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Cumulative_record_additional::find($id)->delete();

		return 'deleted';
	}

}