<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\College;
use App\Course;
use Auth;
use App\Guidance_councelor;

class CollegeController extends Controller
{
    //

    public function getAllColleges($id = null){
    	if ($id == null) {
            return College::orderBy('id', 'asc')->get();
        } else {
            return $this->show_college($id);
        }
    }

    public function show_college($id) {
        return College::find($id);
    }

     public function store_college(Request $request) {

    	$college = new College;

    	$college->name = $request->input('name');

       	$college->save();

       	return 'Employee record successfully created with id ' . $college->id;


    }

    public function update_college(Request $request, $id) {

    	$college = College::find($id);

    	$college->name = $request->input('name');

    	$college->save();

    	


    }
     public function delete_college(Request $request, $id) {
        $college = College::find($id);
        $courses = Course::where("college_id", $college->id)->get();
        
        foreach ($courses as $course) {
            $course->delete();
        }

        $college->delete();


    }


    public function getInfrastructure(){
        $colleges = College::all();


        $response_infrastructure = [];

        foreach ($colleges as $college) {

            $course = Course::where("college_id", $college->id)->get();
            $infrastructure = [
                "colleges" => $college,
                "courses" => $course
            ];
            array_push($response_infrastructure, $infrastructure);
        }

        return $response_infrastructure;

    }
    public function getColleges(){
       if(Auth::user()->status == 'guidance'){
        $guidance = Guidance_councelor::where('code', Auth::user()->code)->first();
        $college = [];
        array_push($college, College::find($guidance->college_id));
        return $college ; 
       }

       // if(Auth::user()->status == 'director'){
        return College::all();
       // }
       

    }

    public function getCourses(){
        if(Auth::user()->status == 'guidance'){
            $guidance = Guidance_councelor::where('code', Auth::user()->code)->first();
            return Course::where('college_id',$guidance->college_id)->get() ; 
        }
       
            return College::all();
        

    }
}
