<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Referral;
use App\Student;
use App\User;
use App\Feedback;
use App\Feedbackchat;
use Auth;
use App\Guidance_councelor;

class ReferralController extends Controller
{

	public function getReferral($id){

		return Referral::where('student_id',$id)->get();
	}
    public function newReferral(Request $request)
	 {
	 	$referral = new Referral;

	 	$referral->professor_id = $request->input('professor_id');
	 	$referral->student_id = $request->input('student_id');
	 	$referral->college_id = $request->input('college_id');
	 	$referral->message = $request->input('message');
	 	$referral->description = $request->input('description');

	 	$referral->save();

	 	return "Success creating new Referral";
	 }

	 public function getAllSentReferral(){
	 	$user = Auth::user();
	 	//check the status of the user 
	 	if(Auth::user()->status == 'guidance'){
	 		$guidance = Guidance_councelor::where('code', $user->code)->first();
	 		$referrals = Referral::where('college_id', $guidance->college_id)->orderBy('created_at', 'desc')->get();
	 	}
	 	if(Auth::user()->status == 'professor'){
			$referrals = Referral::where('professor_id', $user->code)->orderBy('created_at', 'desc')->get();
	 	}

	 	$response_referrals = [];

	 	foreach ($referrals as $referral) {
	 		$student = User::where('code', $referral->student_id)->first();
	 		$professor = User::where('code', $referral->professor_id)->first();

	 		$referral_object = [
	 			"referral_id" => $referral->id,
	 			"student_name" =>  $student->firstname . " " . $student->lastname,
	 			"professor_name" => $professor->firstname . " " . $professor->lastname,  
	 			"message" => $referral->message,
	 			"description" => $referral->description,
	 			"created_date" => $referral->created_at
	 		];
	 		array_push($response_referrals,$referral_object);
	 	}

	 	return $response_referrals;

	 }
	 public function sendFeedback(Request $request){
	 	$referral_id = $request->input('referral_id');
	 	$user_id = $request->input('user_id');
	 	$referral_reply = $request->input('message');
	 	$feedbackchat = Feedbackchat::where('referral_id', $referral_id)->first();


	 	$feedbackchat = new Feedbackchat;
	 	$feedbackchat->referral_id = $referral_id;
	 	$feedbackchat->user_id = Auth::user()->code;
	 	$feedbackchat->user_status = Auth::user()->status;
	 	$feedbackchat->message = $referral_reply;

	 	$feedbackchat->save();

	 	// if($feedback){
	 	// 	$feedback->message = $referral_reply;
	 	// 	$feedback->save();

	 	// }else{
	 	// 	$feedback = new Feedback;
		 // 	$feedback->referral_id = $referral_id;
		 // 	$feedback->guidance_id = Auth::user()->code;
		 // 	$feedback->message = $referral_reply;
		 // 	$feedback->save();
	 	// }
	 	

	 	return "Success reply Feedback";

	 }

	 public function getAllFeedbackChats($referral_id){
	 	$feedbackchats = Feedbackchat::where('referral_id', $referral_id)
	 			->orderBy('created_at', 'desc')
	 			->get();
	    $response_chat = [];
	 	foreach ($feedbackchats as $feedbackchat) {

	 		if($feedbackchat->user_status == 'guidance'){
	 			$guidance = Guidance_councelor::where('code', $feedbackchat->user_id)->first();
	 		}
	 		if($feedbackchat->user_status == 'professor'){

	 		}

	 		$user = User::where('code',$feedbackchat->user_id )->first();
	 		$fullName = $user->firstname . " " . $user->lastname;

	 		$isCurrentUser = false;
	 		if($feedbackchat->user_id == Auth::user()->code){
	 			$isCurrentUser = true;
	 		}

	 		$chat = [
	 			"feedbackchat" => $feedbackchat,
	 			"fullname" => $fullName,
	 			"isCurrentUser" => $isCurrentUser
	 		];

	 		array_push($response_chat,$chat);
	 	}

	 	return $response_chat;
	 }
	 public function checkReply($referral_id){

	 	$feedback = Feedback::where('referral_id', $referral_id)->first();

	 	return $feedback;
	 }
}
