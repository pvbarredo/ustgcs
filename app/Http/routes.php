<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    
    Route::get('/','HomeController@announcement');
    Route::get('welcome',function(){
    	return view('welcome');
    });
    Route::post('/password/reset', 'Auth\PasswordController@postReset');

    //navigation route
    Route::get('announcement','HomeController@announcement');
    Route::get('announcement/{id}','AnnouncementController@show');

    Route::get('cumulative','HomeController@cumulative');
    Route::get('cumulative/{id}','HomeController@cumulativeWithID');

    Route::get('calendar','HomeController@calendar');

    Route::get('inbox','HomeController@inbox');
    
    Route::get('changePass','HomeController@changePass');
    Route::get('editAccount','HomeController@editAccount');
    Route::get('college', 'HomeController@college');
    Route::get('course', 'HomeController@course');
    Route::get('account/{id}','HomeController@account');
    Route::get('cumulativeCriteria','HomeController@cumulativeCriteria');
    
    Route::get('about','HomeController@about');
    Route::get('referral','HomeController@referral');


    Route::get('studentProfiling','HomeController@studentProfiling');
    Route::get('nonCompliance','HomeController@nonCompliance');
    Route::get('commonProblem','HomeController@commonProb');
    Route::get('frequentlyReferred','HomeController@frequentlyReferred');
    Route::get('student','HomeController@student');


    //webservice route
    Route::get('webservice/getAllAnnouncementForUser','AnnouncementController@getAllAnnouncementForUser');
    Route::post('webservice/newAnnouncement','AnnouncementController@newAnnouncement');
    Route::get('webservice/getStudentUsingFirstname/{firstname}','UserController@getStudentUsingFirstname');
    Route::post('webservice/newReferral','ReferralController@newReferral');
    Route::get('webservice/getAllSentReferral','ReferralController@getAllSentReferral');
    Route::get('webservice/getReferral/{id}','ReferralController@getReferral');

   
    Route::get('webservice/getCumulativeRecord', 'CumulativeController@view_cumulative');
    Route::get('webservice/getStudentByName/', 'CumulativeController@getStudentByName');
    Route::get('webservice/autoPopulate', 'CumulativeController@autoPopulate');
    
    Route::get('webservice/getGuidanceUsingFirstname/{firstname}','UserController@getGuidanceUsingFirstname');
    Route::post('webservice/sendFeedback','ReferralController@sendFeedback');
    Route::get('webservice/checkReply/{referral_id}','ReferralController@checkReply');
    Route::get('webservice/getAllFeedbackChats/{referral_id}','ReferralController@getAllFeedbackChats');
    

    Route::post('webservice/createCumulativeRecord', 'CumulativeController@store_cumulative');
    Route::post('webservice/createCumulativeRecord/{id}', 'CumulativeController@update_cumulative');

    Route::post('webservice/newMessage', 'InboxController@newMessage'); 
    Route::get('webservice/getMessages', 'InboxController@getMessages');

    Route::post('webservice/changePassword', 'UserController@changePassword');
    Route::post('webservice/compareInputPassToCurrent', 'UserController@compareInputPassToCurrent');
    
    Route::post('webservice/addAccountInfo', 'UserController@addAccountInfo');
    Route::post('webservice/editAccountInfo', 'UserController@editAccountInfo');
    Route::get('webservice/getBasicInfo', 'UserController@getBasicInfo');

    Route::get('webservice/getColleges/{id?}', 'CollegeController@getAllColleges');
    Route::post('webservice/createCollege' ,'CollegeController@store_college');
    Route::post('webservice/createCollege/{id}' ,'CollegeController@update_college');
    Route::delete('webservice/deleteCollege/{id}', 'CollegeController@delete_college');

    Route::get('webservice/getCourses/{id?}', 'CourseController@getAllCourses');
    Route::get('webservice/getCoursesGuidance/guidance', 'CourseController@getAllCoursesGuidance');
    Route::get('webservice/account_showCourses/{id}', 'CourseController@account_showCourses');

    Route::get('webservice/getCourseYears/{id}', 'CourseController@show_courseYear');
    Route::post('webservice/createCourse' ,'CourseController@store_course');
    Route::post('webservice/createCourse/{id}' ,'CourseController@update_course');
    Route::delete('webservice/deleteCourse/{id}', 'CourseController@delete_course');
    
    Route::post('webservice/uploadCSV', 'UploadController@uploadCSV');
    Route::post('webservice/uploadPicture', 'UploadController@uploadPicture');

    Route::get('webservice/getInfrastructure', 'CollegeController@getInfrastructure'); 

    Route::get('webservice/getAuthUser', 'UserController@getAuthUser');
    Route::get('webservice/cumulativeView/{id}', 'CumulativeController@cumulativeView');

    Route::get('webservice/collegeCont/getCourses', 'CollegeController@getCourses');
    Route::get('webservice/collegeCont/getColleges', 'CollegeController@getColleges');

    Route::get('webservice/getNotification', 'AnnouncementController@getNotification');

    Route::post('webservice/sendNotification', 'ReportController@sendNotification');

    Route::get('webservice/checkNonComplianceNotification/{id}', 'ReportController@checkNonComplianceNotification');

    Route::get('webservice/deleteNonComplianceNotificationRecord/{code}', 'ReportController@deleteNonComplianceNotificationRecord');

    Route::get('webservice/getEmailReplies/{id}', 'InboxController@getEmailReplies');
    Route::post('webservice/sendEmailReply', 'InboxController@sendEmailReply');
    

    Route::get('webservice/generateNonComplianceReport', 'ReportController@generateNonComplianceReport');
    
    Route::post('webservice/studentProfiling', 'ReportController@studentProfiling');

    Route::post('webservice/commonProblem', 'ReportController@commonProblem');
    Route::get('webservice/updateInboxNotification/{id}','AnnouncementController@updateInboxNotification');
    Route::get('webservice/frequentlyReferred','ReportController@frequentlyReferred');

    Route::get('webservice/getAllStudents','StudentController@getAllStudents');
    Route::get('webservice/softDeleteStudent/{studentID}','StudentController@softDeleteStudent');

    
    
    Route::post('maintenance/{table}','MaintenanceController@store');
	Route::get('maintenance/{table}','MaintenanceController@index');
	Route::get('maintenance/create/{table}','MaintenanceController@create');
	Route::get('maintenance/{id}/{table}','MaintenanceController@show');
	Route::delete('maintenance/{id}/{table}','MaintenanceController@destroy');
	Route::put('maintenance/{id}/{table}','MaintenanceController@update');
	Route::get('maintenance/{id}/edit/{table}','MaintenanceController@edit');
	Route::get('maintenance/{id}/edit/{table}','MaintenanceController@edit');
	Route::get('pdf','ReportController@generatePDF');
	Route::get('pie','ReportController@generatePieChart');


    //additional cumulative
    Route::get('webservice/addedCumulativeField','AddedCumulativeFieldController@index');
    Route::get('webservice/addedCumulativeField/{id}','AddedCumulativeFieldController@show');
    Route::post('webservice/addedCumulativeField','AddedCumulativeFieldController@store');
    // Route::post('webservice/addedCumulativeField/update','AddedCumulativeFieldController@edit');
    Route::delete('webservice/addedCumulativeField/{id}','AddedCumulativeFieldController@destroy');




	Route::post('webservice/updateAddedCumulativeField','CumulativeController@edit');
    Route::post('webservice/answerAddedCumulativeField','CumulativeController@answer');
    // Route::get('webservice/getCumulativeRecord', 'CumulativeController@view_cumulative');

    Route::post('webservice/filterStudent','ReportController@filterStudent');
});
