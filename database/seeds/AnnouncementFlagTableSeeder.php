<?php

use Illuminate\Database\Seeder;

class AnnouncementFlagTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('announcement_flag')->delete();
        
        \DB::table('announcement_flag')->insert(array (
            0 => 
            array (
                'id' => 2,
                'announcement_id' => '3',
                'user_id' => '2011000003',
                'created_at' => '2016-05-13 12:12:47',
                'updated_at' => '2016-05-13 12:12:47',
            ),
        ));
        
        
    }
}
