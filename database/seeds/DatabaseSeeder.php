<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
        $this->call('UsersTableSeeder');
        $this->call('AnnouncementTableSeeder');
        $this->call('StudentTableSeeder');
        $this->call('AnnouncementFlagTableSeeder');
        $this->call('ProfessorTableSeeder');
        $this->call('DirectorTableSeeder');
        $this->call('FeedbackTableSeeder');
        $this->call('GuidanceCouncelorTableSeeder');
        $this->call('InboxTableSeeder');
        $this->call('OrganizationTableSeeder');
        $this->call('PasswordResetsTableSeeder');
        $this->call('ReferralTableSeeder');
        $this->call('CollegesTableSeeder');
        $this->call('CoursesTableSeeder');
        $this->call('CumulativeRecordTableSeeder');
        $this->call('AnnouncementPermissionTableSeeder');
        $this->call('FeedbackchatTableSeeder');
        $this->call('NotificationBadgeTableSeeder');
    }
}
