<?php

use Illuminate\Database\Seeder;

class CollegesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('colleges')->delete();
        
        \DB::table('colleges')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Engineering',
                'created_at' => '2016-04-24 16:08:00',
                'updated_at' => '2016-04-24 16:08:00',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Science',
                'created_at' => '2016-04-24 16:08:12',
                'updated_at' => '2016-05-13 10:39:50',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Institute of Information and Computing Sciences',
                'created_at' => '2016-04-24 16:08:22',
                'updated_at' => '2016-05-13 10:41:59',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Commerce',
                'created_at' => '2016-05-13 10:42:25',
                'updated_at' => '2016-05-13 10:42:43',
            ),
        ));
        
        
    }
}
