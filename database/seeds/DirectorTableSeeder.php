<?php

use Illuminate\Database\Seeder;

class DirectorTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('director')->delete();
        
        \DB::table('director')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => '2010000000',
                'created_at' => '2016-04-12 21:32:30',
                'updated_at' => '2016-04-12 21:32:30',
            ),
        ));
        
        
    }
}
