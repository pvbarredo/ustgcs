<?php

use Illuminate\Database\Seeder;

class AnnouncementPermissionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('announcement_permission')->delete();
        
        \DB::table('announcement_permission')->insert(array (
            0 => 
            array (
                'id' => 3,
                'announcement_id' => 3,
                'year' => '',
                'college' => '',
                'course' => '',
                'professor' => 0,
                'guidance' => 1,
                'director' => 1,
                'is_all' => 0,
                'created_at' => '2016-05-13 12:11:03',
                'updated_at' => '2016-05-13 12:11:03',
            ),
            1 => 
            array (
                'id' => 4,
                'announcement_id' => 4,
                'year' => '1',
                'college' => '3',
                'course' => '',
                'professor' => 0,
                'guidance' => 0,
                'director' => 1,
                'is_all' => 0,
                'created_at' => '2016-05-13 12:15:48',
                'updated_at' => '2016-05-13 12:15:48',
            ),
            2 => 
            array (
                'id' => 5,
                'announcement_id' => 5,
                'year' => '2',
                'college' => '3',
                'course' => '3,2',
                'professor' => 0,
                'guidance' => 1,
                'director' => 0,
                'is_all' => 0,
                'created_at' => '2016-05-13 12:25:23',
                'updated_at' => '2016-05-13 12:25:23',
            ),
            3 => 
            array (
                'id' => 6,
                'announcement_id' => 6,
                'year' => '',
                'college' => '',
                'course' => '1',
                'professor' => 0,
                'guidance' => 1,
                'director' => 1,
                'is_all' => 0,
                'created_at' => '2016-05-13 12:33:50',
                'updated_at' => '2016-05-13 12:33:50',
            ),
            4 => 
            array (
                'id' => 7,
                'announcement_id' => 7,
                'year' => '2',
                'college' => '3',
                'course' => '3',
                'professor' => 0,
                'guidance' => 1,
                'director' => 0,
                'is_all' => 0,
                'created_at' => '2016-05-13 12:37:55',
                'updated_at' => '2016-05-13 12:37:55',
            ),
            5 => 
            array (
                'id' => 8,
                'announcement_id' => 8,
                'year' => '1',
                'college' => '',
                'course' => '2',
                'professor' => 0,
                'guidance' => 1,
                'director' => 0,
                'is_all' => 0,
                'created_at' => '2016-05-13 18:15:00',
                'updated_at' => '2016-05-13 18:15:00',
            ),
            6 => 
            array (
                'id' => 9,
                'announcement_id' => 9,
                'year' => '1,2',
                'college' => '3',
                'course' => '2,3',
                'professor' => 0,
                'guidance' => 1,
                'director' => 0,
                'is_all' => 0,
                'created_at' => '2016-05-13 18:16:13',
                'updated_at' => '2016-05-13 18:16:13',
            ),
        ));
        
        
    }
}
