<?php

use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('student')->delete();
        
        \DB::table('student')->insert(array (
            0 => 
            array (
                'id' => 2,
                'code' => '1234',
                'year' => 3,
                'college_id' => 2,
                'course_id' => 4,
                'created_at' => '2016-04-24 16:43:06',
                'updated_at' => '2016-04-24 16:43:06',
            ),
            1 => 
            array (
                'id' => 3,
                'code' => '2012000001',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:43',
                'updated_at' => '2016-05-13 11:30:43',
            ),
            2 => 
            array (
                'id' => 4,
                'code' => '2012000002',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:43',
                'updated_at' => '2016-05-13 11:30:43',
            ),
            3 => 
            array (
                'id' => 5,
                'code' => '2012000003',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:43',
                'updated_at' => '2016-05-13 11:30:43',
            ),
            4 => 
            array (
                'id' => 6,
                'code' => '2012000004',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:44',
                'updated_at' => '2016-05-13 11:30:44',
            ),
            5 => 
            array (
                'id' => 7,
                'code' => '2012000005',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:44',
                'updated_at' => '2016-05-13 11:30:44',
            ),
            6 => 
            array (
                'id' => 8,
                'code' => '2012000006',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:44',
                'updated_at' => '2016-05-13 11:30:44',
            ),
            7 => 
            array (
                'id' => 9,
                'code' => '2012000007',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:44',
                'updated_at' => '2016-05-13 11:30:44',
            ),
            8 => 
            array (
                'id' => 10,
                'code' => '2012000008',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:44',
                'updated_at' => '2016-05-13 11:30:44',
            ),
            9 => 
            array (
                'id' => 11,
                'code' => '2012000009',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:45',
                'updated_at' => '2016-05-13 11:30:45',
            ),
            10 => 
            array (
                'id' => 12,
                'code' => '2012000010',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:45',
                'updated_at' => '2016-05-13 11:30:45',
            ),
            11 => 
            array (
                'id' => 13,
                'code' => '2012000011',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:45',
                'updated_at' => '2016-05-13 11:30:45',
            ),
            12 => 
            array (
                'id' => 14,
                'code' => '2012000012',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:45',
                'updated_at' => '2016-05-13 11:30:45',
            ),
            13 => 
            array (
                'id' => 15,
                'code' => '2012000013',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:45',
                'updated_at' => '2016-05-13 11:30:45',
            ),
            14 => 
            array (
                'id' => 16,
                'code' => '2012000014',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:45',
                'updated_at' => '2016-05-13 11:30:45',
            ),
            15 => 
            array (
                'id' => 17,
                'code' => '2012000015',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:46',
                'updated_at' => '2016-05-13 11:30:46',
            ),
            16 => 
            array (
                'id' => 18,
                'code' => '2012000016',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:46',
                'updated_at' => '2016-05-13 11:30:46',
            ),
            17 => 
            array (
                'id' => 19,
                'code' => '2012000017',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:46',
                'updated_at' => '2016-05-13 11:30:46',
            ),
            18 => 
            array (
                'id' => 20,
                'code' => '2012000018',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:46',
                'updated_at' => '2016-05-13 11:30:46',
            ),
            19 => 
            array (
                'id' => 21,
                'code' => '2012000019',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:47',
                'updated_at' => '2016-05-13 11:30:47',
            ),
            20 => 
            array (
                'id' => 22,
                'code' => '2012000020',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:47',
                'updated_at' => '2016-05-13 11:30:47',
            ),
            21 => 
            array (
                'id' => 23,
                'code' => '2012000021',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:47',
                'updated_at' => '2016-05-13 11:30:47',
            ),
            22 => 
            array (
                'id' => 24,
                'code' => '2012000022',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:47',
                'updated_at' => '2016-05-13 11:30:47',
            ),
            23 => 
            array (
                'id' => 25,
                'code' => '2012000023',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:47',
                'updated_at' => '2016-05-13 11:30:47',
            ),
            24 => 
            array (
                'id' => 26,
                'code' => '2012000024',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:48',
                'updated_at' => '2016-05-13 11:30:48',
            ),
            25 => 
            array (
                'id' => 27,
                'code' => '2012000025',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:48',
                'updated_at' => '2016-05-13 11:30:48',
            ),
            26 => 
            array (
                'id' => 28,
                'code' => '2012000026',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:48',
                'updated_at' => '2016-05-13 11:30:48',
            ),
            27 => 
            array (
                'id' => 29,
                'code' => '2012000027',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:48',
                'updated_at' => '2016-05-13 11:30:48',
            ),
            28 => 
            array (
                'id' => 30,
                'code' => '2012000028',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:48',
                'updated_at' => '2016-05-13 11:30:48',
            ),
            29 => 
            array (
                'id' => 31,
                'code' => '2012000029',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:48',
                'updated_at' => '2016-05-13 11:30:48',
            ),
            30 => 
            array (
                'id' => 32,
                'code' => '2012000030',
                'year' => 1,
                'college_id' => 3,
                'course_id' => 2,
                'created_at' => '2016-05-13 11:30:49',
                'updated_at' => '2016-05-13 11:30:49',
            ),
            31 => 
            array (
                'id' => 33,
                'code' => '2012000031',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:33',
                'updated_at' => '2016-05-13 11:34:33',
            ),
            32 => 
            array (
                'id' => 34,
                'code' => '2012000032',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:33',
                'updated_at' => '2016-05-13 11:34:33',
            ),
            33 => 
            array (
                'id' => 35,
                'code' => '2012000033',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:34',
                'updated_at' => '2016-05-13 11:34:34',
            ),
            34 => 
            array (
                'id' => 36,
                'code' => '2012000034',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:34',
                'updated_at' => '2016-05-13 11:34:34',
            ),
            35 => 
            array (
                'id' => 37,
                'code' => '2012000035',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:34',
                'updated_at' => '2016-05-13 11:34:34',
            ),
            36 => 
            array (
                'id' => 38,
                'code' => '2012000036',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:34',
                'updated_at' => '2016-05-13 11:34:34',
            ),
            37 => 
            array (
                'id' => 39,
                'code' => '2012000037',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:34',
                'updated_at' => '2016-05-13 11:34:34',
            ),
            38 => 
            array (
                'id' => 40,
                'code' => '2012000038',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:35',
                'updated_at' => '2016-05-13 11:34:35',
            ),
            39 => 
            array (
                'id' => 41,
                'code' => '2012000039',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:35',
                'updated_at' => '2016-05-13 11:34:35',
            ),
            40 => 
            array (
                'id' => 42,
                'code' => '2012000040',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:35',
                'updated_at' => '2016-05-13 11:34:35',
            ),
            41 => 
            array (
                'id' => 43,
                'code' => '2012000041',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:36',
                'updated_at' => '2016-05-13 11:34:36',
            ),
            42 => 
            array (
                'id' => 44,
                'code' => '2012000042',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:36',
                'updated_at' => '2016-05-13 11:34:36',
            ),
            43 => 
            array (
                'id' => 45,
                'code' => '2012000043',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:37',
                'updated_at' => '2016-05-13 11:34:37',
            ),
            44 => 
            array (
                'id' => 46,
                'code' => '2012000044',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:37',
                'updated_at' => '2016-05-13 11:34:37',
            ),
            45 => 
            array (
                'id' => 47,
                'code' => '2012000045',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:37',
                'updated_at' => '2016-05-13 11:34:37',
            ),
            46 => 
            array (
                'id' => 48,
                'code' => '2012000046',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:37',
                'updated_at' => '2016-05-13 11:34:37',
            ),
            47 => 
            array (
                'id' => 49,
                'code' => '2012000047',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:37',
                'updated_at' => '2016-05-13 11:34:37',
            ),
            48 => 
            array (
                'id' => 50,
                'code' => '2012000048',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:38',
                'updated_at' => '2016-05-13 11:34:38',
            ),
            49 => 
            array (
                'id' => 51,
                'code' => '2012000049',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:38',
                'updated_at' => '2016-05-13 11:34:38',
            ),
            50 => 
            array (
                'id' => 52,
                'code' => '2012000050',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:38',
                'updated_at' => '2016-05-13 11:34:38',
            ),
            51 => 
            array (
                'id' => 53,
                'code' => '2012000051',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:38',
                'updated_at' => '2016-05-13 11:34:38',
            ),
            52 => 
            array (
                'id' => 54,
                'code' => '2012000052',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:38',
                'updated_at' => '2016-05-13 11:34:38',
            ),
            53 => 
            array (
                'id' => 55,
                'code' => '2012000053',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:39',
                'updated_at' => '2016-05-13 11:34:39',
            ),
            54 => 
            array (
                'id' => 56,
                'code' => '2012000054',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:39',
                'updated_at' => '2016-05-13 11:34:39',
            ),
            55 => 
            array (
                'id' => 57,
                'code' => '2012000055',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:39',
                'updated_at' => '2016-05-13 11:34:39',
            ),
            56 => 
            array (
                'id' => 58,
                'code' => '2012000056',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:39',
                'updated_at' => '2016-05-13 11:34:39',
            ),
            57 => 
            array (
                'id' => 59,
                'code' => '2012000057',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:39',
                'updated_at' => '2016-05-13 11:34:39',
            ),
            58 => 
            array (
                'id' => 60,
                'code' => '2012000058',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:40',
                'updated_at' => '2016-05-13 11:34:40',
            ),
            59 => 
            array (
                'id' => 61,
                'code' => '2012000059',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:40',
                'updated_at' => '2016-05-13 11:34:40',
            ),
            60 => 
            array (
                'id' => 62,
                'code' => '2012000060',
                'year' => 2,
                'college_id' => 3,
                'course_id' => 3,
                'created_at' => '2016-05-13 11:34:40',
                'updated_at' => '2016-05-13 11:34:40',
            ),
        ));
        
        
    }
}
