<?php

use Illuminate\Database\Seeder;

class ReferralTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('referral')->delete();
        
        \DB::table('referral')->insert(array (
            0 => 
            array (
                'id' => 7,
                'professor_id' => 2009000001,
                'college_id' => '1',
                'student_id' => '2012000006',
                'message' => 'Absenteeism/Tardiness',
                'description' => '2 weeks absent',
                'is_read' => 0,
                'created_at' => '2016-05-13 14:00:08',
                'updated_at' => '2016-05-13 14:00:08',
            ),
            1 => 
            array (
                'id' => 8,
                'professor_id' => 2009000001,
                'college_id' => '1',
                'student_id' => '2012000002',
                'message' => 'Course Indecision',
                'description' => 'He\'s not motivated anymore to go to school',
                'is_read' => 0,
                'created_at' => '2016-05-13 14:01:41',
                'updated_at' => '2016-05-13 14:01:41',
            ),
            2 => 
            array (
                'id' => 9,
                'professor_id' => 2009000001,
                'college_id' => '1',
                'student_id' => '2012000005',
                'message' => 'Absenteeism/Tardiness',
                'description' => 'He\'s FA already',
                'is_read' => 0,
                'created_at' => '2016-05-13 14:02:13',
                'updated_at' => '2016-05-13 14:02:13',
            ),
            3 => 
            array (
                'id' => 10,
                'professor_id' => 2009000001,
                'college_id' => '1',
                'student_id' => '2012000010',
                'message' => 'Course Indecision',
                'description' => 'not motivated',
                'is_read' => 0,
                'created_at' => '2016-05-13 14:04:40',
                'updated_at' => '2016-05-13 14:04:40',
            ),
            4 => 
            array (
                'id' => 11,
                'professor_id' => 2009000001,
                'college_id' => '1',
                'student_id' => '2012000002',
                'message' => 'Absenteeism/Tardiness',
                'description' => '1 week absent',
                'is_read' => 0,
                'created_at' => '2016-05-13 18:06:16',
                'updated_at' => '2016-05-13 18:06:16',
            ),
        ));
        
        
    }
}
