<?php

use Illuminate\Database\Seeder;

class AnnouncementTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('announcement')->delete();
        
        \DB::table('announcement')->insert(array (
            0 => 
            array (
                'id' => 3,
                'title' => 'Hi Guidance Counselors!',
                'message' => 'Welcome to the our new website!',
                'created_by' => '2010000001',
                'created_at' => '2016-05-13 12:11:03',
                'updated_at' => '2016-05-13 12:11:03',
            ),
            1 => 
            array (
                'id' => 4,
                'title' => 'Hi 1st year IICS!',
                'message' => 'Hi 1st year IICS!',
                'created_by' => '2010000001',
                'created_at' => '2016-05-13 12:15:48',
                'updated_at' => '2016-05-13 12:15:48',
            ),
            2 => 
            array (
                'id' => 5,
                'title' => 'Hi 2nd year CS IICS Students!',
                'message' => 'Welcome cs iics students!',
                'created_by' => '2011000001',
                'created_at' => '2016-05-13 12:25:23',
                'updated_at' => '2016-05-13 12:25:23',
            ),
            3 => 
            array (
                'id' => 6,
                'title' => 'Hi eng counselors!',
                'message' => 'welcome',
                'created_by' => '2010000001',
                'created_at' => '2016-05-13 12:33:50',
                'updated_at' => '2016-05-13 12:33:50',
            ),
            4 => 
            array (
                'id' => 7,
            'title' => 'Hi 2nd year CS IICS Students! (Part2)',
            'message' => 'welcome (part2)',
                'created_by' => '2011000001',
                'created_at' => '2016-05-13 12:37:55',
                'updated_at' => '2016-05-13 12:37:55',
            ),
            5 => 
            array (
                'id' => 8,
                'title' => 'Hi to all 1st year IS',
                'message' => 'welcome',
                'created_by' => '2011000001',
                'created_at' => '2016-05-13 18:15:00',
                'updated_at' => '2016-05-13 18:15:00',
            ),
            6 => 
            array (
                'id' => 9,
                'title' => 'Final exam',
                'message' => 'tomorrow',
                'created_by' => '2011000001',
                'created_at' => '2016-05-13 18:16:13',
                'updated_at' => '2016-05-13 18:16:13',
            ),
        ));
        
        
    }
}
