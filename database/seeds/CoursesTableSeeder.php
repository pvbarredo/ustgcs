<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('courses')->delete();
        
        \DB::table('courses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'BS Civil Engineering',
                'max_years' => '5',
                'college_id' => 1,
                'created_at' => '2016-04-24 16:08:47',
                'updated_at' => '2016-04-24 16:08:47',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'BS Information System',
                'max_years' => '4',
                'college_id' => 3,
                'created_at' => '2016-04-24 16:09:02',
                'updated_at' => '2016-04-24 16:09:02',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'BS Computer Science',
                'max_years' => '4',
                'college_id' => 3,
                'created_at' => '2016-04-24 16:09:23',
                'updated_at' => '2016-04-24 16:09:23',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'BS Biology',
                'max_years' => '4',
                'college_id' => 2,
                'created_at' => '2016-04-24 16:09:56',
                'updated_at' => '2016-04-24 16:09:56',
            ),
        ));
        
        
    }
}
