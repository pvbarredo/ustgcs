<?php

use Illuminate\Database\Seeder;

class ProfessorTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('professor')->delete();
        
        \DB::table('professor')->insert(array (
            0 => 
            array (
                'id' => 3,
                'code' => '784512',
                'college_id' => 2,
                'created_at' => '2016-05-03 21:23:08',
                'updated_at' => '2016-05-03 21:23:08',
            ),
            1 => 
            array (
                'id' => 4,
                'code' => '2009000001',
                'college_id' => 3,
                'created_at' => '2016-05-13 13:22:30',
                'updated_at' => '2016-05-13 13:22:30',
            ),
            2 => 
            array (
                'id' => 5,
                'code' => '2009000002',
                'college_id' => 3,
                'created_at' => '2016-05-13 13:22:30',
                'updated_at' => '2016-05-13 13:22:30',
            ),
            3 => 
            array (
                'id' => 6,
                'code' => '2009000003',
                'college_id' => 3,
                'created_at' => '2016-05-13 13:23:50',
                'updated_at' => '2016-05-13 13:23:50',
            ),
        ));
        
        
    }
}
