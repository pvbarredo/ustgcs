<?php

use Illuminate\Database\Seeder;

class GuidanceCouncelorTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('guidance_councelor')->delete();
        
        \DB::table('guidance_councelor')->insert(array (
            0 => 
            array (
                'id' => 2,
                'code' => '654322',
                'college_id' => 1,
                'created_at' => '2016-04-24 16:48:53',
                'updated_at' => '2016-04-24 16:48:53',
            ),
            1 => 
            array (
                'id' => 3,
                'code' => '2011000001',
                'college_id' => 3,
                'created_at' => '2016-05-13 11:44:47',
                'updated_at' => '2016-05-13 11:44:47',
            ),
            2 => 
            array (
                'id' => 4,
                'code' => '2011000002',
                'college_id' => 3,
                'created_at' => '2016-05-13 11:44:47',
                'updated_at' => '2016-05-13 11:44:47',
            ),
            3 => 
            array (
                'id' => 5,
                'code' => '2011000003',
                'college_id' => 1,
                'created_at' => '2016-05-13 11:44:47',
                'updated_at' => '2016-05-13 11:44:47',
            ),
            4 => 
            array (
                'id' => 6,
                'code' => '2011000004',
                'college_id' => 1,
                'created_at' => '2016-05-13 11:44:48',
                'updated_at' => '2016-05-13 11:44:48',
            ),
            5 => 
            array (
                'id' => 7,
                'code' => '2011000005',
                'college_id' => 1,
                'created_at' => '2016-05-13 11:44:48',
                'updated_at' => '2016-05-13 11:44:48',
            ),
            6 => 
            array (
                'id' => 8,
                'code' => '2011000006',
                'college_id' => 1,
                'created_at' => '2016-05-13 11:44:48',
                'updated_at' => '2016-05-13 11:44:48',
            ),
        ));
        
        
    }
}
