<?php

use Illuminate\Database\Seeder;

class NotificationBadgeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('notification_badge')->delete();
        
        \DB::table('notification_badge')->insert(array (
            0 => 
            array (
                'id' => 6,
                'user_code' => '1234',
                'announcement' => 0,
                'inbox' => NULL,
                'created_at' => '2016-05-13 11:05:42',
                'updated_at' => '2016-05-13 11:05:42',
            ),
            1 => 
            array (
                'id' => 7,
                'user_code' => '90909090',
                'announcement' => 0,
                'inbox' => NULL,
                'created_at' => '2016-05-13 11:11:46',
                'updated_at' => '2016-05-13 11:11:46',
            ),
            2 => 
            array (
                'id' => 8,
                'user_code' => '2012000001',
                'announcement' => 3,
                'inbox' => NULL,
                'created_at' => '2016-05-13 11:36:17',
                'updated_at' => '2016-05-13 18:18:10',
            ),
            3 => 
            array (
                'id' => 9,
                'user_code' => '2011000001',
                'announcement' => 6,
                'inbox' => NULL,
                'created_at' => '2016-05-13 11:45:44',
                'updated_at' => '2016-05-13 18:16:13',
            ),
            4 => 
            array (
                'id' => 10,
                'user_code' => '2010000001',
                'announcement' => 3,
                'inbox' => NULL,
                'created_at' => '2016-05-13 12:07:27',
                'updated_at' => '2016-05-13 12:33:50',
            ),
            5 => 
            array (
                'id' => 11,
                'user_code' => '2011000003',
                'announcement' => 3,
                'inbox' => NULL,
                'created_at' => '2016-05-13 12:12:22',
                'updated_at' => '2016-05-13 12:38:12',
            ),
            6 => 
            array (
                'id' => 12,
                'user_code' => '2012000031',
                'announcement' => 0,
                'inbox' => NULL,
                'created_at' => '2016-05-13 12:16:37',
                'updated_at' => '2016-05-13 12:16:37',
            ),
            7 => 
            array (
                'id' => 13,
                'user_code' => '2012000054',
                'announcement' => 0,
                'inbox' => NULL,
                'created_at' => '2016-05-13 12:17:35',
                'updated_at' => '2016-05-13 12:17:35',
            ),
            8 => 
            array (
                'id' => 14,
                'user_code' => '2012000025',
                'announcement' => 1,
                'inbox' => NULL,
                'created_at' => '2016-05-13 12:18:09',
                'updated_at' => '2016-05-13 12:18:09',
            ),
            9 => 
            array (
                'id' => 15,
                'user_code' => '2012000033',
                'announcement' => 2,
                'inbox' => NULL,
                'created_at' => '2016-05-13 12:26:52',
                'updated_at' => '2016-05-13 18:15:38',
            ),
            10 => 
            array (
                'id' => 16,
                'user_code' => '2012000003',
                'announcement' => 3,
                'inbox' => NULL,
                'created_at' => '2016-05-13 13:07:59',
                'updated_at' => '2016-05-14 00:12:20',
            ),
            11 => 
            array (
                'id' => 17,
                'user_code' => '2012000002',
                'announcement' => 1,
                'inbox' => NULL,
                'created_at' => '2016-05-13 13:26:11',
                'updated_at' => '2016-05-13 13:26:11',
            ),
            12 => 
            array (
                'id' => 18,
                'user_code' => '2012000010',
                'announcement' => 3,
                'inbox' => NULL,
                'created_at' => '2016-05-13 18:16:33',
                'updated_at' => '2016-05-13 18:16:33',
            ),
            13 => 
            array (
                'id' => 19,
                'user_code' => '2012000035',
                'announcement' => 3,
                'inbox' => NULL,
                'created_at' => '2016-05-13 18:16:48',
                'updated_at' => '2016-05-13 18:16:48',
            ),
            14 => 
            array (
                'id' => 20,
                'user_code' => '2012000004',
                'announcement' => 3,
                'inbox' => NULL,
                'created_at' => '2016-05-13 23:52:00',
                'updated_at' => '2016-05-13 23:52:00',
            ),
            15 => 
            array (
                'id' => 21,
                'user_code' => '654322',
                'announcement' => 6,
                'inbox' => NULL,
                'created_at' => '2016-05-14 02:39:29',
                'updated_at' => '2016-05-14 02:39:29',
            ),
        ));
        
        
    }
}
