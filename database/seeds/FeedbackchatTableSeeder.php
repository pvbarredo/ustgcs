<?php

use Illuminate\Database\Seeder;

class FeedbackchatTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('feedbackchat')->delete();
        
        \DB::table('feedbackchat')->insert(array (
            0 => 
            array (
                'id' => 1,
                'referral_id' => 11,
                'user_id' => 2011000003,
                'user_status' => 'guidance',
                'message' => 'Noted',
                'created_at' => '2016-05-13 18:07:55',
                'updated_at' => '2016-05-13 18:07:55',
            ),
            1 => 
            array (
                'id' => 2,
                'referral_id' => 11,
                'user_id' => 2011000003,
                'user_status' => 'guidance',
                'message' => 'already talkd to him',
                'created_at' => '2016-05-13 18:08:08',
                'updated_at' => '2016-05-13 18:08:08',
            ),
            2 => 
            array (
                'id' => 3,
                'referral_id' => 11,
                'user_id' => 2009000001,
                'user_status' => 'professor',
                'message' => 'Okay',
                'created_at' => '2016-05-13 18:08:46',
                'updated_at' => '2016-05-13 18:08:46',
            ),
        ));
        
        
    }
}
