<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferralTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('referral', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('professor_id');
			$table->string('college_id');
			$table->string('student_id');
			$table->string('message');
			$table->string('description');
			$table->boolean('is_read');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('referral');
	}

}
