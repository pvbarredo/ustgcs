<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCumulativeRecordAdditionalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cumulative_record_additional', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('user_code');
			$table->string('field');
			$table->string('value');
			$table->string('created_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cumulative_record_additional');
	}

}
