<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_permission', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('announcement_id');
            $table->string('year')->nullable();
            $table->string('college')->nullable();
            $table->string('course')->nullable();
            $table->boolean('professor');
            $table->boolean('guidance');
            $table->boolean('director');
            $table->boolean('is_all');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('announcement_permission');
    }
}
