<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCumulativeRecordTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cumulative_record', function(Blueprint $table)
		{
			$table->increments('cum_id');
			$table->integer('student_id');
			$table->string('auxiliary_name')->nullable();
			$table->string('nickname')->nullable();
			$table->string('nationality');
			$table->string('religion');
			$table->string('email');
			$table->string('curr_address');
			$table->string('curr_city');
			$table->string('perm_address');
			$table->string('perm_city');
			$table->string('mobile_no');
			$table->string('home_phone_no')->nullable();
			$table->string('living_arrangement');
			$table->string('acad_health_prob');
			$table->string('curricular_health_prob');
			$table->boolean('psychiatric_help');
			$table->boolean('counseling');
			$table->string('father_name');
			$table->string('father_address');
			$table->string('father_occupation');
			$table->string('father_contact');
			$table->string('mother_name');
			$table->string('mother_address');
			$table->string('mother_occupation');
			$table->string('mother_contact');
			$table->string('parent_marital_status');
			$table->string('parent_marital_status_spec1')->nullable();
			$table->string('parent_marital_status_spec2')->nullable();
			$table->string('parent_marital_status_other')->nullable();
			$table->string('guardian_name');
			$table->string('guardian_relation');
			$table->string('guardian_contact');
			$table->string('family_life_perception');
			$table->string('close_family');
			$table->string('motivate_factor');
			$table->string('counselor_help');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cumulative_record');
	}

}
