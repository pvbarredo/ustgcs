<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code')->unique();
			$table->string('firstname');
			$table->string('middlename');
			$table->string('lastname');
			$table->string('email');
			$table->string('status');
			$table->boolean('is_active');
			$table->boolean('is_first');
			$table->string('password');
			$table->integer('created_by');
			$table->string('creation_mode');
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
