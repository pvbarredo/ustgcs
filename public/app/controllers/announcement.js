var app = angular.module('announcement', ['angularMoment', 'oitozero.ngSweetAlert', 'url.constant']);


app.controller('announcementController', function($scope, $http, API_URL, $filter) {
    var collegeCheckbox = [],
        courseCheckbox = [];

    getAllAnnouncementForUser = function() {

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getAllAnnouncementForUser'
        }).
        success(function(data) {
            console.log("success", data);

            $scope.announcements = data.announcements;
            $scope.cacheAnnouncements = data.announcements;
            $scope.total_unread = data.total_unread;
            $scope.flag = data.flag;
        }).
        error(function(data) {
            // swal("Error!", "Please reload the page!", "error");
        });
    }
    getAllAnnouncementForUser();

    getNotification = function() {

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
        success(function(data) {
            console.log(data);
            $scope.notification = data;
        }).
        error(function(data) {
            // swal("Error!", "Please reload the page!", "error");
        });
    }
    getNotification();

    getAuthUser = function() {
        $http.get(API_URL + "webservice/getAuthUser")
            .success(function(response) {
                console.log("auth", response);
                $scope.auth = response;

                if ($scope.auth.status === 'student') {
                    checkNonComplianceNotification();
                }

            }).error(function(response) {
                alert('There is something wrong');
            });
    }
    getAuthUser();


    checkNonComplianceNotification = function() {
        $http({
            method: 'GET',
            url: API_URL + 'webservice/checkNonComplianceNotification/' + $scope.auth.code
        }).
        success(function(data) {
            if (data === 'true') {
                swal("Non Compliance Notification!", "Please complete your compliance report", "warning");
            }

        }).
        error(function(data) {
            // swal("Error!", "Please reload the page!", "error");
        });

    }



    getInfrastructure = function() {
        $http({
            method: 'GET',
            url: API_URL + 'webservice/getInfrastructure'
        }).
        success(function(data) {
            console.log(data);
            $scope.infrastructures = data;
        }).
        error(function(data) {
            // swal("Error!", "Please reload the page!", "error");
        });
    }
    getInfrastructure();

    getColleges = function() {
        $http({
            method: 'GET',
            url: API_URL + 'webservice/collegeCont/getColleges'
        }).
        success(function(data) {
            console.log('colleges', data);
            $scope.colleges = data;
        }).
        error(function(data) {
            // swal("Error!", "Please reload the page!", "error");
        });
    }
    getColleges();

    getCourses = function() {
        $http({
            method: 'GET',
            url: API_URL + 'webservice/collegeCont/getCourses'
        }).
        success(function(data) {
            console.log(data);
            $scope.courses = data;
        }).
        error(function(data) {
            // swal("Error!", "Please reload the page!", "error");
        });
    }
    getCourses();


    // $scope.searchChanged = function(search){

    //     if(search){
    //          $scope.announcements = $filter('filter')($scope.cacheAnnouncements, search);
    //     }else{
    //         $scope.announcements = $scope.cacheAnnouncements;
    //     }



    // }
    $scope.allCheckboxChanged = function() {
        var checkboxes = $scope.checkboxes;
        console.log(checkboxes);
        var comps = $('.receiver-notAll');
        if (checkboxes.all) {

            comps.prop('disabled', true);
            for (var i = 0; i < comps.length; i++) {
                comps[i].checked = true;
            };

        } else {
            comps.prop('disabled', false);
            for (var i = 0; i < comps.length; i++) {
                comps[i].checked = false;
            };

        }
    }

    $scope.newAnnouncement = function() {
        console.log('clicked');
        $('#myModal').modal('show');
    }

    $scope.setCollegeCheckbox = function(college) {

        var isChecked = false;
        angular.forEach(collegeCheckbox, function(values) {
            if (!isChecked) {
                if (college.id === values) {
                    isChecked = true;
                }
            }

        })

        if (isChecked) {
            var index = collegeCheckbox.indexOf(college.id);
            collegeCheckbox.splice(index, 1);
        } else {
            collegeCheckbox.push(college.id);
        }

        // console.log(college);
        // console.log(collegeCheckbox);
    }
    $scope.setCourseCheckbox = function(course) {
        var isChecked = false;
        angular.forEach(courseCheckbox, function(values) {
            if (!isChecked) {
                if (course.id === values) {
                    isChecked = true;
                }
            }

        })

        if (isChecked) {
            var index = courseCheckbox.indexOf(course.id);
            courseCheckbox.splice(index, 1);
        } else {
            courseCheckbox.push(course.id);
        }

        console.log(course);
        console.log(courseCheckbox.join(','));
    }

    $scope.createAnnouncement = function() {
        var title = ($scope.title) ? $scope.title : '',
            message = ($scope.message) ? $scope.message : '',
            checkboxes = $scope.checkboxes;

        var form = {
            title: title,
            message: message
        };

        if(checkboxes == undefined){
            var checkboxes = {};
            checkboxes.first = true;
            checkboxes.second = true;
            checkboxes.third = true;
            checkboxes.fourth = true;
            checkboxes.fifth = true;
        }

        if (checkboxes.all) {
            form.isCheckedAll = true;
        } else {
            form.isCheckedAll = false;
            var yearCheckbox = [];
            if (checkboxes.first) {
                yearCheckbox.push(1);
            }
            if (checkboxes.second) {
                yearCheckbox.push(2);
            }
            if (checkboxes.third) {
                yearCheckbox.push(3);
            }
            if (checkboxes.fourth) {
                yearCheckbox.push(4);
            }
            if (checkboxes.fifth) {
                yearCheckbox.push(5);
            }

            form.year = yearCheckbox.join(',');

            form.college = collegeCheckbox.join(',');
            form.course = courseCheckbox.join(',');
            form.professor = checkboxes.professor;
            form.guidance = checkboxes.guidance;
            form.director = checkboxes.director;

        }


        console.log(form);

        if (title !== '' && message !== '') {
            $http({
                method: 'POST',
                url: API_URL + 'webservice/newAnnouncement',
                data: form
            }).
            success(function(data) {
                console.log(data);

                $('#myModal').modal('hide');
                $scope.title = '';
                $scope.message = '';
                swal({
                        title: "Success!",
                        text: "You created an announcement successfully!",
                        type: "success",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Okay",
                        closeOnConfirm: false
                    },
                    function() {
                        location.reload();
                    });



            }).
            error(function(data) {
                // swal("Error!", "Please reload the page!", "error");
            });
        }

    }
});

var appView = angular.module('viewAnnouncementController', ['oitozero.ngSweetAlert', 'url.constant'])
appView.controller('viewAnnouncementController', function($scope, $http, API_URL) {

    getNotification = function() {

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
        success(function(data) {
            console.log(data);
            $scope.notification = data;
        }).
        error(function(data) {
            // swal("Error!", "Please reload the page!", "error");
        });
    }
    getNotification();

});