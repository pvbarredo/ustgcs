var app = angular.module('inbox', ['angucomplete-alt', 'angularMoment', 'oitozero.ngSweetAlert', 'url.constant']);
app.controller('inboxController', function($scope, $http, API_URL) {
    getNotification = function() {

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
        success(function(data) {
            console.log(data);
            $scope.notification = data;
        }).
        error(function(data) {
            console.log(data);
            alert('There is something wrong');
        });
    }
    getNotification();

    getMessages = function() {
        $http.get(API_URL + "webservice/getMessages")
            .success(function(response) {
                $scope.userMessages = response;
                console.log($scope.userMessages);
            });
    }
    getMessages();
    $scope.newMessageClicked = function() {
        $('#messageDescription').hide();
        $('#createMessageWindow').modal('show');
        $scope.$broadcast('angucomplete-alt:clearInput', 'search_student');
    }


    $scope.createMessageClicked = function() {


        if ($scope.message == null) {
            alert('Please enter message');
            return;
        }
        if ($scope.message.title == null) {
            alert('Please enter title');
            return;
        }

        if ($scope.message.message == null) {
            alert('Please enter message');
            return;

        }
        $scope.message.student = $scope.selected;
        console.log('message', $scope.message);

        $http({
            method: 'POST',
            url: API_URL + 'webservice/newMessage',
            data: $scope.message
        }).
        success(function(data) {
            console.log(data);
            $('#createMessageWindow').modal('hide');
            getMessages();
            alert('SUCCESS CREATION');

        }).
        error(function(data) {
            console.log(data);
            alert(data);
        });

    }

    $scope.student = function(selected) {
        if (selected) {
            $('#messageDescription').show();
            $scope.selected = selected.description;
        } else {
            $('#messageDescription').hide();
        }
    }

    $scope.sendEmailReply = function() {

        if (!$scope.replyMessage) {
            swal("Error!", "Please put a message in reply", "error");
            return;
        }

        var reply = {
            inbox_id: $scope.viewMessage.id,
            sender_code: $scope.auth.code,
            status: $scope.auth.status,
            message: $scope.replyMessage
        }

        $http({
            method: 'POST',
            url: API_URL + 'webservice/sendEmailReply',
            data: reply
        }).
        success(function(data) {
            swal("Done", "Message Sent");
            getEmailReplies($scope.viewMessage.id);
        }).
        error(function(data) {
            console.log(data);
            alert('There is something wrong');
        });

        
    }



    getEmailReplies = function(inbox_id) {
        $http({
            method: 'GET',
            url: API_URL + 'webservice/getEmailReplies/' + inbox_id
        }).
        success(function(data) {

            $scope.email_replies = [];
            
            _.each(data , function(records){
                
                var email_reply = {
                    reply : records,
                    isCurrentUser: ($scope.auth.code === records.sender_code) ? true : false,
                    fullName : records.firstname + ' ' + records.lastname  
                }

                $scope.email_replies.push(email_reply);
            });

                
        }).
        error(function(data) {
            alert(data);
        });
    }

    getAuthUser = function() {
        $http.get(API_URL + "webservice/getAuthUser")
            .success(function(response) {
                console.log("auth", response);
                $scope.auth = response;

                // if ($scope.auth.status === 'student') {
                //     checkNonComplianceNotification();
                // }

            }).error(function(response) {
                alert('There is something wrong');
            });
    }
    getAuthUser();

    $scope.viewMessageClicked = function(message) {
        $('#viewMessage').modal('show');
        $scope.viewMessage = message;

        $http.get(API_URL + "webservice/updateInboxNotification/" + message.id)
            .success(function(response) {
                console.log('SUCCESS');
            });

         getEmailReplies(message.id);   

    }
    $('#viewMessage').on('hidden.bs.modal', function() {
        location.reload();
    })

});