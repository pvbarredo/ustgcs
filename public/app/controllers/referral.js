var app = angular.module('referral', ['angucomplete-alt','angularMoment','oitozero.ngSweetAlert','url.constant']);

app.controller('referralController', function($scope, $http, API_URL) {
	$('#referral-form').hide();



  getAuthUser = function(){
        $http.get(API_URL + "webservice/getAuthUser")
        .success(function(response) {
            console.log("auth",response);
            $scope.auth = response;
        
        }).error(function(response) {
            alert('There is something wrong');
        });   
    }
    getAuthUser();
  
  getNotification = function(){

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
            success(function(data) {
                console.log(data);
                $scope.notification = data;
            }).
            error(function(data) {
                console.log(data);
                alert('There is something wrong');
        });
    }
    getNotification();
  
  getAllSentReferral = function(){
        $http({
            method: 'GET',
            url: API_URL + 'webservice/getAllSentReferral'
        }).
            success(function(data) {
                console.log('sent referral', data);
                $scope.referrals = data;
               
            }).
            error(function(data) {
                console.log(data);
                alert(data);
        });
    }


  getAllSentReferral();

	$scope.newReferral = function(){
		$scope.$broadcast('angucomplete-alt:clearInput', 'search_student');
		$('#myModal').modal('show');
	}
	
	$scope.student = function(selected) {
      if (selected) {
        console.log(selected);
        
        $scope.selected = selected;

        $scope.year = selected.description.year;
        $scope.course = selected.description.course;
        $scope.college_id = selected.description.college_id;
        $scope.college = selected.description.college;

        $('#referral-form').show();
      } else {
        console.log('cleared');
        $('#referral-form').hide();
      }
    };

    $scope.guidance = function(selected) {
      if (selected) {
        $('#sendReferralButton').prop('disabled', false);
        console.log('GUIDANCE' , selected);
        $scope.guidance_id = selected.description.id;
      } else {
        $('#sendReferralButton').prop('disabled', true);
      }
    };

  $scope.sendFeedback = function(referral){
    // console.log('view referral',$scope.replyMessage,referral);
    // $scope.replyMessage;
    
    var formdata = {
      referral_id : referral.referral_id,
      message : $scope.replyMessage
    }
    //webservice
    $http({
          method: 'POST', 
          url: API_URL + 'webservice/sendFeedback',
          data: formdata
      }).
          success(function(data) {
              
              $('#viewReferralModal').modal('hide');
              console.log(data);
              
                // alert('SUCCESS');
              
              
          }).
          error(function(data) {
              console.log("Error on this request");
              alert(data);
      });


  }

  $scope.sendReferral = function(){
    var checkBoxValues = [
        'Absenteeism/Tardiness',
        'Withdrawal From Group Activities',
        'Deteriorating Performance',
        'Course Indecision',
        'Lack of Interest',
        'Dropping School',
        'High Anxiousness',
        'Family Problem',
        'Misbehavior in Class',
        'Member of a Not Recognized Organization'
    ];

    if(!$scope.checkbox){
      alert("Please click any from the checkbox");
      return;
    }
    
    var formData = {},
        year = $scope.year,
        course = $scope.course,
        professor_id = $scope.professor_id,
        message = '',
        desc = $scope.desc,
        checkbox= $scope.checkbox,
        student = $scope.selected.description,
        college_id = $scope.college_id;

        message = (checkbox.reason1) ? checkBoxValues[0] : '';
        message = (checkbox.reason2) ? ((message) ? message +', ' + checkBoxValues[1] : checkBoxValues[1] ) : message;
        message = (checkbox.reason3) ? ((message) ? message +', ' + checkBoxValues[2] : checkBoxValues[2] ) : message;
        message = (checkbox.reason4) ? ((message) ? message +', ' + checkBoxValues[3] : checkBoxValues[3] ) : message;
        message = (checkbox.reason5) ? ((message) ? message +', ' + checkBoxValues[4] : checkBoxValues[4] ) : message;
        message = (checkbox.reason6) ? ((message) ? message +', ' + checkBoxValues[5] : checkBoxValues[5] ) : message;
        message = (checkbox.reason7) ? ((message) ? message +', ' + checkBoxValues[6] : checkBoxValues[6] ) : message;
        message = (checkbox.reason8) ? ((message) ? message +', ' + checkBoxValues[7] : checkBoxValues[7] ) : message;
        message = (checkbox.reason9) ? ((message) ? message +', ' + checkBoxValues[8] : checkBoxValues[8] ) : message;
        message = (checkbox.reason10) ? ((message) ? message +', ' + checkBoxValues[9] : checkBoxValues[9] ) : message;
        message = (checkbox.reason11) ? ((message) ? message +', ' + $scope.reasonSpec : $scope.reasonSpec ) : message;



        console.log(message)

        var formdata = {
          firstname : student.firstname,
          middlename : student.middlename,
          lastname : student.lastname,
          year : String(year),
          course : course,
          professor_id : String(professor_id),
          student_id : student.id,
          college_id : college_id,
          message : message,
          description: desc
        }
        
        $http({
                method: 'POST', 
                url: API_URL + 'webservice/newReferral',
                data: formdata
            }).
                success(function(data) {
                    $('#referral-form').hide();
                    $('#myModal').modal('hide');
                    console.log(data);
                    alert('SUCCESS CREATION');
                    getAllSentReferral();
                
                }).
                error(function(data) {
                    console.log("Error on this request");
                    // alert(data);
            });


  }
  $scope.othersSelected = function(){
      var checkbox = $scope.checkbox;
      
      if(checkbox.reason11){
        $('#reasonSpec').prop('disabled', false);
        // $('.referralCheckbox').prop('disabled', true);
        // $scope.checkbox.reason1 = false;
        // $scope.checkbox.reason2 = false;
        // $scope.checkbox.reason3 = false;
        // $scope.checkbox.reason4 = false;
        // $scope.checkbox.reason5 = false;
        // $scope.checkbox.reason6 = false;
        // $scope.checkbox.reason7 = false;
        // $scope.checkbox.reason8 = false;
        // $scope.checkbox.reason9 = false;
        // $scope.checkbox.reason10 = false;
        

      }else{
        $('#reasonSpec').prop('disabled', true);
        $('.referralCheckbox').prop('disabled', false);
      }
  }

  $scope.sendFeedbackchat = function(referral){
    // $scope.replyMessage;
    
    var formdata = {
      referral_id : referral.referral_id,
      user_id : $scope.auth.code,
      message : $scope.replyMessage
    }
    //webservice
    $http({
          method: 'POST', 
          url: API_URL + 'webservice/sendFeedback',
          data: formdata
      }).
          success(function(data) {
              
              console.log(data);
              getAllFeedbackChats(referral.referral_id);
              // alert('Success');
              
          }).
          error(function(data) {
              console.log("Error on this request");
              alert(data);
      });

  }


  getAllFeedbackChats = function(referral_id){
    $http({
        method: 'GET',
        url: API_URL + 'webservice/getAllFeedbackChats/' + referral_id 
    }).
        success(function(data) {
           console.log('view', data);
           $scope.chats = data;
        }).
        error(function(data) {
            alert(data);
    });

  }
	$scope.toggleViewReferral = function(referral){

    $scope.viewReferral = referral;


    //get feedback chat conversations

    getAllFeedbackChats(referral.referral_id);

    //check if there is already a reply

    // $http({
    //     method: 'GET',
    //     url: API_URL + 'webservice/checkReply/' +referral.referral_id 
    // }).
    //     success(function(data) {
    //         console.log('sent referral', data);
    //         if(data){
    //           $scope.replyMessage = data.message;
    //           // $('#replyButton').prop('disabled', true);
    //           if($scope.auth.status == 'professor'){
    //             $('#replyMessage').prop('disabled', true);
    //           }
              
    //         }else{
    //           $scope.replyMessage = '';
    //           // $('#replyButton').prop('disabled', false);
    //           if($scope.auth.status == 'professor'){
    //             $('#replyMessage').prop('disabled', true);
    //           }
    //         }
            
           
    //     }).
    //     error(function(data) {
    //         alert(data);
    // });

    $('#viewReferralModal').modal('show');
  }

  $scope.replyMessageChanged = function(){
    // $('#replyButton').prop('disabled', false);
  }

});