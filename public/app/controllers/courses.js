var app = angular.module('courses', ['oitozero.ngSweetAlert','url.constant']);
app.controller('coursesController', function($scope, $http, API_URL) {

    $http.get(API_URL + "webservice/getColleges")
            .success(function(response) {
                $scope.colleges = response;
            }).error(function(response) {
     
            console.log(response);

            alert('This is embarassing. An error has occured. Please check the log for details');
        });

    $http.get(API_URL + "webservice/getCourses")
            .success(function(response) {
                console.log('testRes' ,response);
                $scope.courses = response;
            }).error(function(response) {
     
            console.log(response);

            alert('This is embarassing. An error has occured. Please check the log for details');
        });

    getNotification = function(){

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
            success(function(data) {
                console.log(data);
                $scope.notification = data;
            }).
            error(function(data) {
                console.log(data);
                alert('There is something wrong');
        });
    }
    getNotification();        
	    
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Create Course";
                break;
            case 'edit':
                $scope.form_title = "Edit Course Detail";
                $scope.id = id;
                $http.get(API_URL + 'webservice/getCourses/' + id)
                        .success(function(response) {
                            console.log('checkget', response);
                            $scope.courseHold = response[0];
                        });
                break;
            default:
                break;
        }
        console.log(id);
        $('#myModal').modal('show');
    }

    $scope.retrieveName = function(id){
        console.log('idcheck', id);
        $http.get(API_URL + "webservice/getCourses" + id)
            .success(function(response) {
                console.log('testName' ,response);
                $scope.courses = response;
            }).error(function(response) {
     
            console.log(response);

            alert('This is embarassing. An error has occured. Please check the log for details');
        });

    }


   $scope.save = function(modalstate, cour_id) {
        var url = API_URL + "webservice/createCourse";
        // var name = $scope.college.name;
    
        if(cour_id != null){
            url += "/" + cour_id;
        }
    
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.courseHold),   
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(API_URL);
            console.log(response);

            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        console.log('testId', id);
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                    url: API_URL + 'webservice/deleteCourse/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        location.reload();
                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete');
                    });
        } else {
            return false;
        }
    }



});

