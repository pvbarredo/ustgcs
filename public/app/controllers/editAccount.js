var app = angular.module('editUser', ['oitozero.ngSweetAlert','url.constant']);

app.controller('editUserController', function($scope, $http, API_URL) {

   getNotification = function(){

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
            success(function(data) {
                console.log(data);
                $scope.notification = data;
            }).
            error(function(data) {
                console.log(data);
                alert('There is something wrong');
        });
    }
    getNotification();
    $scope.editAccountSubmitClicked = function(){
    	var firstname = $scope.firstname,
    		middlename = $scope.middlename,
    		lastname = $scope.lastname;

  		var form = {
  			firstname: firstname,
  			middlename : middlename,
  			lastname : lastname
  		}

  		$http({
	        method: 'POST',
	        url: API_URL + 'webservice/editAccountInfo',
	        data: {newPass : $scope.newPass}
	    }).
        success(function(data) {
           alert('Success editing profile');
           
        }).
        error(function(data) {
            console.log(data);
            alert(data);
        });


    }
    
});