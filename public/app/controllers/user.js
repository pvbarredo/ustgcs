var changePasswordApp = angular.module('changePassword', ['oitozero.ngSweetAlert','url.constant'])

changePasswordApp.controller('changePasswordController', function($scope, $http, API_URL) {

    getNotification = function(){

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
            success(function(data) {
                console.log(data);
                $scope.notification = data;
            }).
            error(function(data) {
                console.log(data);
                alert('There is something wrong');
        });
    }
    getNotification();
    $scope.changePasswordSubmitClicked = function(){

    	var oldPass = $scope.oldPass,
    		newPass = $scope.newPass,
    		confirmNewPass = $scope.confirmNewPass;

    		//we should have a proper messaging pop up
            if (oldPass == null){
                alert('Please enter old password');
                return;
            }

            if (newPass == null){
                alert("Please enter new password");
                return;
            }

            if (confirmNewPass == null){
                alert("Please enter confirm password");
                return;
            }


    		if (newPass !== confirmNewPass){
    			alert('New Password doesnt match');
    			return;
    		}
            else{
                if(newPass == oldPass){
                    alert('New password should not be the same as your old password');
                    return;
                }
            }

    		//check if old password is correct

    		
    		$http({
		        method: 'POST',
		        url: API_URL + 'webservice/compareInputPassToCurrent',
		        data: {oldPass : oldPass}
		    }).
	        success(function(data) {
	            if(data === 'true'){
	            	changePassword();
	            }else{
	            	alert('Wrong OLD password');
	            }
	           
	        }).
	        error(function(data) {
	            console.log(data);
	            alert('THere is something wrong');
	        });

    }

    changePassword = function(){
    	$http({
		        method: 'POST',
		        url: API_URL + 'webservice/changePassword',
		        data: {newPass : $scope.newPass}
		    }).
	        success(function(data) {
	           alert('Success changing password');

	           
	        }).
	        error(function(data) {
	            console.log(data);
	            alert('THere is something wrong');
	        });
    }
    
});

var editUserApp = angular.module('editUser', ['url.constant'])
editUserApp.controller('editUserController', function($scope, $http, API_URL) {

	getAllAnnouncementForUser = function(){

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getAllAnnouncementForUser'
        }).
            success(function(data) {
                console.log(data);
                $scope.announcements = data.announcements;
                $scope.total_unread = data.total_unread;
                $scope.flag = data.flag ;
            }).
            error(function(data) {
                console.log(data);
                alert('THere is something wrong');
        });
    }
    getAllAnnouncementForUser();


    getBasicInfo = function(){
		 $http.get(API_URL + "webservice/getBasicInfo")
            .success(function(response) {
                $scope.basicInfo = response;
            }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
	}



    $http.get(API_URL + "webservice/getColleges")
            .success(function(response) {
                $scope.colleges = response;
            }).error(function(response) {
     
            console.log(response);

            alert('This is embarassing. An error has occured. Please check the log for details');
        });

    $scope.getTailorCourse = function(id){
        if(id != null){
            $http.get(API_URL + "webservice/getCourses/" + id)
                    .success(function(response) {
                        console.log('testRes' ,response); 
                        $scope.courses = response;

                    }).error(function(response) {
             
                    console.log(response);

                    alert('This is embarassing. An error has occured. Please check the log for details');
                });
        }
    }

    $scope.getTailorYears = function(id){
      if(id != null){
          $http.get(API_URL + "webservice/getCourseYears/" + id)
                    .success(function(response) {
                        console.log('testRes' ,response);         
                      
                   
                            var maxYearArr = [];
                           var max_years = response.max_years;
                          for (var ctr1=1 ; ctr1<= max_years; ctr1++){
                            maxYearArr.push(ctr1);
                            console.log('test',maxYearArr);
                          }
                          $scope.courseYears = maxYearArr;

                    }).error(function(response) {
             
                    console.log(response);

                    alert('This is embarassing. An error has occured. Please check the log for details');
                });
      }
    }
    getBasicInfo();

    getAuthUser = function(){
        $http.get(API_URL + "webservice/getAuthUser")
        .success(function(response) {
            console.log("auth",response);
            $scope.auth = response;
         
        }).error(function(response) {
            alert('There is something wrong');
        });   
    }
    getAuthUser();

	$scope.editAccountSubmitClicked = function(){

    	var basicInfo = $scope.basicInfo,
    		firstname = basicInfo.firstname,
    		middlename = basicInfo.middlename,
    		lastname = basicInfo.lastname,
            email = basicInfo.email,
            college = basicInfo.college_id,
            course = basicInfo.course_id,
            year = basicInfo.year,
            status = $scope.auth.status;

        if(firstname == null){
            alert("Please enter first name");
            return;

        }

        if(middlename == null){
            alert("Please enter middle name");
            return;
        }

        if(lastname == null){
            alert("Please enter last name");
            return;

        }

        if(status == 'student'){
            if(course == null){
                alert("Please enter course");
                return;

            }
            if(year == null){
                alert("Please enter year");
                return;
            }

        }
        if(email == null){
            alert("Please enter email");
            return;
        }




  		var form = {
  			firstname: firstname,
  			middlename : middlename,
  			lastname : lastname,
            email : email,
            college_id : college,
            course_id : course,
            year : year
  		}

  		$http({
	        method: 'POST',
	        url: API_URL + 'webservice/editAccountInfo',
	        data: form
	    }).
        success(function(data) {
           alert('Success editing profile');
           location.reload();
           
        }).
        error(function(data) {
            console.log(data);
            alert('THere is something wrong');
        });


    }
    
});
