var app = angular.module('cumulative', ['angucomplete-alt', 'oitozero.ngSweetAlert', 'url.constant']);
app.controller('cumulativeController', function($scope, $http, API_URL, $location) {

    $scope.field_focus = function(addedCumulativeField) {
        $scope.selectedAddedCumulativeField = addedCumulativeField;
        $scope.answerField = _.clone(addedCumulativeField);
        $('#answer_question_modal').modal('show');



    }

    $scope.answer_clicked = function() {
        $http({
            method: 'POST',
            url: API_URL + 'webservice/answerAddedCumulativeField',
            data: $scope.answerField
        }).
        success(function(data) {

            swal("Success!", "Success answering additional question!", "success");
            $('#answer_question_modal').modal('hide');

            $scope.selectedAddedCumulativeField.value = $scope.answerField.value;

        }).error(function(response) {
            swal("Error!", "Please reload the page!", "error");
        });
    }


    $scope.cumulative = {};

    getAllAddedCumulativeField = function(id) {

        $http({
            method: 'GET',
            url: API_URL + 'webservice/addedCumulativeField/' + id
        }).
        success(function(data) {
            $scope.addedCumulativeFields = data;
        }).
        error(function(data) {
            console.log(data);
            swal("Error!", "Please reload the page!", "error");
        });
    };

    $scope.editAddedField = function(addedCumulativeField) {
        $scope.edit_question = _.clone(addedCumulativeField);
        $('#edit_question_modal').modal('show');


    }

    $scope.updateAddedField = function(question_field) {

        $http({
            method: 'POST',
            url: API_URL + 'webservice/updateAddedCumulativeField',
            data: question_field
        }).
        success(function(data) {

            swal({
                    title: "Success!",
                    text: "Success editing additional question!",
                    type: "success",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Okay",
                    closeOnConfirm: false
                },
                function() {
                    location.reload();
                });

        }).error(function(response) {
            swal("Error!", "Please reload the page!", "error");
        });

    }

    $scope.removeAddedField = function(addedCumulativeField) {

        swal({
                title: "Are you sure?",
                text: "You want to delete the added field",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            },
            function() {

                $http.delete(API_URL + "webservice/addedCumulativeField/" + addedCumulativeField.id)
                    .success(function(response) {
                        swal("Deleted!", "The field is deleted", "success");
                    }).error(function(response) {
                        swal("Error!", "Please reload the page!", "error");
                    });


            });
    }

    $scope.acadHealthProblemsChanged = function(value, specify) {
        if (specify) {
            $scope.cumulative.finalAcad_health_prob = $scope.acadZ;
        } else {
            if (value === 'Yes') {
                $scope.cumulative.acad_health_prob = '';
                $scope.cumulative.finalAcad_health_prob = '';
            } else {
                $scope.cumulative.acad_health_prob = 'No';
                $scope.cumulative.finalAcad_health_prob = 'No';
                $scope.acadZ = '';
            }
        }

        console.log('finalAcad_health_prob Value', $scope.cumulative.finalAcad_health_prob);

    }

    $scope.add_question = function() {
        var parameter = {
            user_code: $scope.global_id,
            field: $scope.added_question_field,
            value: '',
            created_by: $scope.auth.code
        }


        $http({
            method: 'POST',
            url: API_URL + 'webservice/addedCumulativeField',
            data: parameter
        }).
        success(function(data) {
            $('#add_question_modal').modal('hide');
            swal({
                    title: "Success!",
                    text: "Success adding additional question!",
                    type: "success",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Okay",
                    closeOnConfirm: false
                },
                function() {
                    location.reload();
                });

        }).error(function(response) {
            swal("Error!", "Please reload the page!", "error");
        });


    }

    $scope.addQuestionClicked = function() {

        $('#add_question_modal').modal('show');


    }
    $scope.curricularHealthProblemsChanged = function(value, specify) {
        if (specify) {
            $scope.cumulative.finalCurricular_health_prob = $scope.currZ;
        } else {
            if (value === 'Yes') {
                $scope.cumulative.curricular_health_prob = '';
                $scope.cumulative.finalCurricular_health_prob = '';
            } else {
                $scope.cumulative.curricular_health_prob = 'No';
                $scope.cumulative.finalCurricular_health_prob = 'No';
                $scope.currZ = '';
            }
        }

        console.log('finalCurricular_health_prob Value', $scope.cumulative.finalCurricular_health_prob);
    }

    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    getAuthUser = function() {
        $http.get(API_URL + "webservice/getAuthUser")
            .success(function(response) {
                console.log("auth", response);
                $scope.auth = response;
                getAllAddedCumulativeField(response.code);
                getCumulativeRecord();

            }).error(function(response) {
                swal("Error!", "Please reload the page!", "error");
            });
    }
    getAuthUser();

    function getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    getNotification = function() {

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
        success(function(data) {
            console.log(data);
            $scope.notification = data;
        }).
        error(function(data) {
            console.log(data);
            swal("Error!", "Please reload the page!", "error");
        });
    }
    getNotification();

    responseSuccess = function(response) {

        $scope.cumulative = response;


        $scope.cumulative.firstname = response.firstname;
        $scope.cumulative.middlename = response.middlename;
        $scope.cumulative.lastname = response.lastname;
        $scope.cumulative.age = getAge(response.birthday);
        var d = new Date(response.birthday);
        $scope.cumulative.birthMonth = monthNames[d.getMonth()];
        $scope.cumulative.birthDay = d.getDate()+1;
        $scope.cumulative.birthYear = d.getFullYear();

        $scope.cumulative.gender = response.gender ? 'Male' : 'Female';


        $scope.cumulative.finalAcad_health_prob = $scope.cumulative.acad_health_prob;

        if ($scope.cumulative.acad_health_prob !== 'No') {
            $scope.acadZ = $scope.cumulative.acad_health_prob;
        }

        $scope.cumulative.finalCurricular_health_prob = $scope.cumulative.curricular_health_prob;

        if ($scope.cumulative.curricular_health_prob !== 'No') {
            $scope.currZ = $scope.cumulative.curricular_health_prob;
        }

        //process close family check box
        $closeCheckboxValues = ['Mother', 'Father', 'Older Sibling', 'Younger Sibling'];
        var closeValues = $scope.cumulative.close_family.split(",");
        console.log('console Values', closeValues);

        angular.forEach(closeValues, function(values) {

            if (values === 'Mother') {
                $scope.cumulative.close_mother = 1;
            } else if (values === 'Father') {
                $scope.cumulative.close_father = 1;
            } else if (values === 'Older Sibling') {
                $scope.cumulative.close_old_sibling = 1;
            } else if (values === 'Younger Sibling') {
                $scope.cumulative.close_young_sibling = 1;
            } else {
                $scope.cumulative.close_other = 1;
                $scope.cumulative.close_other_spec = values;
            }

        })


        $scope.finalCloseFamilyCheckbox = [];


        //process motivation check boxes
        $motivationCheckboxValues = ['Family', 'Significant Other', 'Prestige', 'Friends', 'Career', 'Passion', 'Job', 'Fulfillment', 'Interest'];

        var motValues = $scope.cumulative.motivate_factor.split(",");
        console.log('mot factors', motValues);

        angular.forEach(motValues, function(value) {
            if (value === 'Family') {
                $scope.cumulative.motivate_family = 1;
            } else if (value === 'Significant Other') {
                $scope.cumulative.motivate_significant_other = 1;
            } else if (value === 'Prestige') {
                $scope.cumulative.motivate_prestige = 1;
            } else if (value === 'Friends') {
                $scope.cumulative.motivate_friends = 1;
            } else if (value === 'Career') {
                $scope.cumulative.motivate_career = 1;
            } else if (value === 'Passion') {
                $scope.cumulative.motivate_passion = 1;
            } else if (value === 'Job') {
                $scope.cumulative.motivate_job = 1;
            } else if (value === 'Fulfillment') {
                $scope.cumulative.motivate_fulfillment = 1;
            } else if (value === 'Interest') {
                $scope.cumulative.motivate_interest = 1;
            }

        })


        $scope.cumulative.finalMotivateFactors = [];



        //process counselor check boxes

        var counValues = $scope.cumulative.counselor_help.split(",");
        console.log('counselor value', counValues);

        angular.forEach(counValues, function(value) {
            if (value === 'Information') {
                $scope.cumulative.counselor_info = 1;
            } else if (value === 'Academics') {
                $scope.cumulative.counselor_academics = 1;
            } else if (value === 'Prestige') {
                $scope.cumulative.motivate_prestige = 1;
            } else if (value === 'Relationships') {
                $scope.cumulative.counselor_relationships = 1;
            } else if (value === 'Personal') {
                $scope.cumulative.counselor_personal = 1;
            } else {
                $scope.cumulative.counselor_others = 1;
                $scope.cumulative.counselor_others_spec = value;
            }

        })



        $scope.cumulative.finalCounselorHelp = [];

    }

    $scope.closeFamilyCheckboxChanged = function(data) {
        if (data === 'Mother') {
            if ($scope.finalCloseFamilyCheckbox.indexOf('Mother') == -1) {
                $scope.finalCloseFamilyCheckbox.push('Mother');
            }
        }
        if (data === 'Father') {
            if ($scope.finalCloseFamilyCheckbox.indexOf('Father') == -1) {
                $scope.finalCloseFamilyCheckbox.push('Father');
            }
        }
        if (data === 'oldSibling') {
            if ($scope.finalCloseFamilyCheckbox.indexOf('Older Sibling') == -1) {
                $scope.finalCloseFamilyCheckbox.push('Older Sibling');
            }
        }
        if (data === 'youngSibling') {
            if ($scope.finalCloseFamilyCheckbox.indexOf('Younger Sibling') == -1) {
                $scope.finalCloseFamilyCheckbox.push('Younger Sibling');
            }
        }

    }

    getCumulativeRecord = function() {

        if ($scope.auth.status === 'student') {
            $http.get(API_URL + "webservice/getCumulativeRecord")
                .success(function(response) {
                    if (response) {
                        responseSuccess(response);
                    }

                }).error(function(response) {

                    console.log(response);

                    swal("Error!", "Please reload the page!", "error");
                });
        } else {
            var url = $location.absUrl().split('?')[0],
                url = url.split('/'),
                id = url[url.length - 1];

            $scope.global_id = id;
            getAllAddedCumulativeField(id);
            $http.get(API_URL + "webservice/cumulativeView/" + id)
                .success(function(response) {
                    $('.readonly').find('input, textarea, select').attr('readonly', 'readonly');
                    $('.readonly').find('input, textarea, select').attr('disabled', true);

                    $("#edit_question_input").prop('disabled', false);
                    $("#edit_question_input").prop('readonly', false);


                    $("#questionAdditional").prop('disabled', false);
                    $("#questionAdditional").prop('readonly', false);

                    if (response) {
                        responseSuccess(response);
                    } else {
                        swal("No Cumulative Record!", "Student hasn't filed his cumulative record", "warning");
                    }

                }).error(function(response) {

                    console.log(response);

                    swal("Error!", "Please reload the page!", "error");
                });
        }

    }
    $scope.checkMotivation = [];
    $scope.checkboxChanged = function(z) {
        var count = 0,
            checkBoxes = [];
        if ($scope.cumulative.motivate_family && count !== 3) {
            checkBoxes.push(0);
            count++;
        }
        if ($scope.cumulative.motivate_significant_other && count !== 3) {
            checkBoxes.push(1);
            count++;
        }
        if ($scope.cumulative.motivate_prestige && count !== 3) {
            checkBoxes.push(2);
            count++;
        }
        if ($scope.cumulative.motivate_friends && count !== 3) {
            checkBoxes.push(3);
            count++;
        }
        if ($scope.cumulative.motivate_career && count !== 3) {
            checkBoxes.push(4);
            count++;
        }
        if ($scope.cumulative.motivate_passion && count !== 3) {
            checkBoxes.push(5);
            count++;
        }
        if ($scope.cumulative.motivate_job && count !== 3) {
            checkBoxes.push(6);
            count++;
        }
        if ($scope.cumulative.motivate_fulfillment && count !== 3) {
            checkBoxes.push(7);
            count++;
        }
        if ($scope.cumulative.motivate_interest && count !== 3) {
            checkBoxes.push(8);
            count++;
        }
        if (count === 3) {
            for (var i = 0; i < 9; i++) {
                var isThere = false;
                for (var y = 0; y < 4; y++) {
                    if (i === checkBoxes[y]) {
                        isThere = true;
                    }
                }
                if (!isThere) {
                    $scope.checkMotivation[i] = true;
                }
            }
        } else {
            for (var i = 0; i < 9; i++) {
                $scope.checkMotivation[i] = false;
            }
        }



    }

    $scope.dpClicked = function() {

        $('#uploadPictureModal').modal('show');
    }
    $scope.save = function(cum_id) {
        var url = API_URL + "webservice/createCumulativeRecord";


        console.log('cum_id', cum_id)
        if (cum_id != null) {
            var stud_id = $scope.cumulative.student_id;
            url += "/" + stud_id;
        }



        if ($scope.cumulative.firstname == null) {
            swal("Cumulative Error!", "Please enter your first name", "warning");
            return;
        }



        if ($scope.cumulative.lastname == null) {
            swal("Cumulative Error!", "Please enter your last name", "warning");
            return;
        }

        if ($scope.cumulative.nickname == null) {
            swal("Cumulative Error!", "Please enter your nickname", "warning");
            return;
        }



        if ($scope.cumulative.nationality == null) {
            swal("Cumulative Error!", "Please enter your nationality", "warning");
            return;
        }

        if ($scope.cumulative.religion == null) {
            swal("Cumulative Error!", "Please enter your religion", "warning");
            return;
        }

        if ($scope.cumulative.email == null) {
            swal("Cumulative Error!", "Please enter your email", "warning");
            return;
        }

        if ($scope.cumulative.curr_address == null) {
            swal("Cumulative Error!", "Please enter your current address", "warning");
            return;
        }

        if ($scope.cumulative.curr_city == null) {
            swal("Cumulative Error!", "Please enter your current city", "warning");
            return;
        }

        if ($scope.cumulative.mobile_no == null) {
            swal("Cumulative Error!", "Please enter your mobile number", "warning");
            return;
        }

        if ($scope.cumulative.perm_address == null) {
            swal("Cumulative Error!", "Please enter your permanent address", "warning");
            return;
        }

        if ($scope.cumulative.perm_city == null) {
            swal("Cumulative Error!", "Please enter your permanent city", "warning");
            return;
        }

        if ($scope.cumulative.home_phone_no == null) {
            alert('Please enter your home phone number');
            return;
        }

        if ($scope.cumulative.living_arrangement == null) {
            alert('Please select your current living arrangement');
            return;
        }

        if ($scope.cumulative.finalAcad_health_prob == null) {
            alert('Please select if you have academic health problems')
            return;
        }


        if ($scope.cumulative.finalCurricular_health_prob == null) {
            alert('Please select if you have extra curricular health problems');
            return;
        }



        if ($scope.cumulative.psychiatric_help == null) {
            alert("Please select if you sought psychiatric help");
            return;
        }

        if ($scope.cumulative.counseling == null) {
            alert("Please select if you have undergone counseling");;
            return;
        }

        if ($scope.cumulative.father_name == null) {
            alert("Please enter your father's name");
            return;
        }

        if ($scope.cumulative.father_address == null) {
            alert("Please enter your father's address");
            return;
        }

        if ($scope.cumulative.father_occupation == null) {
            alert("Please enter your father's occupation");
            return;
        }

        if ($scope.cumulative.father_contact == null) {
            alert("Please enter your father's contact number");
            return;
        }

        if ($scope.cumulative.mother_name == null) {
            alert("Please enter your mother's name");
            return;
        }

        if ($scope.cumulative.mother_address == null) {
            alert("Please enter your mother's address");
            return;
        }

        if ($scope.cumulative.mother_occupation == null) {
            alert("Please enter your mother's occupation");
            return;
        }

        if ($scope.cumulative.mother_contact == null) {
            alert("Please eneter your mother's contact number");
            return;
        }

        if ($scope.cumulative.parent_marital_status == null) {
            alert("Please select marital status");
            return;
        } else {
            if ($scope.cumulative.parent_marital_status == "marStatParNot1") {
                if ($scope.cumulative.parent_marital_status_spec1 == null) {
                    alert("Please select whose parent is working abroad");
                    return;

                }
            } else if ($scope.cumulative.parent_marital_status == "marStatParNot2") {
                if ($scope.cumulative.parent_marital_status_spec2 == null) {
                    alert("Please select whose parent is already deceased");
                    return;
                }
            } else if ($scope.cumulative.parent_marital_status == "marStat4") {
                if ($scope.cumulative.parent_marital_status_other == null) {
                    alert("Please specify the other marital status of your parents");
                    return;
                }
            }
        }

        if ($scope.cumulative.guardian_name == null) {
            alert("Please enter your guardian name");
            return;
        }

        if ($scope.cumulative.guardian_relation == null) {
            alert("Please enter your relation to your guardian");
            return;
        }

        if ($scope.cumulative.guardian_contact == null) {
            alert("Please enter your guardian's contact number");
            return;
        }

        if ($scope.cumulative.family_life_perception == null) {
            alert("Please select how do you perceive your family life");
            return;
        }

        if ($scope.cumulative.close_other == "true") {
            if ($scope.cumulative.close_other_spec == null) {
                alert("Please specify your other close people");
                return;
            }
        }

        if ($scope.cumulative.counselor_others == "1") {
            if ($scope.cumulatve.counselor_others_spec == null) {
                alert("Please specify your other counselor help needs from the counselor");
                return;
            }
        }



        if ($scope.cumulative.acad_health_prob != "true") {
            $scope.cumulative.acad_health_prob_spec = "";
        }

        if ($scope.cumulative.curricular_health_prob != "true") {
            $scope.cumulative.curricular_health_prob_spec = "";
        }

        if ($scope.cumulative.parent_marital_status != "marStatParNot1") {
            $scope.cumulative.parent_marital_status_spec1 = "";
        }

        if ($scope.cumulative.parent_marital_status != "marStatParNot2") {
            $scope.cumulative.parent_marital_status_spec2 = "";
        }

        if ($scope.parent_marital_status != 'marStat4') {
            $scope.cumulative.parent_marital_status_other = "";
        }


        if ($scope.cumulative.close_mother == null) {
            $scope.cumulative.close_mother = "false";
        }

        if ($scope.cumulative.close_father == null) {
            $scope.cumulative.close_father = "false";
        }

        if ($scope.cumulative.close_old_sibling == null) {
            $scope.cumulative.close_old_sibling = "false";
        }

        if ($scope.cumulative.close_young_sibling == null) {
            $scope.cumulative.close_young_sibling = "false";
        }

        if ($scope.cumulative.close_other == null) {
            $scope.cumulative.close_other = "false";
            $scope.cumulative.close_other_spec = "";
        }

        if ($scope.cumulative.close_other == "false") {
            $scope.cumulative.close_other_spec = "";
        }

        var fullMotivateFactors = [];

        if ($scope.cumulative.motivate_family == null) {
            $scope.cumulative.motivate_family = "false";
        } else {
            if ($scope.cumulative.motivate_family != "0") {
                fullMotivateFactors.push('Family');
            }
        }


        if ($scope.cumulative.motivate_significant_other == null) {
            $scope.cumulative.motivate_significant_other = "false";
        } else {
            if ($scope.cumulative.motivate_significant_other != "0") {
                fullMotivateFactors.push('Significant Other');
            }
        }

        if ($scope.cumulative.motivate_prestige == null) {
            $scope.cumulative.motivate_prestige = "false";
        } else {
            if ($scope.cumulative.motivate_prestige != "0") {
                fullMotivateFactors.push('Prestige');
            }
        }

        if ($scope.cumulative.motivate_friends == null) {
            $scope.cumulative.motivate_friends = "false";
        } else {
            if ($scope.cumulative.motivate_friends != "0") {
                fullMotivateFactors.push('Friends');
            }
        }

        if ($scope.cumulative.motivate_career == null) {
            $scope.cumulative.motivate_career = "false";
        } else {
            if ($scope.cumulative.motivate_career != "0") {
                fullMotivateFactors.push('Career');
            }
        }


        if ($scope.cumulative.motivate_passion == null) {
            $scope.cumulative.motivate_passion = "false";
        } else {
            if ($scope.cumulative.motivate_passion != "0") {
                fullMotivateFactors.push('Passion');
            }
        }

        if ($scope.cumulative.motivate_job == null) {
            $scope.cumulative.motivate_job = "false";
        } else {
            if ($scope.cumulative.motivate_job != "0") {
                fullMotivateFactors.push('Job');
            }
        }

        if ($scope.cumulative.motivate_fulfillment == null) {
            $scope.cumulative.motivate_fulfillment = "false";
        } else {
            if ($scope.cumulative.motivate_fulfillment != "0") {
                fullMotivateFactors.push('Fulfillment');
            }
        }

        if ($scope.cumulative.motivate_interest == null) {
            $scope.cumulative.motivate_interest = "false";
        } else {
            if ($scope.cumulative.motivate_interest != "0") {
                fullMotivateFactors.push('Interest');
            }
        }

        $scope.cumulative.finalMotivateFactors = fullMotivateFactors.join(",");

        var counselorHelp = [];

        if ($scope.cumulative.counselor_info == null) {
            $scope.cumulative.counselor_info = "false";
        } else {
            if ($scope.cumulative.counselor_info != "0") {
                counselorHelp.push('Information');
            }
        }

        if ($scope.cumulative.counselor_academics == null) {
            $scope.cumulative.counselor_academics = "false";
        } else {
            if ($scope.cumulative.counselor_academics != "0") {
                counselorHelp.push('Academics');
            }
        }

        if ($scope.cumulative.counselor_relationships == null) {
            $scope.cumulative.counselor_relationships = "false";
        } else {
            if ($scope.cumulative.counselor_relationships != "0") {
                counselorHelp.push('Relationships');
            }
        }

        if ($scope.cumulative.counselor_personal == null) {
            $scope.cumulative.counselor_personal = "false";
        } else {
            if ($scope.cumulative.counselor_personal != "0") {
                counselorHelp.push('Personal');
            }
        }


        if ($scope.cumulative.counselor_others == null) {
            $scope.cumulative.counselor_others = "false";
            $scope.cumulative.counselor_others_spec = "";

        } else {

            if ($scope.cumulative.counselor_others == "0") {
                $scope.cumulative.counselor_others_spec = "";
            } else {
                if ($scope.cumulative.counselor_others_spec !== '' && $scope.cumulative.counselor_others_spec !== null) {
                    counselorHelp.push($scope.cumulative.counselor_others_spec);
                }
            }

        }

        $scope.cumulative.finalCounselorHelp = counselorHelp.join(",");



        //close family check box checking


        var finalCloseFamilyCheckbox = [];
        if ($scope.cumulative.close_mother !== 'false' && $scope.cumulative.close_mother !== 0 && $scope.cumulative.close_mother !== false) {
            finalCloseFamilyCheckbox.push('Mother');
        }

        if ($scope.cumulative.close_father !== 'false' && $scope.cumulative.close_father !== 0 && $scope.cumulative.close_father !== false) {
            finalCloseFamilyCheckbox.push('Father');
        }

        if ($scope.cumulative.close_old_sibling !== 'false' && $scope.cumulative.close_old_sibling !== 0 && $scope.cumulative.close_old_sibling !== false) {
            finalCloseFamilyCheckbox.push('Older Sibling');
        }

        if ($scope.cumulative.close_young_sibling !== 'false' && $scope.cumulative.close_young_sibling !== 0 && $scope.cumulative.close_young_sibling !== false) {
            finalCloseFamilyCheckbox.push('Younger Sibling');
        }

        if ($scope.cumulative.close_other !== 'false' && $scope.cumulative.close_other !== 0 && $scope.cumulative.close_other !== false) {
            finalCloseFamilyCheckbox.push($scope.cumulative.close_other_spec);
        }
        $scope.cumulative.finalCloseFamilyCheckbox = finalCloseFamilyCheckbox.join(",");
        console.log('final Close', $scope.cumulative.finalCloseFamilyCheckbox);


        // console.log('mother', $scope.cumulative.close_mother);
        //  console.log('Father', $scope.cumulative.close_father);
        //   console.log('Older Sibling', $scope.cumulative.close_old_sibling);
        //    console.log('Young Sibling', $scope.cumulative.close_young_sibling);
        //     console.log('other', $scope.cumulative.close_other);
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.cumulative),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(response) {


            $http.get(API_URL + "webservice/deleteNonComplianceNotificationRecord/" + $scope.auth.code)
                .success(function(response) {
                    console.log('Success delete in notification completion');
                });

            swal({
                    title: "Success!",
                    text: "Successfully saved cumulative record!",
                    type: "success",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Okay",
                    closeOnConfirm: false
                },
                function() {
                    location.reload();
                });
        }).error(function(response) {
            console.log(API_URL);
            console.log(response);

            alert('Please re-check your cumulative record');
        });
    }


    $scope.populate = function() {
        $http.get(API_URL + "webservice/autoPopulate")
            .success(function(response) {
                $scope.cumulative = response;
            });
    }



});


app.controller('cumulativeCriteriaController', function($scope, $http, API_URL, $location, $window) {
    getNotification = function() {

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
        success(function(data) {
            console.log(data);
            $scope.notification = data;
        }).
        error(function(data) {
            console.log(data);
            alert('There is something wrong');
        });
    }
    getNotification();

    $scope.student = function(selected) {
        if (selected) {
            console.log(selected);
            $scope.selectedStudent = selected;
            $('#searchCriteria').prop('disabled', false);

        } else {
            console.log('cleared');
            $('#searchCriteria').prop('disabled', true);
        }
    };



    $scope.searchCriteriaButtonClicked = function(selected) {

        // $location.path('cumulative/' + selected.id);
        $window.location.href = 'cumulative/' + selected.id;
    }



});