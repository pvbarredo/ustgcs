var app = angular.module('addUser', ['oitozero.ngSweetAlert','url.constant']);

app.controller('addUserController', function($scope, $http, API_URL) {

    getNotification = function(){

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
            success(function(data) {
                console.log(data);
                $scope.notification = data;
            }).
            error(function(data) {
                console.log(data);
                swal("Error!", "Please reload the page!", "error");
        });
    }
    getNotification();
    

    $http.get(API_URL + "webservice/getColleges")
            .success(function(response) {
                $scope.colleges = response;
            }).error(function(response) {
     
            console.log(response);

            swal("Error!", "Please reload the page!", "error");
        });

    $scope.getTailorCourse = function(id){
        if(id != null){
            $http.get(API_URL + "webservice/account_showCourses/" + id)
                    .success(function(response) {
                        console.log('testRes' ,response); 
                        $scope.courses = response;

                    }).error(function(response) {
             
                    console.log(response);

                    swal("Error!", "Please reload the page!", "error");
                });
        }
    }

    $scope.getTailorYears = function(id){
      if(id != null){
          $http.get(API_URL + "webservice/getCourseYears/" + id)
                    .success(function(response) {
                        console.log('testRes' ,response);         
                      
                   
                            var maxYearArr = [];
                           var max_years = response.max_years;
                          for (var ctr1=1 ; ctr1<= max_years; ctr1++){
                            maxYearArr.push(ctr1);
                            console.log('test',maxYearArr);
                          }
                          $scope.courseYears = maxYearArr;

                    }).error(function(response) {
             
                    console.log(response);

                    swal("Error!", "Please reload the page!", "error");
                });
      }
    }
    $scope.addAccountSubmitClicked = function(){
      var firstname = $scope.basicInfo.firstname,
        middlename = $scope.basicInfo.middlename,
        lastname = $scope.basicInfo.lastname,
        code = $scope.basicInfo.code,
        accountType  = $scope.basicInfo.accountType,
        email = $scope.basicInfo.email,
        birthday = + $scope.basicInfo.birthYear +'-' + $scope.basicInfo.month + '-' + $scope.basicInfo.day, 
        gender = $scope.basicInfo.gender,
        college = $scope.basicInfo.college_id,
        course = $scope.basicInfo.course_id,
        year = $scope.basicInfo.year;
        // contact = $scope.basicInfo.contact;
    
        var d = new Date(birthday);
        if(d.toString() === 'Invalid Date'){
            alert('Invalid Date');
            return;
        }


        if(firstname == null){
            alert('Please enter first name');
            return;
        }

        if(middlename == null){
            alert('Please enter middle name');
            return;
        }

        if(lastname == null){
            alert('Please enter last name');
            return;
        }

        if(code == null){
            alert('Please enter ID');
            return;
        }

        if(accountType == null){
            alert("Please select account type");
            return;
        }
        else{
            if(accountType == 'student'){
                if(college == null){
                    alert("Please select college");
                    return;
                }

                if(year == null){
                    alert("Please select year");
                    return;
                }

            }
            else if(accountType == 'guidance'){
                  if(college == null){
                    alert("Please select college");
                    return;
                }
            }

        }

        if(email == null){
            alert("Please enter email address");
            return;
        }

    



      var userForm = {
        accountType: accountType,
        code: code,
        birthday: d,
        gender: gender,
        firstname: firstname,
        middlename : middlename,
        lastname : lastname,
        email : email,
        college : college,
        course : course,
        year : year


      }
      console.log('user', userForm);
      $http({
          method: 'POST',
          url: API_URL + 'webservice/addAccountInfo',
          data: userForm
      }).
        success(function(data) {
           swal("Success!", "Success Adding profile", "success");
           
        }).
        error(function(data) {
            console.log(data);
            alert('There is something wrong. I don\'t know what it is. But I\'ll make sure it will be fixed soon ');
        });


    }
    
});