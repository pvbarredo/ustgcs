var app = angular.module('colleges', ['oitozero.ngSweetAlert','url.constant'])
app.controller('collegesController', function($scope, $http, API_URL) {

    $http.get(API_URL + "webservice/getColleges")
            .success(function(response) {
                console.log('testRes' ,response);
                $scope.colleges = response;
            }).error(function(response) {
     
            console.log(response);

            alert('This is embarassing. An error has occured. Please check the log for details');
        });

    getNotification = function(){

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
            success(function(data) {
                console.log(data);
                $scope.notification = data;
            }).
            error(function(data) {
                console.log(data);
                alert('There is something wrong');
        });
    }
    getNotification();
	    
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Create College";
                break;
            case 'edit':
                $scope.form_title = "Edit College Detail";
                $scope.id = id;
                $http.get(API_URL + 'webservice/getColleges/' + id)
                        .success(function(response) {
                            console.log(response);
                            $scope.collegiate = response;
                        });
                break;
            default:
                break;
        }
        console.log(id);
        $('#myModal').modal('show');
    }


   $scope.save = function(modalstate, col_id) {
        var url = API_URL + "webservice/createCollege";
        // var name = $scope.college.name;
        console.log('scope', name);

        console.log('col_id', col_id)
        if(col_id != null){
            url += "/" + col_id;
        }
    
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.collegiate),   
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(API_URL);
            console.log(response);

            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        console.log('testId', id);
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                    url: API_URL + 'webservice/deleteCollege/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        location.reload();
                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete');
                    });
        } else {
            return false;
        }
    }



});

