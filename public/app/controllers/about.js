var app = angular.module('about', ['oitozero.ngSweetAlert','url.constant']);

app.controller('aboutController', function($scope, $http, API_URL) {

	getNotification = function(){

        $http({
            method: 'GET',
            url: API_URL + 'webservice/getNotification'
        }).
            success(function(data) {
                $scope.notification = data;
            }).
            error(function(data) {
                swal("Error!", "Please reload the page!", "error");
        });
    }
    getNotification();


    
});
