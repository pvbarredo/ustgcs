@extends('layouts.app')
@section('title')
<title>USTGCS - Inbox</title>
@endsection

@section('css')
<link href="<?= asset('css/angucomplete-alt.css') ?>" rel="stylesheet">
@endsection

@section('app')
<div ng-app="inbox" ng-controller="inboxController" >
@endsection


@section('content')
<div class="container">
	<br/>
	<div class="row">
		<div class="col-md-6">
	@if(Auth::user()->status == 'guidance')
	<button id="createMessage" ng-click="newMessageClicked()" type="button" class="btn btn-primary">Create Message</button>
	@endif
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-6">
			<div class="input-group">
					<input type="text" id="searchInbox"  placeholder="Search inbox" ng-model="searchInbox"  >
					<span class="glyphicon glyphicon-search"> </span>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class = "col-md-12">
			<table class="table table-striped table-condensed">
			  <tr>
			  	@if(Auth::user()->status == 'guidance')
			    <!-- <th>Read by Student</th> -->
			    <th>Recepient Student</th>
			    @endif

			    @if(Auth::user()->status == 'student')
			    <!-- <th>Read</th> -->
			    <th>Guidance Councelor</th>
			    @endif

			    <th>Title</th>
			    <th>Date</th>
			    <th></th>
			  </tr>
			  <tr ng-repeat="userMessage in userMessages | filter:searchInbox">
			  	<!-- <td><span class='glyphicon glyphicon-remove' ng-if="!userMessage.is_read"></span><span class='glyphicon glyphicon-ok' ng-if="userMessage.is_read"></span></td> -->
			  	@if(Auth::user()->status == 'guidance')
			  		
			  		<td>@{{ userMessage.student_name }}</td>
			  	@endif
			  	@if(Auth::user()->status == 'student')
			  		
			  		<td>@{{ userMessage.guidance_name }}</td>
			  	@endif
	            
	            <td>@{{ userMessage.title }}</td>
	            <td>@{{ userMessage.created_at.date | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a'}}</td>

	           	<td><button type="button"  class="btn btn-default" ng-click="viewMessageClicked(userMessage)">View</button> </td>
	        </tr>



			</table>
		</div>
	</div>
	<div class="modal fade" tabindex="-1" role="dialog" id="viewMessage">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">View Message</h4>
	      </div>
	      <div class="modal-body">

	        
		       	<div class="row">
		          <div class="col-md-12">
		            <div class="form-group">
		              <label for="viewMessageTitle">Title</label>
		              <input type="text" id="viewMessageTitle" class="form-control" placeholder="Title of the message" ng-model="viewMessage.title" readonly>
		            </div>
		          </div>
		        </div>
		        <div class="row">
		          <div class="col-md-12">
		            <div class="form-group">
		              <label for="viewMessageMessage">Message</label>
		              <textarea id="viewMessageMessage" class="form-control " rows="5" ng-model="viewMessage.message" name="message" readonly></textarea>
		            </div>
		          </div>
		        </div>
		    
		    <div id="reply"> 
	        	<div class="row">
		          <div class="col-md-10">
		            <div class="form-group">
		              <label for="replyMessage">Reply</label>
	         		 <textarea id="replyMessage" class="form-control " rows="1" ng-model="replyMessage"></textarea>

		            </div>
		          </div>
		          <div class="col-md-2">
		          	<br>
		          	<br>
		          	<button type="button" id="replyButton" class="btn btn-primary" ng-click="sendEmailReply()">Reply</button>
		          </div>


		        </div>	
	        </div> 

	        <div id="emailReply">
	        	<div class="row">
	        		 <div class="col-md-10">
	        			<label>Email Replies</label>
	        		</div>

	        	</div>
	        	
        		<div ng-repeat="email_reply in email_replies">
    			 	<div class="row">
    			 		<div class="col-md-6">
		        			 <div class="panel panel-default" ng-if="email_reply.isCurrentUser">
								  <div class="panel-heading">You 
								  	<br> 
								  	<small><em>@{{ email_reply.reply.status | uppercase }}</em></small><br>
								  	<small><em><time title="@{{ email_reply.feedbackchat.created_at | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a' }}">@{{ email_reply.reply.created_at | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a'}}</time></em></small>
								  </div>
								  <div class="panel-body">@{{ email_reply.reply.message}}</div>
							 </div>
						</div>
						<div class="col-md-6">
		        			 <div class="panel panel-default" ng-if="!email_reply.isCurrentUser">
								  <div class="panel-heading">@{{ email_reply.fullName }} 
								  	<br> 
								  	<small><em>@{{ email_reply.reply.status | uppercase }}</em></small><br>
								  	<small><em><time title="@{{ chat.feedbackchat.created_at | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a' }}">@{{ email_reply.reply.created_at | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a'}}</time></em></small>
								  </div>
								  <div class="panel-body">@{{ email_reply.reply.message}}</div>
							 </div>
						</div>


					</div>
					
					 
        		</div>
        	
	        	
	        </div>   <!-- EMAIL end -->   

	      </div>
	     
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" id="createMessageWindow" data-backdrop="static">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Message</h4>
	      </div>
	      <div class="modal-body">

	      	<div class="row">
	          <div class="col-md-12">
	            <div class="form-group">
	              <label for="refStud">Name of Student</label>
	              <div angucomplete-alt id="search_student" 
			          placeholder="Search student" 
			          maxlength="50" 
			          pause="100" 
			          remote-url= "webservice/getStudentUsingFirstname/"
		              title-field="id,firstname,lastname"
			          minlength="1" 
			          input-class="form-control form-control-small" 
			          match-class="highlight"
			          field-required="true"
			          selected-object="student"
			          focus-out="focusOut()">
		          </div>
	            </div>
	          </div>
	        </div>

	       	
	      	<div id="messageDescription" >
	       	<div class="row">
	          <div class="col-md-12">
	            <div class="form-group">
	              <label for="messageTitle">Title</label>
	              <input type="text" id="messageTitle" class="form-control" placeholder="Title of the message" ng-model="message.title" >
	            </div>
	          </div>
	        </div>
	        <div class="row">
	          <div class="col-md-12">
	            <div class="form-group">
	              <label for="messageMessage">Message</label>
	              <textarea id="messageMessage" class="form-control " rows="5" ng-model="message.message" name="message" ></textarea>
	            </div>
	          </div>
	        </div>
	    </div> <!-- end of message description -->


	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" ng-click="createMessageClicked()">Create Message</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


</div>
@endsection

@section('controller')
<script src="<?= asset('app/controllers/inbox.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angucomplete-alt.js') ?>"></script>
@endsection
