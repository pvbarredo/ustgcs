@extends('layouts.app')
@section('title')
<title>USTGCS - Course</title>
@endsection

@section('app')
<div ng-app="courses" ng-controller="coursesController">
@endsection

@section('content')

  <div class="container">
        <div class="row">
        <br/>
        <br/>
        <h4>Courses</h4>
        

         <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>College</th>
                   
                        <th><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Course</button></th>
                    </tr>
                </thead>
                <tbody>
                   <tr ng-repeat="course in courses">
                        <td>@{{ course.id }}</td>
                        <td>@{{ course.name }}</td>
                        <td>@{{ course.college_name }}</td>
                        <td>
                            <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', course.id)">Edit</button>
                            <button class="btn btn-danger btn-xs btn-delete" ng-click="confirmDelete(course.id)">Delete</button>
                        </td>
                    </tr> 
                </tbody>
            </table>


            </div>

      <div class="container">
       <div class="modal fade" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">@{{form_title}}</h4>
            </div>
            <form name="collegeForm">
            <div class="row">
            	<div class="col-md-12">
                
                    <div class="form-group">
                        <div class = "col-md-4">
                            <label for="courseName">Name</label>
                            <input type="text" class="form-control" name="courseName" placeholder="courseName" ng-model="courseHold.name" required>
                        </div>
                      </div>
                    <div class="form-group">
                        <div class = "col-md-4">
                            <label for="maxYears">Max years</label>
                            <input type="text" class="form-control" name="max_years" placeholder="Max Years" ng-model="courseHold.max_years" required>
                        </div>
                
                    </div>
                    <div class="form-group">
                        <div class = "col-md-4">
                            <label for="collegeName">College</label>
                           <select id="collegeName" class = "form-control"  name="collegeName" ng-model="courseHold.college_id">
                                                    <option value="@{{college.id}}" ng-repeat="college in colleges">@{{college.name}}</option>
                                              
                            </select>
                        </div>
                        
                
                    </div>
                 </div>
             </div>
            
            <br/>
            <div class="row">
            	<div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-5">
                        <button class="btn btn-default" type="button" ng-click="save(modalState, id)">Submit</button>

                    </div>
                </div>
               	</div>
            </div>
            <br/>
            
            
            </form>
           </div>
          </div>
         </div>
       </div>
             
     </div>
            <br/>
          
         
            
                

</div>
@endsection

@section('controller')
<script src="<?= asset('app/controllers/courses.js') ?>"></script>

@endsection      
