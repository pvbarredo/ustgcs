@extends('layouts.app')
@section('title')
<title>USTGCS - Account</title>
@endsection

@section('app')
<div ng-app="addUser" ng-controller="addUserController">
@endsection



@section('content')
<div class="container">

	<ul class="nav nav-tabs">
	    <li class="active"><a data-toggle="tab" href="#single">Single</a></li>
	    <li><a data-toggle="tab" href="#batch">Batch</a></li>
    </ul>



<div class="tab-content">
	<div id="single" class="tab-pane fade in active">
	<div class="row">
		 <div class="col-md-12">
           <h4>Add Single Account</h4>
           <hr/>
        </div>
           <div class="row">
            <div class="form-group">
              <div class = "col-md-5">
                <label for="accountType">Account Type</label>
                  <select id="accountType" class = "form-control"  name="accountType" ng-model="basicInfo.accountType">
                      <option value="student">Student</option>
                      <option value="professor">Professor</option> 
                      <option value="guidance">Guidance</option>
                       @if(Auth::user()->status != 'guidance') 
                      <option value="director">Director</option>                       
                      @endif
                    </select>
              </div>
            </div>
        </div>
         <div class="row" ng-show="basicInfo.accountType">

          <div class="col-md-5">
            <div class="form-group">
              <label for="code">ID</label>
              <input type="text" class="form-control" placeholder="ID"  ng-model="basicInfo.code">
            </div>
          </div>
        </div>
        <div class="row">

          <div class="col-md-5" ng-show="basicInfo.accountType">
            <div class="form-group">
              <label for="firstname">First Name</label>
              <input type="text" class="form-control" placeholder="First Name"  ng-model="basicInfo.firstname">
            </div>
          </div>
        </div>
        <div class="row" ng-show="basicInfo.accountType">
          <div class="col-md-5">
            <div class="form-group">
              <label for="middlename">Middle Name</label>
              <input type="text" class="form-control" id="middlename" placeholder="Middle Name" ng-model="basicInfo.middlename" >
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-5" ng-show="basicInfo.accountType">
            <div class="form-group">
              <label for="lastname">Last Name</label>
              <input type="text" class="form-control" id="lastname" placeholder="Last Name" ng-model="basicInfo.lastname" >
            </div>
          </div>
        </div>
        
        <div class="row">
            <div class="col-md-5" ng-show="basicInfo.accountType">
            <label for="birthday">Birthday</label>
                <div class="form-group">
                  <div class="col-md-6" ng-show="basicInfo.accountType">
                      <select id="month" class = "form-control"  name="month" ng-model="basicInfo.month">
                          <option value="1">January</option>
                          <option value="2">February</option>
                          <option value="3">March</option>
                          <option value="4">April</option>
                          <option value="5">May</option>
                          <option value="6">June</option>
                          <option value="7">July</option>
                          <option value="8">August</option>
                          <option value="9">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>
                      </select>
                  </div>
                  <div class="col-md-2" ng-show="basicInfo.accountType">
                   <input type="text" class = "form-control"  placeholder='DD' ng-model="basicInfo.day">
                  </div>
                  <div class="col-md-4" ng-show="basicInfo.accountType">
                    <input type="text"  class = "form-control" placeholder='YYYY' ng-model="basicInfo.birthYear">
                  </div>
                  
                  
                </div>
            </div>
        </div>


        <div class="row">
          <div class="col-md-5" ng-show="basicInfo.accountType">
            <div class="form-group">
              <label for="radGender">
              Gender:
              </label>
               <div class="form-group">
                  <label class="radio-inline">
                    <input type="radio" name="radGender" id="radMale" ng-model="basicInfo.gender" value="1" checked> Male
                  </label>
            
               
                  <label class="radio-inline">
                    <input type="radio" name="radGender" ng-model="basicInfo.gender" id="radFemale" value="0"> Female
                  </label>
            </div>
          </div>
        </div>
        </div>
        <div class="row" ng-show="basicInfo.accountType">
          <div class="col-md-5">
            <div class="form-group">
              <label for="accEmail">Email</label>
              <input type="email" class="form-control" id="accEmail" placeholder="E-Address" ng-model="basicInfo.email"	>
            </div>
          </div>
        </div>
         <!-- <div class="row" ng-show="basicInfo.accountType">
          <div class="col-md-5">
            <div class="form-group">
              <label for="accPass">Password</label>
              <input type="password" class="form-control" id="accPass" placeholder="Password" ng-model="basicInfo.pass"	>
            </div>
          </div>
        </div>
        <div class="row" ng-show="basicInfo.accountType">
          <div class="col-md-5">
            <div class="form-group">
              <label for="accConPass"> Confirm Password</label>
              <input type="password" class="form-control" id="accConPass" placeholder="Confirm Password" ng-model="basicInfo.confirmpass"	>
            </div>
          </div>
        </div> -->
        <div class="row" ng-show="basicInfo.accountType && basicInfo.accountType != 'director' && basicInfo.accountType != 'professor' ">
          <div class="col-md-5">
            <div class="form-group">
               
                    <label for="collegeName">College</label>
                       <select id="collegeName" class = "form-control"  name="collegeName" ng-model="basicInfo.college_id" ng-change="getTailorCourse(basicInfo.college_id)">
                          <option value="@{{college.id}}" ng-repeat="college in colleges">@{{college.name}}</option>
                                              
                        </select>
   
            </div>
          </div>
        </div>
        <div class="row" ng-show="basicInfo.college_id && basicInfo.accountType == 'student'">
        <div class="col-md-5">
            <div class="form-group">
              <label for="courseName">Course</label>
            	<select id="collegeName" class = "form-control"  name="collegeName" ng-model="basicInfo.course_id" ng-change="getTailorYears(basicInfo.course_id)">
                          <option value="@{{course.id}}" ng-repeat="course in courses">@{{course.name}}</option>
                                              
                 </select>
            </div>
          </div>
        </div>
        <div class="row" ng-show="basicInfo.course_id && basicInfo.accountType == 'student'">
        <div class="col-md-5">
            <div class="form-group">
              <label for="maxYears">Year</label>
            	<select id="maxYears" class = "form-control"  name="maxYears" ng-model="basicInfo.year">
                          <option value="@{{yearDis}}" ng-repeat="yearDis in courseYears">@{{yearDis}}</option>
                                              
                 </select>
            </div>
          </div>
        </div>
        <!-- <div class="row" ng-show="basicInfo.accountType">
          <div class="col-md-5">
              <div class="form-group">
                <label for="accContact">Contact No.</label>
                <input type="text" class="form-control" id="accContact" placeholder="Contact No." ng-model="basicInfo.contact">
              </div>
          </div>
        </div> -->
        <br/>
        <div class="row">
                <div class="form-group">
                    <div class="col-md-5">
                        <button class="btn btn-primary" type="submit" ng-click="addAccountSubmitClicked()">Submit</button>
                    </div>
                </div>
        </div>
	</div>
	<br/>
	<hr/>
	<br/>

	</div>  <!-- single end div -->
	<div id="batch" class="tab-pane fade">
	<div class="row">
	<h4>Batch Add</h4>
	<hr/>
	<br/>
      @if ($isSuccessful)
          <div class="alert alert-success">
            <strong>Success Uploading!</strong> 
          </div>
      @endif
		<form action="{{ url('webservice/uploadCSV') }}" method="post" enctype="multipart/form-data">
			{!! csrf_field() !!}
			<input class="btn btn-default" type="file" name="csv" value="" />
      <br/>
			<input class= "btn btn-primary" type="submit" name="submit" value="Save" />
		</form>
	</div>

	</div>   <!-- eng batch div -->
</div> <!-- end tab content div -->	
</div>
@endsection
@section('controller')

<script src="<?= asset('app/controllers/addAccount.js') ?>"></script>

@endsection   