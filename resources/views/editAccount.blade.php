@extends('layouts.app')
@section('title')
<title>USTGCS - Edit Account</title>
@endsection

@section('app')
<div ng-app="editUser" ng-controller="editUserController">
@endsection

@section('content')
<div class="container">
 <div class="row">
        <div class="col-md-12">
           <h4>Edit Account</h4>
           <hr/>
        </div>

       
        <div class="row">

          <div class="col-md-5">
            <div class="form-group">
              <label for="firstname">First Name</label>
              <input type="text" class="form-control" placeholder="First Name"  ng-model="basicInfo.firstname">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-5">
            <div class="form-group">
              <label for="middlename">Middle Name</label>
              <input type="text" class="form-control" id="middlename" placeholder="Middle Name" ng-model="basicInfo.middlename" >
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-5">
            <div class="form-group">
              <label for="lastname">Last Name</label>
              <input type="text" class="form-control" id="lastname" placeholder="Last Name" ng-model="basicInfo.lastname" >
            </div>
          </div>
        </div>
        @if (Auth::user()->status !== 'admin' )
        <div class="row">
          <div class="col-md-5">
            <div class="form-group">
              <label for="accEmail">Email</label>
              <input type="email" class="form-control" id="accEmail" placeholder="E-Address" ng-model="basicInfo.email" >
            </div>
          </div>
        </div>
        @endif
      
        <!-- <div class="row">
          <div class="col-md-5">
            <div class="form-group">
               
                    <label for="collegeName">College</label>
                     @if (Auth::user()->status === 'student' )
                       <select id="collegeName" class = "form-control"  name="collegeName" ng-model="basicInfo.college_id"  convert-to-number ng-change="getTailorCourse(basicInfo.college_id)">
                      @else
                      <select id="collegeName" class = "form-control"  name="collegeName" ng-model="basicInfo.college_id">
                      @endif

                          <option value="@{{college.id}}" ng-repeat="college in colleges" ng-selected="@{{college.id === basicInfo.college_id}}">@{{college.name}}</option>
                                              
                        </select>
   
            </div>
          </div>
        </div> -->
         @if (Auth::user()->status === 'student' )
        <div class="row" ng-if="!basicInfo.collegeId != null">
        <div class="col-md-5">
            <div class="form-group">
              <label for="courseName">Course</label>
              <select id="collegeName" class = "form-control"  name="collegeName" ng-model="basicInfo.course_id" ng-change="getTailorYears(basicInfo.course_id)">
                          <option value="@{{course.id}}" ng-repeat="course in courses">@{{course.name}}</option>
                                              
                 </select>
            </div>
          </div>
        </div>
        <div class="row" ng-if="!basicInfo.course_id != null">
        <div class="col-md-5">
            <div class="form-group">
              <label for="maxYears">Year</label>
              <select id="maxYears" class = "form-control"  name="maxYears" ng-model="basicInfo.year">
                          <option value="@{{yearDis}}" ng-repeat="yearDis in courseYears">@{{yearDis}}</option>
                                              
                 </select>
            </div>
          </div>

        </div>
        @endif
       <!--  <div class="row">
          <div class="col-md-5">
              <div class="form-group">
                <label for="accContact">Contact No.</label>
                <input type="text" class="form-control" id="accContact" placeholder="Contact No." ng-model="basicInfo.contact">
              </div>
          </div> -->
        </div>
        <div class="row">
                <div class="form-group">
                    <div class="col-md-5">
                        <button class="btn btn-default" type="submit" ng-click="editAccountSubmitClicked()">Submit</button>
                    </div>
                </div>
        </div>



        </div>
</div>


@endsection
@section('controller')
<script src="<?= asset('app/controllers/user.js') ?>"></script>

@endsection    