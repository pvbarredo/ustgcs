@extends('layouts.app')
@section('title')
<title>USTGCS - Referral</title>
@endsection

@section('css')
<link href="<?= asset('css/angucomplete-alt.css') ?>" rel="stylesheet">
@endsection

@section('app')
<div ng-app="referral" ng-controller="referralController">
@endsection

@section('content')
<div class="container">
	@if(Auth::user()->status == 'professor')
	<br>
	<div class="text-right"><button id="newReferral" ng-click="newReferral()" type="button" class="btn btn-primary">Create New Referral</button></div>
	@endif

	<div class="row">

        <div class = "col-md-12">
        <hr/>
        <table class="table table-striped table-condensed">
          <tr>
          	<th>Referred Student</th>

          	

            @if(Auth::user()->status == 'guidance')
            <th>Professor</th>
            @endif

            <th>Date</th>
            <th></th>
            
          </tr>

          <tr ng-repeat="referral in referrals">
            <td>@{{ referral.student_name }}</td>


            @if(Auth::user()->status == 'guidance')
            <td>@{{ referral.professor_name }}</td>
            @endif

            <td>@{{ referral.created_date.date | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a' }}</td>
           	<td><button id="viewReferral" type="button" ng-click="toggleViewReferral(referral)" class="btn btn-default">View</button> </td>
        </tr>
          

        </table>
        </div>

      </div>



      <!-- modal for viewing each referral -->
      <div class="modal fade" id="viewReferralModal">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><h4>Referral Description</h4>
	      </div>
	      <div class="modal-body">
	       	<div class="row">
	          <div class="col-md-12">
	            <div class="form-group">
	              <label for="studName">Student Name</label>
	              <input type="text" class="form-control" id="studName" placeholder="Student Name" ng-model="viewReferral.student_name" readonly>
	            </div>
	          </div>
	        </div>
	        <div class="row">
	          <div class="col-md-12">
	            <div class="form-group">
	              <label for="refMessage">Message</label>
	              <textarea id="refMessage" class="form-control " rows="5" ng-model="viewReferral.message" name="message" readonly></textarea>
	            </div>
	          </div>
	        </div>
	                <div class="row">
				        	<div class="col-md-12">

				            <div class="form-group">
				              <label for="refDesc">Description</label>
				              <textarea id="refDesc" class="form-control " rows="5" ng-model="viewReferral.description" name="description" readonly></textarea>
				            </div>
				          
				        	</div>

				        </div>

	        
	        <div id="reply"> 
	        	<div class="row">
		          <div class="col-md-10">
		            <div class="form-group">
		              <label for="replyMessage">Reply</label>
             		 <textarea id="replyMessage" class="form-control " rows="1" ng-model="replyMessage" ng-change="replyMessageChanged()"></textarea>

		            </div>
		          </div>
		          <div class="col-md-2">
		          	<br>
		          	<br>
		          	<button type="button" id="replyButton" class="btn btn-primary" ng-click="sendFeedbackchat(viewReferral)">Reply</button>
		          </div>


		        </div>	
	        </div>
	        <div id="feedbackchat">
	        	<div class="row">
	        		 <div class="col-md-10">
	        			<label>Feedback Discussions</label>
	        		</div>

	        	</div>
	        	
        		<div ng-repeat="chat in chats">
    			 	<div class="row">
    			 		<div class="col-md-6">
		        			 <div class="panel panel-default" ng-if="chat.isCurrentUser">
								  <div class="panel-heading">You 
								  	<br> 
								  	<small><em>@{{ chat.feedbackchat.user_status | uppercase }}</em></small><br>
								  	<small><em><time title="@{{ chat.feedbackchat.created_at | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a' }}">@{{ chat.feedbackchat.created_at | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a'}}</time></em></small>
								  </div>
								  <div class="panel-body">@{{ chat.feedbackchat.message}}</div>
							 </div>
						</div>
						<div class="col-md-6">
		        			 <div class="panel panel-default" ng-if="!chat.isCurrentUser">
								  <div class="panel-heading">@{{ chat.fullname }} 
								  	<br> 
								  	<small><em>@{{ chat.feedbackchat.user_status | uppercase }}</em></small><br>
								  	<small><em><time title="@{{ chat.feedbackchat.created_at | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a' }}">@{{ chat.feedbackchat.created_at | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a'}}</time></em></small>
								  </div>
								  <div class="panel-body">@{{ chat.feedbackchat.message}}</div>
							 </div>
						</div>


					</div>
					
					 
        		</div>
        	
	        	
	        </div>   <!-- feedback chat end -->

	       
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        @if(Auth::user()->status == 'professor')
	        <!-- <button type="button" class="btn btn-primary">Delete</button> -->
	        @endif
	        @if(Auth::user()->status == 'guidance')
	        <!-- <button type="button" id="replyButton" class="btn btn-primary" ng-click="sendFeedback(viewReferral)">Reply</button> -->
	        @endif
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

      <!-- end of modal for viewing each referral -->





      <!-- modal -->

      
      <div class="modal fade" id="myModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Create Referral</h4>
	      </div>
	      <div class="modal-body">
	        	     
				        <div class="row">
				          <div class="col-md-12">
				            <div class="form-group">
				              <label for="refStud">Name of Referred Student</label>

				              <div angucomplete-alt id="search_student" 
						          placeholder="Search student" 
						          maxlength="50" 
						          pause="100" 
						          remote-url= "webservice/getStudentUsingFirstname/"
					              title-field="id,firstname,lastname"
						          minlength="1" 
						          input-class="form-control form-control-small" 
						          match-class="highlight"
						          field-required="true"
						          selected-object="student"
						          focus-out="focusOut()">
					          </div>
				            </div>
				          </div>
				        </div>

				   <!-- display after selecting student      -->
				   <div id="referral-form">
			   		<div class="row">
				          <div class="col-md-12">
				            <div class="form-group">
				              <label for="refCourse">College</label>
				              <input type="text" class="form-control" id="refCourse" placeholder="College" ng-model="college" readonly>
				            </div>
				          </div>
				        </div>
				        <div class="row">
				          <div class="col-md-12">
				            <div class="form-group">
				              <label for="refCourse">Course</label>
				              <input type="text" class="form-control" id="refCourse" placeholder="Course" ng-model="course" readonly>
				            </div>
				          </div>
				        </div>
				        <div class="row">
				          <div class="col-md-12">
				            <div class="form-group">
				              <label for="refYearSec">Year & Section</label>
				              <input type="text" class="form-control " id="refYearSec" placeholder="Year & Section" ng-model="year" readonly >
				            </div>
				          </div>
				        </div>
				        <br/>
				        <h5><strong>Reason/s for Referral</strong></h5>
				        <div class="row">
					        <div class="col-md-4">
					          <label class="checkbox-inline">
					          <input type="checkbox" class="referralCheckbox" ng-model="checkbox.reason1"  >
					          Absenteeism/Tardiness
					          </label>
					        </div>
					        <div class="col-md-4">
					          <label class="checkbox-inline">
					          <input type="checkbox" class="referralCheckbox" ng-model="checkbox.reason2" >
					          Withdrawal From Group Activities
					          </label>
					        </div>
					        <div class="col-md-4">
					          <label class= "checkbox-inline">
					          <input type="checkbox" class="referralCheckbox" ng-model="checkbox.reason3"  >
					          Deteriorating Performance
					          </label>
					        </div>
				       </div>
				       <div class="row">
					        <div class="col-md-4">
					          <label class="checkbox-inline">
					          <input type="checkbox" class="referralCheckbox" ng-model="checkbox.reason4" >
					          Course Indecision
					          </label>
					        </div>
					        <div class="col-md-4">
					          <label class="checkbox-inline">
					          <input type="checkbox" class="referralCheckbox" ng-model="checkbox.reason5" >
					          Lack of Interest
					          </label>
					        </div>
					        <div class="col-md-4">
					          <label class="checkbox-inline">
					          <input type="checkbox" class="referralCheckbox" ng-model="checkbox.reason6" >
					          Dropping School
					          </label>
					        </div>
				       </div>
				       <div class="row">
					        <div class="col-md-4">
					          <label class="checkbox-inline">
					          <input type="checkbox" class="referralCheckbox" ng-model="checkbox.reason7" >
					          High Anxiousness
					          </label>
					        </div>
					        <div class="col-md-4">
					          <label class="checkbox-inline">
					          <input type="checkbox" class="referralCheckbox" ng-model="checkbox.reason8" >
					          Family Problem
					          </label>
					        </div>
					        <div class="col-md-4">
					          <label class="checkbox-inline">
					          <input type="checkbox" class="referralCheckbox" ng-model="checkbox.reason9" >
					          Misbehavior in Class
					          </label>
					        </div>
				       </div>
				       <div class="row">
				        <div class="col-md-4">
				          <label class="checkbox-inline">
				          <input type="checkbox" class="referralCheckbox" ng-model="checkbox.reason10" >
				          Member of a Not Recognized Organization
				          </label>
				        </div> 
				        <div class="col-md-4 form-inline">
				          <label class="checkbox-inline">
				          <input type="checkbox"  ng-change="othersSelected()" ng-model="checkbox.reason11"  >
				          Others
				          </label>
				          <input type="text" class="form-control" id="reasonSpec" ng-model="reasonSpec" placeholder="Please specify..."  disabled>
				        </div>
				        </div>
				        <div class="row">
				        	<div class="col-md-12">

				            <div class="form-group">
				              <label for="refDesc">Description</label>
				              <textarea id="refDesc" class="form-control " rows="5" ng-model="desc" name="desc"></textarea>
				            </div>
				          
				        	</div>

				        </div>
				        <div class="row">
				        <div class="col-md-12">
				        <!-- <div class="form-group">
				              <label for="refStud">Name of guidance</label>
				              <div angucomplete-alt id="search_student" 
						          placeholder="Search guidance" 
						          maxlength="50" 
						          pause="100" 
						          remote-url= "webservice/getGuidanceUsingFirstname/"
					              title-field="firstname,lastname"
						          minlength="1" 
						          input-class="form-control form-control-small" 
						          match-class="highlight"
						          field-required="true"
						          selected-object="guidance"
						          focus-out="focusOut()">
					          </div>
					          </div> -->	
				          </div>
				       </div>

			        </div>       
		        <!-- END - display after selecting student      -->
				     
	      </div>
	      <div class="modal-footer">
	      	<input type="hidden" name="professor_id" ng-init="professor_id={{ Auth::user()->code }};" ng-value="professor_id"/>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" id="sendReferralButton" class="btn btn-primary" ng-click="sendReferral()">Send Referral</button>
	      </div>
	    </div>
	  </div>
	  <!-- end modal -->
</div>
</div>
@endsection
@section('controller')
<script src="<?= asset('app/controllers/referral.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angucomplete-alt.js') ?>"></script>
@endsection