@extends('layouts.app')
@section('title')
<title>USTGCS - INDEX</title>
@endsection

@section('app')
<div ng-app="announcement" ng-controller="announcementController" >
@endsection

@section('content')

<div class="container">
      <br>
      <div class="text-right">

     @if (Auth::user()->status != "student")
      <button id="newAnnouncement" ng-click="newAnnouncement()" type="button" class="btn btn-primary">Create New Announcement</button>   
     @endif
      <br>
        <div class="input-group">
          <input type="text" id="searchAnnouncement"  placeholder="Search Announcement" ng-model="searchAnnouncement" >
          <span class="glyphicon glyphicon-search"> </span>
          
        </div>
      </div>
      <br>
    

      <!-- Modal popup for new announcement creation -->
      <div class="modal fade" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Create New Announcement</h4>
            </div>
            <div class="modal-body">


                 <form class="form-horizontal" role="form" method="POST">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="title" ng-model="title" value="{{ old('title') }} " required="true">

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Message</label>

                            <div class="col-md-6">
                                <textarea class="form-control " rows="5" ng-model="message" name="message" value="{{ old('message') }} " required="true"></textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Receivers</label>

                            <div class="col-md-6">
                               <div class="checkbox">
                                  <label><input type="checkbox" class="receiver-All" id="all-receiver" ng-change="allCheckboxChanged()" ng-model="checkboxes.all">All</label>
                                </div>
                                <div class="checkbox">
                                  <label><input type="checkbox" class="receiver-notAll" ng-model="checkboxes.first">First Year</label>
                                </div>
                                <div class="checkbox">
                                  <label><input type="checkbox" class="receiver-notAll" ng-model="checkboxes.second">Second Year</label>
                                </div>
                                <div class="checkbox">
                                  <label><input type="checkbox" class="receiver-notAll" ng-model="checkboxes.third">Third Year</label>
                                </div>
                                <div class="checkbox">
                                  <label><input type="checkbox" class="receiver-notAll" ng-model="checkboxes.fourth">Fourth Year</label>
                                </div>
                                <div class="checkbox">
                                  <label><input type="checkbox" class="receiver-notAll" ng-model="checkboxes.fifth">Fifth Year</label>
                                </div>
                                
                            </div>

                             <label class="col-md-4 control-label">Colleges</label>
                             <div class="col-md-6">
                              <div ng-repeat="college in colleges">
                                <div class="checkbox">
                                    <label><input type="checkbox" class="receiver-notAll" ng-model="x" ng-change="setCollegeCheckbox(college)">@{{college.name}}</label>
                                </div>
                              </div>
                            
                            
                              </div>
                              <label class="col-md-4 control-label">Courses</label>
                             <div class="col-md-6">
                             
                              <div ng-repeat="course in courses">
                                  <div class="checkbox">
                                      <label><input type="checkbox" class="receiver-notAll" ng-model="x" ng-change="setCourseCheckbox(course)">@{{course.name}}</label>
                                  </div>
                              </div>
                            
                              </div>
                             

                       
                           

                        
                            
                            
                            
                        </div>
                  

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary" ng-click="createAnnouncement()">Create</button>
            </div>
            </form>
          </div>
        </div>
      </div>

      <!-- End of Modal Popup -->


       <div ng-repeat="announcement in announcements | filter:searchAnnouncement">
           <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">

                  <div ng-if="flag[$index].value  == 0">
                    <strong><a class="announcement-title" ng-href=" @{{ API_URL + 'announcement/' +flag[$index].id }}">@{{ announcement.title | uppercase}}</a></strong>
                  </div>
                  <div ng-if="flag[$index].value  == 1">
                     <a class="announcement-title" ng-href=" @{{ API_URL + 'announcement/' +flag[$index].id }}">@{{ announcement.title | uppercase }}</a>
                  </div>
                </h3>
                 <small><em>Posted by: @{{ flag[$index].created_by}}</em></small><br>

                 <small><em><time title="@{{ announcement.created_at | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a' }}">Date Posted: @{{ announcement.created_at | amDateFormat: 'dddd, MMMM Do YYYY, h:mm a'}}</time></em></small>
              </div>
              <div class="panel-body">
                  @{{ announcement.message }}
                   
              </div>
            </div>
      </div>
     
  <br>    
</div>
@endsection

@section('controller')
<script src="<?= asset('app/controllers/announcement.js') ?>"></script>

@endsection