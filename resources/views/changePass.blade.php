@extends('layouts.app')
@section('title')
<title>USTGCS - Change Password</title>
@endsection

@section('app')
<div ng-app="changePassword" ng-controller="changePasswordController">
@endsection

@section('content')

  <div class="container">
        <div class="row">
        <br/>
        <br/>
        <h4>Change Password</h4>
    
        <hr/>
            
            <form name="changePassForm" >
            <div class="row">
                
                    <div class="form-group">
                        <div class = "col-md-5">
                            <label for="oldPass">Old Password</label>
                            <input type="password" class="form-control" name="oldPass" placeholder="Old Password" ng-model="oldPass" required>
                        </div>
                        
                
                  </div>
            </div>
           
            <div class="row">
                    <div class="form-group">
                        <div class = "col-md-5">
                            <label for="newPass">New Password</label>
                            <input type="password" class="form-control" name="newPass" placeholder="New Password" ng-model="newPass" required>
                        </div>
                    </div>
            </div>
            <div class="row">
                     <div class="form-group">
                        <div class = "col-md-5">
                            <label for="confirmNewPass">Confirm New Password</label>
                            <input type="password" class="form-control" name="confirmNewPass" placeholder="Confirm New Password" ng-model="confirmNewPass" required>
                        </div>
                    </div>
            </div>
            <br/>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-5">
                        <button class="btn btn-default" type="submit" ng-click="changePasswordSubmitClicked()">Submit</button>

                    </div>
                </div>
            </div>

            
            
            </form>
             
            </div>
            <br/>
          
         
            
                

          </div>
@endsection

@section('controller')
<script src="<?= asset('app/controllers/user.js') ?>"></script>

@endsection      
