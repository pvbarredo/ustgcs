@extends('layouts.app')
@section('title')
<title>USTGCS - Generated Profiling</title>
@endsection

@section('app')
<div ng-app="report" ng-controller="generatedProfilingController">
@endsection

@section('content')
<div class="container">

	
	@if($displays['displayGender'] )
	    <div id="chart-div"></div>
	    @piechart('Gender', 'chart-div')
 	@endif

 	@if($displays['displayReligion'] )
	    <div id="religion-chart-div"></div>
	    @piechart('Religion', 'religion-chart-div')
	@endif

	@if($displays['displayCity'] )
	    <div id="perm-city-chart-div"></div>
	    @piechart('Permanent City', 'perm-city-chart-div')

	    <div id="curr-city-chart-div"></div>
	    @piechart('Current City', 'curr-city-chart-div')

	@endif

	@if($displays['displayLivingArrangements'] )
	    <div id="livingArrangement-chart-div"></div>
	    @piechart('Living Arrangement', 'livingArrangement-chart-div')
	@endif

    @if($displays['displayHealths'] )
	    <div id="health-chart-div"></div>
	    @piechart('Health Chart', 'health-chart-div')
	@endif

	@if($displays['displayAssessments'] )
	    <div id="assessment-chart-div"></div>
	    @piechart('Assessment Chart', 'assessment-chart-div')
	@endif

	@if($displays['displayCounselings'] )
	    <div id="counceling-chart-div"></div>
	    @piechart('Counceling Chart', 'counceling-chart-div')
	@endif

	@if($displays['displayMaritals'] )
	    <div id="marital-chart-div"></div>
	    @piechart('Marital Chart', 'marital-chart-div')
	@endif

	@if($displays['displayAbroads'] )
	    <div id="abroad-chart-div"></div>
	    @piechart('Abroad Chart', 'abroad-chart-div')
	@endif

	@if($displays['displayDeceased'] )
	    <div id="deceased-chart-div"></div>
	    @piechart('Deceased Chart', 'deceased-chart-div')
	@endif
    

</div>
@endsection


@section('controller')
<script src="<?= asset('app/lib/angular/chart.js/dist/Chart.min.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-chart.js/dist/angular-chart.min.js') ?>"></script>
<script src="<?=asset('app/lib/angular/pdfmake/build/pdfmake.min.js') ?>" ></script>
<script src="<?=asset('app/lib/angular/pdfmake/build/vfs_fonts.js') ?>" ></script>
<script src="<?= asset('app/controllers/report.js') ?>"></script>

@endsection      
  
