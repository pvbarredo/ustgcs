@extends('layouts.app')
@section('title')
<title>USTGCS - Generated Profiling</title>
@endsection

@section('app')
<div ng-app="report" ng-controller="generateCommonProblemController">
@endsection

@section('content')
<div class="container">

	
	<div id="common-chart-div"></div>
	@piechart('Common Problems', 'common-chart-div')

	<div id="commonBar-chart-div"></div>
	@barchart('Common Problems', 'commonBar-chart-div')

</div>
@endsection


@section('controller')
<script src="<?= asset('app/controllers/report.js') ?>"></script>
@endsection      
  
