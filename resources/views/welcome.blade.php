@extends('layouts.app')

@section('title')
<title>PETER WELCOME PAGE WITH OFFLINE DEPENDENCY</title>
@endsection
@section('content')
<div ng-app="simplePage" ng-controller="simplePageController">
<div class="container">
    <h2> SIMPLE ANGULAR BOOTSTRAP </h2>
    <br>
    <div class="row"> 
        <table class="table">
            <thead>
                    <tr>
                        <th>ID</th>
                        <th>Code</th>
                        <th>Firstname</th>
                        <th>Middlename</th>
                        <th>Lastname</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th><button id="btn-add" class="btn btn-primary btn-xs" ng-click="getAllUsers()">Get All User</button></th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="student in users">
                        <td>@{{ student.id }}</td>
                        <td>@{{ student.code }}</td>
                        <td>@{{ student.firstname }}</td>
                        <td>@{{ student.middlename }}</td>
                        <td>@{{ student.lastname }}</td>
                        <td>@{{ student.email }}</td>
                        <td>@{{ student.status }}</td>
                        <td>
                           
                        </td>
                    </tr>
                </tbody>
        </table>

   </div> 
   <div id="chart-div"></div>
    

    {!! Form::open(['action'=>'ImageController@store', 'files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('image', 'Choose an image') !!}
            {!! Form::file('image') !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Save', array( 'class'=>'btn btn-danger form-control' )) !!}
        </div>

        {!! Form::close() !!}
</div>
</div>
@endsection
@section('controller')
<script src="<?= asset('app/controllers/simplePage.js') ?>"></script>
@endsection