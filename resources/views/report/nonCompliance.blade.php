@extends('layouts.app')
@section('title')
<title>USTGCS - Non Compliance Report</title>
@endsection

@section('app')
<div ng-app="report" ng-controller="nonComplianceController">
@endsection

@section('content')
<div class="container">
    <br>
    <br>
    <p><button ng-click="send_notification_clicked()" class="btn btn-success">SEND NOTIFICATION</button>
    <button ng-click="pdf_clicked()" class="btn btn-success">PDF</button></p>
    <h4>@{{ nonCompliant.year}} </h4>
    <table class="table table-striped table-condensed">
          <tr>
          	<th>Student Name</th>
            <th>College</th>
            <th>Course</th>
            <th>Year</th>
          </tr>

          <tr ng-repeat="nonCompliant in nonCompliants">
            <td>@{{ nonCompliant.firstname + ' ' + nonCompliant.lastname }}</td>
            <td>@{{ nonCompliant.college_name}}</td>
            <td>@{{ nonCompliant.course_name}}</td>
            <td>@{{ nonCompliant.year}}</td>
           	
        </tr>
          

    </table>

</div>
@endsection

@section('controller')
<script src="<?= asset('app/lib/angular/chart.js/dist/Chart.min.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-chart.js/dist/angular-chart.min.js') ?>"></script>
<script src="<?=asset('app/lib/angular/pdfmake/build/pdfmake.min.js') ?>" ></script>
<script src="<?=asset('app/lib/angular/pdfmake/build/vfs_fonts.js') ?>" ></script>
<script src="<?= asset('app/controllers/report.js') ?>"></script>

@endsection      
