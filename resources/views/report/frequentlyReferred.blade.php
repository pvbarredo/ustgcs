@extends('layouts.app')
@section('title')
<title>USTGCS - Non Compliance Report</title>
@endsection

@section('app')
<div ng-app="report" ng-controller="frequentlyReferredController">
@endsection

@section('content')
<div class="container">
    <br>
    <br>
   
    <button ng-click="pdf_clicked()" class="btn btn-success">PDF</button></p>
    <table class="table table-striped table-condensed">
          <tr>
          	<th>Referred Student</th>
            <th>Total</th>
          </tr>

          <tr ng-repeat="frequentlyReferredStudent in frequentlyReferredStudents">
            <td>@{{ frequentlyReferredStudent.firstname + ' ' + frequentlyReferredStudent.lastname }}</td>
            <td>@{{ frequentlyReferredStudent.total }}</td>
           	
        </tr>
          

    </table>

</div>
@endsection

@section('controller')
<script src="<?= asset('app/lib/angular/chart.js/dist/Chart.min.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-chart.js/dist/angular-chart.min.js') ?>"></script>
<script src="<?=asset('app/lib/angular/pdfmake/build/pdfmake.min.js') ?>" ></script>
<script src="<?=asset('app/lib/angular/pdfmake/build/vfs_fonts.js') ?>" ></script>
<script src="<?= asset('app/controllers/report.js') ?>"></script>

@endsection      
