@extends('layouts.app')
@section('title')
<title>USTGCS - Common Problem Report</title>
@endsection

@section('app')
<div ng-app="report" ng-controller="commonProblemController">
@endsection

@section('content')
<div class="container">
    <h4>Report</h4>  
    <div class="row">
      <!-- <form class="form-horizontal" role="form" method="POST" action="{{ url('/webservice/commonProblem') }}"> -->
                  {!! csrf_field() !!}
    	<div class="form-group">
                    <label class="col-md-4 control-label">Year</label>

                    <div class="col-md-6">
                        <div class="checkbox">
                          <label><input type="checkbox" name="year[1]" ng-model="checkboxes.first">First Year</label>
                        </div>
                        <div class="checkbox">
                          <label><input type="checkbox" name="year[2]" ng-model="checkboxes.second">Second Year</label>
                        </div>
                        <div class="checkbox">
                          <label><input type="checkbox" name="year[3]" ng-model="checkboxes.third">Third Year</label>
                        </div>
                        <div class="checkbox">
                          <label><input type="checkbox" name="year[4]" ng-model="checkboxes.fourth">Fourth Year</label>
                        </div>
                        <div class="checkbox">
                          <label><input type="checkbox" name="year[5]" ng-model="checkboxes.fifth">Fifth Year</label>
                        </div>
                        
                    </div>

                     <!-- <label class="col-md-4 control-label">Colleges</label>
                     <div class="col-md-6">
                      <div ng-repeat="college in colleges">
                        <div class="checkbox">
                            <label><input type="checkbox" name="college[@{{college.id}}]" ng-model="x" ng-change="setCollegeCheckbox(college)">@{{college.name}}</label>
                        </div>
                      </div>
                    
                    
                      </div> -->
                      <label class="col-md-4 control-label">Courses</label>
                     <div class="col-md-6">
                     
                      <div ng-repeat="course in courses">
                          <div class="checkbox">
                              <label><input type="checkbox" name="course[@{{course.id}}]" ng-model="x" ng-change="setCourseCheckbox(course)">@{{course.name}}</label>
                          </div>
                      </div>

                      <br>
                      <!-- <button type="submit" class="btn btn-primary" >GENERATE</button> -->
                      <button class="btn btn-primary" ng-click="generateReport()" >GENERATE</button>
                      </div>
                      
                </div>


              <!-- </form> -->
              <!-- MODAL FOR BAR GRAPH -->
    <div class="modal fade" id="graphs">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Graphs</h4>
          </div>
          <div class="modal-body">

              <!-- Common Problem -->
              <div class="row" >
                <canvas class="chart chart-bar common-chart" chart-title='graph.common.title' chart-data="graph.common.data" chart-labels="graph.common.labels" chart-series="graph.common.series" chart-options="graph.common.options" ></canvas>
              </div>

              <div class="row" >
                <canvas class="chart chart-pie common-chart" chart-data="graph.common.data" chart-labels="graph.common.labels" chart-series="graph.common.series" chart-options="graph.common.options" ></canvas>
              </div>

              <div class="row" >
                <canvas class="chart chart-line common-chart" chart-data="graph.common.data" chart-labels="graph.common.labels" chart-series="graph.common.series" chart-options="graph.common.options" ></canvas>
              </div>
              <!-- end of common problem -->
              </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button ng-click="pdf()" class="btn btn-success">PDF</button></p>
          </div>
        </div>
      </div>
    </div>

    </div>  

</div>
@endsection

@section('controller')
<script src="<?= asset('app/lib/angular/chart.js/dist/Chart.min.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-chart.js/dist/angular-chart.min.js') ?>"></script>
<script src="<?=asset('app/lib/angular/pdfmake/build/pdfmake.min.js') ?>" ></script>
<script src="<?=asset('app/lib/angular/pdfmake/build/vfs_fonts.js') ?>" ></script>
<script src="<?= asset('app/controllers/report.js') ?>"></script>

@endsection      
