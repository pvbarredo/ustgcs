@extends('layouts.app')
@section('title')
<title>USTGCS - Student Profiling Report</title>
@endsection

@section('app')
<div ng-app="report" ng-controller="studentProfilingController">
@endsection

@section('content')
<div class="container">
    
    <h4>Report</h4>  
    <div class="row">
    	<div class="col-md-4">
    		<div class="form-group">
                    <label class="col-md-4 control-label">Year</label>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/webservice/studentProfiling') }}">
                  {!! csrf_field() !!}
                    <div class="col-md-6">
                        <div class="checkbox">
                          <label><input type="checkbox" name="year[1]" ng-model="checkboxes.first">First Year</label>
                        </div>
                        <div class="checkbox">
                          <label><input type="checkbox" name="year[2]" ng-model="checkboxes.second">Second Year</label>
                        </div>
                        <div class="checkbox">
                          <label><input type="checkbox" name="year[3]" ng-model="checkboxes.third">Third Year</label>
                        </div>
                        <div class="checkbox">
                          <label><input type="checkbox" name="year[4]" ng-model="checkboxes.fourth">Fourth Year</label>
                        </div>
                        <div class="checkbox">
                          <label><input type="checkbox" name="year[5]" ng-model="checkboxes.fifth">Fifth Year</label>
                        </div>
                        
                    </div>

                     <!-- <label class="col-md-4 control-label">Colleges</label>
                     <div class="col-md-6">
                      <div ng-repeat="college in colleges">
                        <div class="checkbox">
                            <label><input type="checkbox" name="college[@{{college.id}}]" ng-model="x" ng-change="setCollegeCheckbox(college)">@{{college.name}}</label>
                        </div>
                      </div>
                      </div> -->


                      <label class="col-md-4 control-label">Courses</label>
                     <div class="col-md-6">
                     
                      <div ng-repeat="course in courses">
                          <div class="checkbox">
                              <label><input type="checkbox" name="course[@{{course.id}}]" ng-model="x" ng-change="setCourseCheckbox(course)">@{{course.name}}</label>
                          </div>
                      </div>
                    
                      </div>
                      
                </div>

    	</div>

    	<div class="col-md-8">

    		<div class="row">
    			<b>I. Identifying Data</b>
    			
    		</div>
        <br>
    		<div class="row">

    			<input type="checkbox" name="data[gender]" ng-model="check.data.gender"> Gender &nbsp;
         
          
    			<input type="checkbox" name="data[religion]" ng-model="check.data.religion"> Religion&nbsp;

    			<!-- <input type="checkbox" name="data[civil]" ng-model="check.data.civilStatus"> Civil Status&nbsp; -->
    			<input type="checkbox" name="data[city]" ng-model="check.data.city"> City&nbsp;
    			<input type="checkbox" name="data[living]" ng-model="check.data.livingArrangement"> Current Living Arrangement

    		</div>
        <br>
    		<div class="row">
    			<b>II. Physical/Mental Health Information</b>
    			
    		</div>
        <br>
    		<div class="row">
    			<input type="checkbox" name="health[limitation]" ng-model="check.health.acad_health_prob"> Health Problems or Physical Limitation &nbsp;
    			<input type="checkbox" name="health[assessment]" ng-model="check.health.psychiatric_help"> Psychiatric assessment or help &nbsp;
    			<input type="checkbox" name="health[counseling]" ng-model="check.health.counseling"> Undergone Counseling
    			<br>
    		</div>
        <br>
    		<div class="row">
    			<b>III. Family Information</b>
    			
    		</div>
        <br>
    		<div class="row">
    			<input type="checkbox" name="family[maritalStatus]" ng-model="check.info.maritalStatus"> Marital Status of Parents &nbsp;
    			<input type="checkbox" name="family[abroad]" ng-model="check.info.abroad"> Parents Working Abroad &nbsp;
    			<input type="checkbox" name="family[deceased]" ng-model="check.info.deceased"> Deceased Parent
    			
    		</div>
        <br>
    		<div class="row">
    			<b>IV. Vocational Academic Placement</b>
    			
    		</div>
        <br>
    		<div class="row">
    			<input type="checkbox" name="family[motivation]" ng-model="check.info.motivation"> Motivation to finish the program &nbsp;
    			<input type="checkbox" name="family[help]" ng-model="check.info.help"> Reason for seeking the help of a counselor
    			
    		</div>
        <br>
        <!-- <button type="submit" class="btn btn-primary" >GENERATE</button> -->
        
      </form>
      <button class="btn btn-primary" ng-click="generateReport()" >GENERATE</button>
    	</div>
    </div>   

    <!-- MODAL FOR BAR GRAPH -->
    <div class="modal fade" id="graphs">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Graphs</h4>
          </div>
          <div class="modal-body">

              <!-- gender -->
              <div class="row" ng-if="check.data.gender">
                <canvas class="chart chart-bar" chart-title='graph.gender.title' chart-data="graph.gender.data" chart-labels="graph.gender.labels" chart-series="graph.gender.series" chart-options="graph.gender.options" ></canvas>
              </div>

              <div class="row" ng-if="check.data.gender">
                <canvas class="chart chart-pie" chart-data="graph.gender.data" chart-labels="graph.gender.labels" chart-series="graph.gender.series" chart-options="graph.gender.options" ></canvas>
              </div>

              <div class="row" ng-if="check.data.gender">
                <canvas class="chart chart-line" chart-data="graph.gender.data" chart-labels="graph.gender.labels" chart-series="graph.gender.series" chart-options="graph.gender.options" ></canvas>
              </div>
              <!-- end of gender -->

              <!-- religion -->
              <div class="row" ng-if="check.data.religion">
                <canvas class="chart chart-bar" chart-title='graph.religion.title' chart-data="graph.religion.data" chart-labels="graph.religion.labels" chart-series="graph.religion.series" chart-options="graph.religion.options" ></canvas>
              </div>

              <div class="row" ng-if="check.data.religion">
                <canvas class="chart chart-pie" chart-data="graph.religion.data" chart-labels="graph.religion.labels" chart-series="graph.religion.series" chart-options="graph.religion.options" ></canvas>
              </div>

              <div class="row" ng-if="check.data.religion">
                <canvas class="chart chart-line" chart-data="graph.religion.data" chart-labels="graph.religion.labels" chart-series="graph.religion.series" chart-options="graph.religion.options" ></canvas>
              </div>

              <!-- end religion -->

              <!-- city -->

              <div class="row" ng-if="check.data.city">
                <canvas class="chart chart-bar" chart-title='graph.city.title' chart-data="graph.city.data" chart-labels="graph.city.labels" chart-series="graph.city.series" chart-options="graph.city.options" ></canvas>
              </div>

              <div class="row" ng-if="check.data.city">
                <canvas class="chart chart-pie" chart-data="graph.city.data" chart-labels="graph.city.labels" chart-series="graph.city.series" chart-options="graph.city.options" ></canvas>
              </div>

              <div class="row" ng-if="check.data.city">
                <canvas class="chart chart-line" chart-data="graph.city.data" chart-labels="graph.city.labels" chart-series="graph.city.series" chart-options="graph.city.options" ></canvas>
              </div>
              <!-- end city -->

              <!-- Living Arrangement -->
              <div class="row" ng-if="check.data.livingArrangement">
                <canvas class="chart chart-bar" chart-title='graph.livingArrangement.title' chart-data="graph.livingArrangement.data" chart-labels="graph.livingArrangement.labels" chart-series="graph.livingArrangement.series" chart-options="graph.livingArrangement.options" ></canvas>
              </div>

              <div class="row" ng-if="check.data.livingArrangement">
                <canvas class="chart chart-pie" chart-data="graph.livingArrangement.data" chart-labels="graph.livingArrangement.labels" chart-series="graph.livingArrangement.series" chart-options="graph.livingArrangement.options" ></canvas>
              </div>

              <div class="row" ng-if="check.data.livingArrangement">
                <canvas class="chart chart-line" chart-data="graph.livingArrangement.data" chart-labels="graph.livingArrangement.labels" chart-series="graph.livingArrangement.series" chart-options="graph.livingArrangement.options" ></canvas>
              </div>
              <!-- end living arrangement -->

              
              <!-- acad health problem -->
              <div class="row" ng-if="check.health.acad_health_prob">
                <canvas class="chart chart-bar" chart-title='graph.acad_health_prob.title' chart-data="graph.acad_health_prob.data" chart-labels="graph.acad_health_prob.labels" chart-series="graph.acad_health_prob.series" chart-options="graph.acad_health_prob.options" ></canvas>
              </div>

              <div class="row" ng-if="check.health.acad_health_prob">
                <canvas class="chart chart-pie" chart-data="graph.acad_health_prob.data" chart-labels="graph.acad_health_prob.labels" chart-series="graph.acad_health_prob.series" chart-options="graph.acad_health_prob.options" ></canvas>
              </div>

              <div class="row" ng-if="check.health.acad_health_prob">
                <canvas class="chart chart-line" chart-data="graph.acad_health_prob.data" chart-labels="graph.acad_health_prob.labels" chart-series="graph.acad_health_prob.series" chart-options="graph.acad_health_prob.options" ></canvas>
              </div>

              <!-- end acad health prob -->

              <!-- psychiatric_help -->
              <div class="row" ng-if="check.health.psychiatric_help">
                <canvas class="chart chart-bar" chart-title='graph.psychiatric_help.title' chart-data="graph.psychiatric_help.data" chart-labels="graph.psychiatric_help.labels" chart-series="graph.psychiatric_help.series" chart-options="graph.psychiatric_help.options" ></canvas>
              </div>

              <div class="row" ng-if="check.health.psychiatric_help">
                <canvas class="chart chart-pie" chart-data="graph.psychiatric_help.data" chart-labels="graph.psychiatric_help.labels" chart-series="graph.psychiatric_help.series" chart-options="graph.psychiatric_help.options" ></canvas>
              </div>

              <div class="row" ng-if="check.health.psychiatric_help">
                <canvas class="chart chart-line" chart-data="graph.psychiatric_help.data" chart-labels="graph.psychiatric_help.labels" chart-series="graph.psychiatric_help.series" chart-options="graph.psychiatric_help.options" ></canvas>
              </div>

              <!-- end psychiatric_help -->

              <!-- counseling -->
              <div class="row" ng-if="check.health.counseling">
                <canvas class="chart chart-bar" chart-title='graph.counseling.title' chart-data="graph.counseling.data" chart-labels="graph.counseling.labels" chart-series="graph.counseling.series" chart-options="graph.counseling.options" ></canvas>
              </div>

              <div class="row" ng-if="check.health.counseling">
                <canvas class="chart chart-pie" chart-data="graph.counseling.data" chart-labels="graph.counseling.labels" chart-series="graph.counseling.series" chart-options="graph.counseling.options" ></canvas>
              </div>

              <div class="row" ng-if="check.health.counseling">
                <canvas class="chart chart-line" chart-data="graph.counseling.data" chart-labels="graph.counseling.labels" chart-series="graph.counseling.series" chart-options="graph.counseling.options" ></canvas>
              </div>
              <!-- end counseling -->

              <!-- marital status -->
              <div class="row" ng-if="check.info.maritalStatus">
                <canvas class="chart chart-bar" chart-title='graph.maritalStatus.title' chart-data="graph.maritalStatus.data" chart-labels="graph.maritalStatus.labels" chart-series="graph.maritalStatus.series" chart-options="graph.maritalStatus.options" ></canvas>
              </div>

              <div class="row" ng-if="check.info.maritalStatus">
                <canvas class="chart chart-pie" chart-data="graph.maritalStatus.data" chart-labels="graph.maritalStatus.labels" chart-series="graph.maritalStatus.series" chart-options="graph.maritalStatus.options" ></canvas>
              </div>

              <div class="row" ng-if="check.info.maritalStatus">
                <canvas class="chart chart-line" chart-data="graph.maritalStatus.data" chart-labels="graph.maritalStatus.labels" chart-series="graph.maritalStatus.series" chart-options="graph.maritalStatus.options" ></canvas>
              </div>

              <!-- end marital status -->

              <!-- abroad status -->
              <div class="row" ng-if="check.info.abroad">
                <canvas class="chart chart-bar" chart-title='graph.abroad.title' chart-data="graph.abroad.data" chart-labels="graph.abroad.labels" chart-series="graph.abroad.series" chart-options="graph.abroad.options" ></canvas>
              </div>

              <div class="row" ng-if="check.info.abroad">
                <canvas class="chart chart-pie" chart-data="graph.abroad.data" chart-labels="graph.abroad.labels" chart-series="graph.abroad.series" chart-options="graph.abroad.options" ></canvas>
              </div>

              <div class="row" ng-if="check.info.abroad">
                <canvas class="chart chart-line" chart-data="graph.abroad.data" chart-labels="graph.abroad.labels" chart-series="graph.abroad.series" chart-options="graph.abroad.options" ></canvas>
              </div>

              <!-- end abroad status -->

               <!-- deceased status -->
              <div class="row" ng-if="check.info.deceased">
                <canvas class="chart chart-bar" chart-title='graph.deceased.title' chart-data="graph.deceased.data" chart-labels="graph.deceased.labels" chart-series="graph.deceased.series" chart-options="graph.deceased.options" ></canvas>
              </div>

              <div class="row" ng-if="check.info.deceased">
                <canvas class="chart chart-pie" chart-data="graph.deceased.data" chart-labels="graph.deceased.labels" chart-series="graph.deceased.series" chart-options="graph.deceased.options" ></canvas>
              </div>

              <div class="row" ng-if="check.info.deceased">
                <canvas class="chart chart-line" chart-data="graph.deceased.data" chart-labels="graph.deceased.labels" chart-series="graph.deceased.series" chart-options="graph.deceased.options" ></canvas>
              </div>

              <!-- end deceased status -->

              <!-- motivation status -->
              <div class="row" ng-if="check.info.motivation">
                <canvas class="chart chart-bar" chart-title='graph.motivation.title' chart-data="graph.motivation.data" chart-labels="graph.motivation.labels" chart-series="graph.motivation.series" chart-options="graph.motivation.options" ></canvas>
              </div>

              <div class="row" ng-if="check.info.motivation">
                <canvas class="chart chart-pie" chart-data="graph.motivation.data" chart-labels="graph.motivation.labels" chart-series="graph.motivation.series" chart-options="graph.motivation.options" ></canvas>
              </div>

              <div class="row" ng-if="check.info.motivation">
                <canvas class="chart chart-line" chart-data="graph.motivation.data" chart-labels="graph.motivation.labels" chart-series="graph.motivation.series" chart-options="graph.motivation.options" ></canvas>
              </div>

              <!-- end motivation status -->

              <!-- help status -->
              <div class="row" ng-if="check.info.help">
                <canvas class="chart chart-bar" chart-title='graph.help.title' chart-data="graph.help.data" chart-labels="graph.help.labels" chart-series="graph.help.series" chart-options="graph.help.options" ></canvas>
              </div>

              <div class="row" ng-if="check.info.help">
                <canvas class="chart chart-pie" chart-data="graph.help.data" chart-labels="graph.help.labels" chart-series="graph.help.series" chart-options="graph.help.options" ></canvas>
              </div>

              <div class="row" ng-if="check.info.help">
                <canvas class="chart chart-line" chart-data="graph.help.data" chart-labels="graph.help.labels" chart-series="graph.help.series" chart-options="graph.help.options" ></canvas>
              </div>

              <!-- end help status -->

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button ng-click="pdf()" class="btn btn-success">PDF</button></p>
          </div>
        </div>
      </div>
    </div>


</div>
@endsection

@section('controller')
<script src="<?= asset('app/lib/angular/chart.js/dist/Chart.min.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-chart.js/dist/angular-chart.min.js') ?>"></script>
<script src="<?=asset('app/lib/angular/pdfmake/build/pdfmake.min.js') ?>" ></script>
<script src="<?=asset('app/lib/angular/pdfmake/build/vfs_fonts.js') ?>" ></script>
<script src="<?= asset('app/controllers/report.js') ?>"></script>

@endsection      
