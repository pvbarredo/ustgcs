@extends('layouts.app')
@section('title')
<title>USTGCS - View Announcement</title>
@endsection

@section('app')
<div  ng-app="viewAnnouncementController" ng-controller="viewAnnouncementController">
@endsection

@section('content')
<div class="container">
  <br>
	<div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">

         {{ $announcement->title }}
   
        </h3>
      </div>
      <div class="panel-body">
   		  {{ $announcement->message }}      
           
      </div>
      <div class="panel-footer">

        <small><em>Posted by: {{ $created_by }}</em></small><br>
        <small><em><time title="{{ $announcement->created_at }}">Date Posted: {{ $announcement->created_at }}</time></em></small>

      </div>
    </div>
</div>
@endsection


@section('controller')
<script src="<?= asset('app/controllers/announcement.js') ?>"></script>
@endsection