<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('title')
    

    <!-- Styles -->
    <link href="<?= asset('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= asset('css/style.css') ?>" rel="stylesheet">
    <link href="<?= asset('css/sweetalert.css') ?>" rel="stylesheet">
    @yield('css')


</head>
<body >
   @yield('app')
   <div class="container-fluid" id="page-header">
    <div class="row" >
        <div class="col-md-3 move-right">
        <img src="<?= asset('img/gdw.png') ?>" id="pic-guidance" class="img-rounded center-block">
        </div>
        <div class="col-md-6 move-right" id="header-holder"><br>
          <h3 class="text-muted" id="ust-header1">University of Sto. Tomas</h3>
          <h3 class="text-muted" id="ust-header3">Counseling and Career Center</h3>
        </div>
        <div class="col-md-3 move-right">
        <img src="<?= asset('img/ustel.png') ?>" id="pic-uste" class="img-rounded center-block" >
        </div>
        <!-- <div class="col-md-2 move-right">
        @if (Auth::user())
          <h5 class="text-right user-holder"><strong><u>{{ Auth::user()->firstname . " " .  Auth::user()->lastname}}</u></strong></h5>
          <h6 class="text-right user-holder">{{ ucfirst(Auth::user()->status)    }}</h6>
        @endif
        </div> -->
      </div>
    <br/>
  </div>

    
  <div class="container-fluid" id ="menu-holder">
    @if (Auth::user())
    <div class="masthead">
        <nav>
          <ul class="nav nav-tabs nav-justified">
            @if (Auth::user()->status != 'professor' && Auth::user()->status != 'admin')
            <li {!! Request::is('announcement') ?'class= "active" ' : '' !!}><a href="{{ url('/announcement') }}">Announcements <span class="badge"> @{{ notification.announcement }} </span></a> </li>
            @endif
             @if (Auth::user()->status == 'professor' || Auth::user()->status == 'guidance')
            <li {!! Request::is('referral') ?'class= "active" ' : '' !!}><a href="{{ url('/referral') }}">Referral</a></li>
            @endif

            @if(Auth::user()->status == 'student')
            <li {!! Request::is('cumulative') ? 'class="active"' : '' !!}><a href="{{ url('/cumulative') }}">Cumulative Record</a></li>
           @elseif (Auth::user()->status == 'guidance' || Auth::user()->status == 'director')
           <li {!! Request::is('cumulativeCriteria') ? 'class="active"' : '' !!}><a href="{{ url('/cumulativeCriteria') }}">Cumulative Record</a></li>
           @endif

           

            @if (Auth::user()->status == 'student')
              <li {!! Request::is('inbox') ? 'class="active"' : '' !!}><a href="{{ url('/inbox') }}">Inbox <span class="badge"> @{{ notification.inbox }} </span></a> </li>
            @endif

            @if (Auth::user()->status == 'guidance')
              <li {!! Request::is('inbox') ? 'class="active"' : '' !!}><a href="{{ url('/inbox') }}">Inbox</a></li>
            @endif



             @if (Auth::user()->status == 'guidance' || Auth::user()->status == 'director')

            <li role="presentation" {!! Request::is('') ? 'class="dropdown active"' : '' !!}>
             <a id="reportLabel" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Reports<span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="reportLabel">
                <li><a href="{{ url('/nonCompliance') }}">Non Compliance</a></li>
                <li><a href="{{ url('/studentProfiling') }}">Student Profiling</a></li>
                <li><a href="{{ url('/commonProblem') }}">Common Problems</a></li>
                <li><a href="{{ url('/frequentlyReferred') }}">Frequently Referred</a></li>
              </ul>
            </li>
             @endif

             @if(Auth::user()->status === 'admin' || Auth::user()->status === 'guidance')
              <li {!! Request::is('student') ? 'class="active"' : '' !!}><a href="{{ url('/student') }}">Students</a></li>
             @endif

             @if (Auth::user()->status === 'student' )
            <li {!! Request::is('account/*') ? 'class="active"' : '' !!}><a href="{{ url('/account/1') }}">Account</a></li>
            @elseif (Auth::user()->status === 'professor' )
           
           
            <li role="presentation" {!! Request::is('account/*') ? 'class="dropdown active"' : '' !!}>
             <a id="accountLabel" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Account<span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="accountLabel">
                <li><a href="{{ url('/account/1') }}">Change Password</a></li>
                <li><a href="{{ url('/editAccount') }}">Edit Account</a></li>
              </ul>

            </li>


            @else
            <li role="presentation" {!! Request::is('account/*') ? 'class="dropdown active"' : '' !!}>
             <a id="accountLabel" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Account<span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="accountLabel">
                <li><a href="{{ url('/account/1') }}">Change Password</a></li>
                <li><a href="{{ url('/editAccount') }}">Edit Account</a></li>

                @if (Auth::user()->status === 'admin' )
                <li><a href="{{ url('/account/2') }}">Add Account</a></li>
                 <li><a href="{{ url('/college') }}">Colleges</a></li>
                <li><a href="{{ url('/course') }}">Courses</a></li>
                @endif
              </ul>

            </li>
            @endif
            <!-- @if (Auth::user()->status !== 'admin' )
            <li {!! Request::is('about') ? 'class="active"' : '' !!}><a href="{{ url('/calendar') }}">My Calendar</a></li>
            @endif -->
            
            @if (Auth::user()->status !== 'admin' )
            <li {!! Request::is('about') ? 'class="active"' : '' !!}><a href="{{ url('/about') }}">About Us</a></li>
            @endif
            <li {!! Request::is('logout') ? 'class="active"' : '' !!}><a href="{{ url('/logout') }}">Log Out</a></li>
          </ul>
        </nav>
   </div>
   <div class="row">
      <div class="col-md-3 col-md-offset-9" id="user-row">

        @if (Auth::user())
          <h5 class="text-right user-holder"><strong><u>{{ Auth::user()->firstname . " " .  Auth::user()->lastname}}</u></strong></h5>
          <h6 class="text-right user-holder">{{ ucfirst(Auth::user()->status)    }}</h6>
        @endif
        
      </div>
   </div>
   </div>
   @endif
<div class="container">
</div>
<br/>
    <br/>
    <br/>
    @yield('content')
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
      <br/>
    <br/>
    <br/>
    <div class="container-fluid" id="page-footer">
      <div class="row">
      <div class="col-md-12">
        <footer class="footer">
            <br/>
            <p class="whiten text-center">&copy; University of Santo Tomas 2016</p>
           <br/>     
        </footer>
        </div>
      </div>
    </div>
        <!-- Load Javascript Libraries (AngularJS, JQuery, Bootstrap) -->
        
    @yield('controllerFirst')

        <script src="<?= asset('js/jquery.min.js') ?>"></script>
        <script src="<?= asset('js/jquery-ui.min.js') ?>"></script>
        <script src="<?= asset('js/bootstrap.min.js') ?>"></script>
        <script src="<?= asset('app/lib/angular/moment.min.js') ?>"></script>
        <script src="<?= asset('app/lib/angular/angular/angular.min.js') ?>"></script>
        
        <script src="<?= asset('app/lib/angular/SweetAlert.js') ?>"></script>
        <script src="<?= asset('app/lib/angular/constants.js') ?>"></script>
        <script src="<?= asset('app/lib/angular/sweetalert.min.js') ?>"></script>
        <script src="<?= asset('app/lib/angular/lodash/lodash.js') ?>"></script>
        <script src="<?= asset('app/lib/angular/angular-moment.min.js') ?>"></script>
        
    @yield('controller')    
</body>
</html>
