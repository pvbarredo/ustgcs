@extends('layouts.app')
@section('title')
<title>USTGCS - Course</title>
@endsection

@section('css')
<link href="<?= asset('css/angucomplete-alt.css') ?>" rel="stylesheet">
@endsection

@section('app')
<div ng-app="cumulative" ng-controller="cumulativeCriteriaController">
@endsection

@section('content')

<div class="container">

    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                  <label for="refStud">Name of Student</label>
                  <div angucomplete-alt id="search_student" 
                      placeholder="Search student" 
                      maxlength="50" 
                      pause="100" 
                      remote-url= "webservice/getStudentUsingFirstname/"
                      title-field="id,firstname,lastname"
                      minlength="1" 
                      input-class="form-control form-control-small" 
                      match-class="highlight"
                      field-required="true"
                      selected-object="student"
                      focus-out="focusOut()">
                  </div>
                </div>
                <button type="button" class="btn btn-primary" ng-click="searchCriteriaButtonClicked(selectedStudent.description)" id="searchCriteria" disabled>Search Cumulative Record</button>
        </div>
    </div>    

</div>

@endsection

@section('controller')
<script src="<?= asset('app/controllers/cumulative.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angucomplete-alt.js') ?>"></script>
@endsection      
