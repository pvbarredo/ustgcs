@extends('layouts.app')
@section('title')
<title>USTGCS - Student Maintenance</title>
@endsection

@section('app')
<div  ng-app="student" ng-controller="studentController">
    @endsection

    @section('content')
    <div class="container">
        <div class="row">
            <br/>
            <br/>
            <h4>Students</h4>


            <hr/>
             @if (Auth::user()->status != 'admin')
            <p><button ng-click="pdf_clicked()" class="btn btn-success">PDF</button></p>
            @endif
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>College</th>
                        <th>Course</th>
                        <th>Year</th> 
                        <!-- <th><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Student</button></th> -->
                    </tr>
                </thead>
                <tbody>
                 <tr ng-repeat="student in students">
                    <td>@{{  student.code }}</td>
                    @if (Auth::user()->status != 'admin')
                    <td><a href ng-click="name_clicked(student)"> @{{ student.firstname + ' ' + student.lastname }}</a></td>
                    @else
                    <td> @{{ student.firstname + ' ' + student.lastname }}</td>
                    @endif

                    <td>@{{ student.college_name }}</td>
                    <td>@{{ student.course_name }}</td>
                    <td>@{{ student.year }}</td>

                    <td>
                            <!-- @if (Auth::user()->status !== 'admin' )
                            <button class="btn btn-default btn-xs btn-detail" ng-click="editClicked(student)">Edit</button>
                            @endif
                            <button class="btn btn-danger btn-xs btn-delete" ng-click="deleteClicked(student)">Delete</button> -->
                        </td>
                    </tr> 
                </tbody>
            </table>


        </div>

        
        <br/>

        <div class="modal fade" id="student_record_modal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Students Record</h4>
          </div>
          <div class="modal-body">
             <h3> @{{ selected_student.firstname + ' ' +  selected_student.lastname}}</h3>
             <div class="row">
             <div class="col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Referral Message</th>
                            <th>Referral Description</th>
                        </tr>
                    </thead>
                    <tbody>

                        

                        <tr ng-repeat="student_referral in student_referrals">
                            <td>@{{student_referral.message}}</td>
                            <td>@{{student_referral.description}}</td>
                     
                        </tr>
                    </tbody>
                </table>
            </div>   
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->   



</div>
@endsection


@section('controller')
<script src="<?=asset('app/lib/angular/pdfmake/build/pdfmake.min.js') ?>" ></script>
<script src="<?=asset('app/lib/angular/pdfmake/build/vfs_fonts.js') ?>" ></script>
<script src="<?= asset('app/controllers/student.js') ?>"></script>
@endsection