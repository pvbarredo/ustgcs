@extends('layouts.app')
@section('title')
<title>USTGCS - Colleges</title>
@endsection

@section('app')
<div ng-app="colleges" ng-controller="collegesController">
@endsection

@section('content')

  <div class="container">
        <div class="row">
        <br/>
        <br/>
        <h4>Colleges</h4>
        

        <hr/>
         <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                   
                        <th><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New College</button></th>
                    </tr>
                </thead>
                <tbody>
                   <tr ng-repeat="college in colleges">
                        <td>@{{  college.id }}</td>
                        <td>@{{ college.name }}</td>
                        <td>
                            <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', college.id)">Edit</button>
                            <button class="btn btn-danger btn-xs btn-delete" ng-click="confirmDelete(college.id)">Delete</button>
                        </td>
                    </tr> 
                </tbody>
            </table>


            </div>

      <div class="container">
       <div class="modal fade" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">@{{form_title}}</h4>
            </div>
            <form name="collegeForm"a>
            <div class="row">
            	<div class="col-md-12">
              
                    <div class="form-group">
                        <div class = "col-md-5">
                            <label for="collegeName">Name</label>
                            <input type="text" class="form-control" name="collegeName" placeholder="CollegeName" ng-model="collegiate.name" required>
                        </div>
                        
                
                          </div>
                 </div>
             </div>
            
            <br/>
            <div class="row">
            	<div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-5">
                        <button class="btn btn-default" type="button" ng-click="save(modalState, id)">Submit</button>

                    </div>
                </div>
               	</div>
            </div>
            <br/>
            
            
            </form>
           </div>
          </div>
         </div>
       </div>
             
     </div>
            <br/>
          
         
            
                

</div>
@endsection

@section('controller')
<script src="<?= asset('app/controllers/colleges.js') ?>"></script>

@endsection      
