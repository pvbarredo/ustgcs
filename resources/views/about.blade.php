@extends('layouts.app')
@section('title')
<title>USTGCS - About</title>
@endsection

@section('app')
<div ng-app="about" ng-controller="aboutController">
  @endsection

  @section('content')
  <div class="container">
   <div class="row">
    <div class="row">
      <div class="col-md-12 text-center">
        <br><br><br><h1 class="section-title">HISTORY</h1>
        <br/>
        <hr>
      </div> <!-- /.col-md-12 -->
    </div>
    <div class="row">
     <div id="timeline">
		   <!--    <ul id="dates">
				<li><a href="#welcome">Beginning 1</a></li>
				<li><a href="#welcome2">Beginning 2</a></li>
				<li><a href="#1969">1969</a></li>
				<li><a href="#1982">1982</a></li>
				<li><a href="#2001">1994</a></li>
				<li><a href="#2007">2002</a></li>
				<li><a href="#PT">2008</a></li>
	            <li><a href="#PT">2008</a></li>
	            <li><a href="#2011">2011</a></li>
	            <li><a href="#2013">2013</a></li>
            </ul> -->
            <ul id="issues">
             <div class="col-md-12">
              
              
              <h1>The Beginning</h1>
              <p>The Guidance and Counseling Department of the University of Santo Tomas was established in the late 1950's congruent to the objectives of Catholic Education, . Specifically, it aimed to develop the intellectual potentials, physical and personality traits and skills of the students and to inculcate values and norms in them.</p>
              
              
              <p>It began in small scale, pioneered by Dr. Rabago, Dr. Samson, Dr. Estrada and Fr. Del Rio O.P. as spiritual director, who entertainerd referred cases sent to the Student's Clinic for guidance by the faculty members.

                The Guidance and Counseling Office was and is still located at the Health Service Building.</p>
                
                
                <h1>1969</h1>
                <p>A program expansion and decentralization of guidance service took place. The office staff increased from two (2) to ten (10) full time counselors and one (1) psychologist, Fr. Dionisio G. Cabezon, O.P. who alse acted as the head of the Department.Consequently, Guidance Offices were opened in different colleges which enabled the counselors to bring the services closer to the students. In the process, it created a more dynamic relationship between counselors and students.
                </p>
                
                <h1>1982</h1>
                <p>Fr. Norberto Castillo, O.P., who was the Rector then, created another unit of the Guidance and Counseling department - the University Testing Center. It came as a welcome challenge to the department and was under the direct supervision of Fr. Cabezon, O.P. The center takes charge of the university wide examination and the evaluation of student applicants. Fr. Cabezon, O.P. supervised the various activities of the university testing center and the guidance units.</p>
                

                
                <h1>1994 - 2001</h1>
                <p>1994 - 2001
                  There was an increase in the number of counselors from eleven (11) to twenty (20)counselors and new offices were likewise created for the Faculty of Medicine and Surgery and the College of Rehabilitation Sciences. At this time, additional one or two counselors were assigned for each college/ faculty or department. In October 1997, Freshmen Admission System (FAS) was created through the assistance of the University Computer Center which helped in processing results of the entrance examination</p>


                  
                  <h1>2002 - 2007</h1>
                  <p>
                    There was a significant increase in the number of counselors from twenty (20) to thirty (30) counselors due to additional new guidance offices created for the different colleges/ facultys. Within this period, the new offices were for the College of Fine Arts and Design, Faculty of Civil Law, AMV- College of Accountancy and College of Tourism and Hospitality Management.</p>
                    
                    
                    
                    <h1>2008</h1>
                    <p>
                      On June 01, 2008, Prof. Lucila O. Bance, Ph.D. officially succeeded Fr. Dionisio Cabezon, O.P. as the Director of the Guidance and Counseling Department (GCD).  Under the leadership of Dr. Bance, the department aims for more visibility  through its programs and services within and outside the university and to continuously work collaboratively with the administrators, faculty members and other school professionals, parents and significant others in the challenge of preparing all students </p>
                      
                      

                      
                      <h1>2008</h1>
                      <p>
                       to meet their needs and the expectations of higher academic standards. Indeed, the Guidance and Counseling Department remained steadfast in its commitment to serve the Thomasian Community and the global community both through its programs and services geared towards holistic development.</p>
                       
                       
                       
                       
                       <h1>2011</h1>
                       <p> With the professionalization of Guidance and Counseling in the country, Dr. Bance, expedited the licensure of all guidance counsellors through its department�s policies. In 2011, the department came to be known as the �Home of Board Topnotchers� with its counsellors garnering the top places in the Professional Regulation Commission (PRC) board examinations. 
                       </p>
                       
                       
                       
                       
                       <h1>2013</h1>
                       <p> In 2013, the department has 43 Guidance Counselors with 2 Psychometricians serving the university, the biggest number of counselors in the country.


                        Indeed, the Guidance and Counseling Department through its professional, committed, compassionate counselors remained steadfast in serving the Thomasian and the global community both through its programs and services geared towards holistic development.  

                      </p>
                      
                      
                      

                    </ul>
                    
                  </div>
                </div>
                <br/>
                <br/>
 <!--    <div class ="row">
    	<div class="col-md-12">
    		<h1 class="section-title">MISSION AND VISION</h1>
    		<hr/>
    		<div>
    			<img src="<?= asset('img/about/12.jpg') ?>" class="img-slide" />
    		</div>
    	</div>

    </div>
    <br/>
    <br/>
    <div class ="row">
    	<div class="col-md-12">
    		<h1 class="section-title">CCC CORE FUNCTIONS</h1>
    		<hr/>
    		<div>
    			<img src="<?= asset('img/about/13.jpg') ?>" class="img-slide" />
    		</div>
    		<div>
    			<img src="<?= asset('img/about/14.jpg') ?>" class="img-slide" />
    		</div>
    	</div>

    </div> -->
    <br/>
    <br/>
    <div class="row">
      <div class="col-md-12 text-center">
        <h1 class="section-title">OUR TEAM</h1>
        <hr/>
      </div> <!-- /.col-md-12 -->
    </div>

    <div class="row">
     <div class="col-md-12">
       <h1>Our Team<br class="cleaner h20" /></h1>
       <p>The Counseling and Career Center is headed by a director and is supported by the counselors, psychometricians and office staff.</p>

       <p>The director's council serves as the advisory staff of the director on matters pertaining to the administration of the Center. The council is composed of three counselors.</p>

       <p>The Center deploys a total of 43 guidance counselors to the different faculties/colleges/schools of the University. Three (3) of them are part of the director's council. The ratio of the college/faculty counselor to the students is 1:1,200 while high school counselor has a ratio of 1:500.</p>

       <p>Two psychometricians assist the Director in all research projects for improving the programs developed by the Center. </p>

       <p>The Center has only one office staff who performs clerical and office management functions.</p>
       <br><br>

       <h1>COUNSELING AND CAREER CENTER</h1>
       <hr/>
     </div>

   </div>


   <div class = "row">
     <div class="col-md-12">
      <ul id="gallery">

        <div class ="row">
          <div class="col-md-6">
            <img src="<?= asset('img/about/g1.jpg') ?>" class="img-portrait" /> <br/>
            <h5>BANCE, LUCILA O. (Guidance Director)<br />COUNSELING & CAREER CENTER<br /><h5>
            </div>
            <div class="col-md-6">
              <img src="<?= asset('img/about/g2.jpg') ?>" class="img-portrait" /><h5>   CONTRERAS, VICKY H. (Office Staff)<br />COUNSELING & CAREER CENTER <br/></h5>
            </div>

          </div>

          <div class="row">
            <div class="col-md-6">
             <img src="<?= asset('img/about/g3.jpg') ?>" class="img-portrait" /><h5>GURANGO, MYRA M. (Psychometrician)	</br>COUNSELING & CAREER CENTER<br /></h5>
           </div>
           <div class="col-md-6">
             <img src="<?= asset('img/about/g4.jpg') ?>" class="img-portrait" /><h5>LANUZA, JUHNELYNN C. (Psychometrician)	</br>COUNSELING & CAREER CENTER<br /></h5>
           </div>
         </div>
       </ul>
     </div>

     <div class="col-md-12">
       <p>The UST Guidance and Counseling Department has 16 Satellite offices located throughout the University's grounds. Localized guidance services are delivered by licensed counselors assigned per faculty/college. Click your respective college/faculty to know more details about your counselor.</p>
     </div>

   </div>

   <div class="row">
     <div class="col-md-12">
      <h1>
        AMV College of Accountancy
      </h1>
      <hr/>
      <h4>
       Office number: 406-1611 local 8542
       Office email: ustguidancecenter@yahoo.com
     </h4>       

     <div class="row">
      <div class="col-md-6">
        <img src="<?= asset('img/about/ACCT - Delos Reyes, Aiza_Toga.jpg') ?>" class="img-portrait" />
        <br/>
        DE LOS REYES, AIZA A., MA, RGC
      </div>
      
      
      <div class="col-md-6">
        <img src="<?= asset('img/about/ENG - Rosales, Naomi_Toga.jpg') ?>" class="img-portrait" />
        <br/>
        ROSALES, NAOMI N., MA, RGC
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
       <img src="<?= asset('img/about/ACCT - Odfina, Katrina_Toga.jpg') ?>" class="img-portrait center-block" /> <br/>
       <h5 class="text-center"> ODFINA, KATRINA REBECCA A., MA, RGC</h5>
     </div>

   </div>
   
 </div>

</div>


<div class ="row">
 <div class="col-md-12">
   <h1>College of Architecture</h1>
   <hr/>
   <h4>Office number: 406-1611 local 8407 <br> Office email: ustguidancecenter@yahoo.com</h4>

   <div class="row">
    <div class="col-md-6">
      <img src="<?= asset('img/about/ARCH - Calces, Angeli_Toga.jpg') ?>" class="img-portrait"  /><br/>
      ASPI-CALCES, ANGELI CHRISTINE C., MA, RGC
    </div>
    <div class="col-md-6">
      <img src="<?= asset('img/about/ACCT - Gallardo, Thess_Toga.jpg') ?>" class="img-portrait"  /><br/>GALLARDO, MARIA THERESA B., MA, RGC
    </div>

  </div>
  <div class="row">
    <div class="col-md-12">
      <img src="<?= asset('img/about/ARCH - Recio, Lalaine_Toga.jpg') ?>" class="img-portrait center-block"  />
      <h5 class="text-center"> RECIO, MARIE LALAINE L., MA, RGC </h5>
    </div>
  </div>
  
  
</div>
</div>

<div class ="row">
 <div class="col-md-12">
   <h1>Faculty of Arts and Letters</h1>
   <h4>Office number: 406-1611 local 8399
    Office email: ustabguidance@gmail.com</h4>
    
    
    <div class="row">
     <div class="col-md-6">

       <img src="<?= asset('img/about/AB - Atillo, Analene_Toga.jpg') ?>" class="img-portrait" /> <br/>BALA-ATILLO, ANALENE N., MA, RGC
     </div>
     <div class="col-md-6">
       <img src="<?= asset('img/about/ENG - Dulawan, Adesty_Toga.jpg') ?>" class="img-portrait" /><br/>
       DULAWAN, ADESTY P., MA, RGC	
     </div>
   </div>
   <div class="row">
     <div class="col-md-6">
       <img src="<?= asset('img/about/PHAR - Gadiana, Leny_Toga.jpg') ?>" class="img-portrait" /><br/>GADIANA, LENY G., RGC	
     </div>
     <div class="col-md-6">
       <img src="<?= asset('img/about/AB - Quita, Tin_Toga.jpg') ?>" class="img-portrait" /><br/>QUITA, CHRISTINE C., MA, RGC
     </div>
   </div>
   
   

 </div>

</div>


<div class="row">
 <div class="col-md-12">

  <h1>Faculty of Civil Law</h1>
  <h4>Office email: ustguidancecenter@yahoo.com</h4>
  <div class="row">
    <div class="col-md-6">
     <img src="<?= asset('img/about/PHAR - Gadiana, Leny_Toga.jpg') ?>" class="img-portrait" /><br/>GADIANA, LENY G., RGC
   </div>
 </div>
</div>

</div>

<div class="row">
 <div class="col-md-12">
  <h1>College of Commerce and Business Administration
  </h1>
  <h4>Office number: 406-1611 local 8393
    Office email: ustguidancecenter@yahoo.com</h4>

    <div class="row">
      <div class="col-md-6">
        <img src="<?= asset('img/about/COM - Alfonso, Nel_Toga.jpg') ?>" class="img-portrait" /><br/>
        ALFONSO, MARY NEL B.
      </div>
      <div class="col-md-6">
        <img src="<?= asset('img/about/COM - Bruno, Jane_Toga.jpg') ?>" class="img-portrait" /><br/>
        BRUNO, THERESA JANE P., MA, MAEd, RGC
      </div>
    </div>
    <div class="col-md-12">
      <img src="<?= asset('img/about/COM - De Leon, Monica_Toga.jpg') ?>" class="img-portrait center-block" />
      <h5 class="text-center">DE LEON, MONICA SOPHIA L., MA, RPsych</h5>
    </div>

  </div>

</div>

<div class="row">
 <div class="col-md-12">

   <h1>College of Education & Conservatory of Music</h1>
   <h4>Office number: 406-1611 local 8406
    Office email: ustguidancecenter@yahoo.com</h4>
    <div class="row">
      <div class="col-md-6">
        <img src="<?= asset('img/about/AB - Cruz, Danielle_Toga.jpg') ?>" class="img-portrait" /><br/>CRUZ, DANIELLE MARIE I., MA, RGC
      </div>
      <div class="col-md-6">
        
        <img src="<?= asset('img/about/EDUC - Triguero, Janice_Toga.jpg') ?>" class="img-portrait" /><br/>
        
        OCRAY-TRIGUERO , JANICE P., MA, RGC	
      </div>
    </div>
    
  </div>

</div>


<div class="row">
 <div class="col-md-12">
  <h1>Education High School </h1>
  <h4>Office number: 406-1611 local 8405
    Office email: carolangelineperez@gmail.com</h4>
    <div class="row">
      <div class="col-md-6">
        <img src="<?= asset('img/about/EHS - Perez, Carol_Toga.jpg') ?>" class="img-portrait" /><br/>MACAWILE-PEREZ, CAROL ANGELINE R., MA, RGC	All Levels
      </div>
    </div>

    

  </div>

</div>


<div class="row">
 <div class="col-md-12">
  <h1>Faculty of Engineering</h1>
  <h4>Office number: 406-1611 local 8408
    Office email: ustengguidance@gmail.com </h4>

    <div class="row">
     <div class="col-md-6">
       <img src="<?= asset('img/about/ENG - Baura, Kristin_Toga.jpg') ?>" class="img-portrait" /><br/>
       BAURA, KRISTIN B.
     </div>
     <div class="col-md-6">
      <img src="<?= asset('img/about/COM - Chua, Claud_Toga.jpg') ?>" class="img-portrait" /><br/>
      CHUA, CLAUDINE S., MA, RGC
    </div>
  </div>
  <div class="row">
   <div class="col-md-6">
    <img src="<?= asset('img/about/ENG - Lim, Khristine_Toga.jpg') ?>" class="img-portrait" /><br/>
    LIM, KHRISTINE LORRAINE C., MA, RGC
  </div>
  <div class="col-md-6">
   <img src="<?= asset('img/about/EDUC - Navarra, Diovie_Toga.jpg') ?>" class="img-portrait" /><br/> NAVARRA, DIOVIE C., MA, RGC
 </div>
</div>
<div class="col-md-6">
  <img src="<?= asset('img/about/ENG - Nicasio, Marissa_Toga.jpg') ?>" class="img-portrait" /><br/>NICASIO, MARISSA S., MA, RGC
</div>
<img src="<?= asset('img/about/USTHS - Portera, Via_Toga.jpg') ?>" class="img-portrait" /><br/>PORTERA, VIA KATRINA G., MA, RGC
</div>

</div>

</div>

<div class="row">
 <div class="col-md-12">
   <h1>College of Fine Arts and Design<br>
   </h1>
   <h4>Office number: 406-1611 local 8456 Office email: ustguidancecenter@yahoo.com</h4>
   <div class="row">
    <div class="col-md-6">

      <img src="<?= asset('img/about/CFAD - Bonifacio, Agnes_Toga.jpg') ?>" class="img-portrait" /><br/>BONIFACIO, MARIA AGNES B., MA, RGC

    </div>
    <div class="col-md-6">
      <img src="<?= asset('img/about/CFAD - Domingo, Princess_Toga.jpg') ?>" class="img-portrait"/><br/>DOMINGO, MARIA DOLORES T., MA, RGC
    </div>
  </div>

  

</div>

</div>


<div class="row">
 <div class="col-md-12">
   <h1>Institute of Physical Education and Athletics<br class="cleaner h20" /></h1>
   <h4>Office email: ipeaguidance@yahoo.com</h4>
   <div class="row">
    <div class="col-md-6">
     <img src="<?= asset('img/about/ENG - Cabaron, Lyn_Toga.jpg') ?>" class="img-portrait" /><br/>
     CABARON, LYNMARIE THERESE A., MA, RGC
   </div>
 </div>


</div>

</div>




<div class="row">
 <div class="col-md-12">

  <h1>Faculty of Medicine and Surgery</h1>
  <h4>406-1611 local 8403 Office email: ustguidancecenter@yahoo.com </h4>
  <div class="row">
    <div class="col-md-6">
      <img src="<?= asset('img/about/MED - Atinaja, Gina_Toga.jpg') ?>" class="img-portrait" /><br/>ATINAJA, MA. REGINA D., Ph.D., RGC, RPsych
    </div>
  </div>

</div>

</div>


<div class="row">
 <div class="col-md-12">
   <h1>College of Nursing</h1>
   <h4>Office number: 406-1611 local 8404

    Office email: ustguidance_nursing@gmail.com</h4>

    <div class="row">
     <div class="col-md-6">
      <img src="<?= asset('img/about/NUR - Cervantes, Nenita_Toga.jpg') ?>" class="img-portrait" /><br/>CERVANTES, NENITA B., RGC
    </div>
    <div class="col-md-6">
      <img src="<?= asset('img/about/NUR - Garinganao, Ten_Toga.jpg') ?>" class="img-portrait" /><br/>GARINGANAO, CHRISTINE P., MA, RGC
    </div>
  </div>


</div>

</div>


<div class="row">
 <div class="col-md-12">
   <h1>Faculty of Pharmacy</h1>
   <h4>Office number: 406-1611 local 8400 Office email: ustguidancecenter@yahoo.com </h4>
   <div class="row">
    <div class="col-md-6">
     <img src="<?= asset('img/about/ENG - Cabaron, Lyn_Toga.jpg') ?>" class="img-portrait" /><br/>
     CABARON, LYNMARIE THERESE A., MA, RGC
   </div>
   <div class="col-md-6">
     <img src="<?= asset('img/about/PHAR - Lu, StepH_Toga.jpg') ?>" class="img-portrait" /><br/>LU, STEPHANIE ANNE L., MA, RGC
   </div>
 </div>
 <div class="row">
  <div class="col-md-12">
   <img src="<?= asset('img/about/PHAR - Quesada, Carmen_Toga.jpg') ?>" class="img-portrait center-block" />
   <h5 class="text-center">QUESADA, CARMEN S., MA, RGC</h5>
 </div>
</div>
</div>

</div>


<div class="row">
 <div class="col-md-12">
  <h1>College of Rehabilitation Science<br></h1>
  <h4>
    Office number: 406-1611 local 8564
    Office email: kcadano@yahoo.com
  </h4>
  <div class="row">
    <div class="col-md-6">
     <img src="<?= asset('img/about/RS - Cadano, Kris_Toga.jpg') ?>" class="img-portrait" /><br/>CADANO, KRISTINE C., MA, RGC
   </div>
 </div>

</div>

</div>



<div class="row">
 <div class="col-md-12">
   <h1>College of Science<br></h1>
   <h4>Office number: 406-1611 local 8401 <br>Office email: ustguidancecenter@yahoo.com</h4>
   <div class="row">
    <div class="col-md-6">
      <img src="<?= asset('img/about/SCI - Cornes, Michelle_Toga.jpg') ?>" class="img-portrait" /><br/>CORNES, MICHELLE ROSE Y., MA, RGC
    </div>
    <div class="col-md-6">
      <img src="<?= asset('img/about/SCI - Macaraan, MaryRose_Toga.jpg') ?>" class="img-portrait" /><br/>MACARAAN, MARYROSE C., MA, RGC
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <img src="<?= asset('img/about/SCI - Pineda, Neriza_Toga.jpg') ?>" class="img-portrait center-block" /><h5 class="text-center">PINEDA, NERIZA G., MA, RGC  </h5>
    </div>
  </div>

</div>

</div>




<div class="row">
 <div class="col-md-12">
   <h1 align="center">College of Tourism and Hospitality Management<h1>

     <h4>Office number: 406-1611 local 8409
       Office email: bautista.angelie@yahoo.com

     </h4>
     
     
   </h1>
   <div class="row">
    <div class="col-md-6">
      <img src="<?= asset('img/about/CTHM - Bautista, Angelie_Toga.jpg') ?>" class="img-portrait" /><br/>BAUTISTA, ANGELIE D., MA, RGC
    </div>
    <div class="col-md-6">
      <img src="<?= asset('img/about/CTHM - Cleofe, Myreen_Toga.jpg') ?>" class="img-portrait" /><br/>CLEOFE, MYREEN P., MA, MAEd, RGC, RPsych
    </div>

  </div>

</div>

<div class="row">
 <div class="col-md-12">
   
   <h1 align="center">UST High School</h1>
   <h4>Office number: 406-1611 local 8402  
    Office email: enriquezdenise@yahoo.com
    Office email: nerizza.valdellon@yahoo.com
  </h4>
  <div class="row">
    <div class="col-md-6">
      <img src="<?= asset('img/about/USTHS - Enriquez, Denise_Toga.jpg') ?>" class="img-portrait" /><br/>ENRIQUEZ, DENISE CYDEE B., MS,MAEd,RGC	Grade 7 	
    </div>
    <div class="col-md-6">
     <img src="<?= asset('img/about/USTHS - Mabasa, Carla_Toga.jpg') ?>" class="img-portrait" /><br/>MABASA, MA. CARLA R., MA, RGC
   </div>
 </div>
 <div class="row">
   <div class="col-md-12">
     <img src="<?= asset('img/about/USTHS - Valdellon, Nerizza_Toga.jpg') ?>" class="img-portrait center-block" />
     <h5 class="text-center">VALDELLON, NERIZZA A., MA, RGC</h5>
   </div>
 </div>
 

</div>

</div>






</div>
</div>
@endsection

@section('controller')
<script src="<?= asset('app/controllers/about.js') ?>"></script>

@endsection  