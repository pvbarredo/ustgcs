@extends('layouts.app')
@section('title')
<title>USTGCS - Calendar</title>
@endsection

@section('css')
<link href="<?= asset('app/lib/angular/fullcalendar/dist/fullcalendar.css') ?>" rel="stylesheet">
@endsection

@section('app')
<div  ng-app="mycalendar" ng-controller="mycalendarController">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <br/>
        <br/>
        <h4>My Calendar</h4>
        

        <hr/>

    </div>
   <div class="btn-toolbar">
    <div class="btn-group">
        <button class="btn btn-success" ng-click="changeView('agendaDay', 'myCalendar')">Day</button>
        <button class="btn btn-success" ng-click="changeView('agendaWeek', 'myCalendar')">Week</button>
        <button class="btn btn-success" ng-click="changeView('month', 'myCalendar')">Month</button>
    </div>
</div>
<!-- <div class="calendar" ng-model="eventSources" calendar="myCalendar" ui-calendar="uiConfig.calendar"></div> -->


<div ui-calendar="uiConfig.calendar" class="calendar" calendar="myCalendar" class="span8 calendar" ng-model="eventSources"></div>

<div class="alert-success calAlert" ng-show="alertMessage != undefined && alertMessage != ''">
    <h4>@{{alertMessage}}</h4>
</div>

<ul class="unstyled">
    <li ng-repeat="e in events | filter:currentDate">
        <div class="alert alert-info">
            <a class="close" ng-click="remove($index)"><i class="icon-remove"></i></a>
            <b> @{{e.title}}</b> - @{{e.start | date:"MMM dd"}}
        </div>
    </li>
</ul>
  
<br/>
            
                

</div>
@endsection

@section('controllerFirst')


@endsection
@section('controller')
<script src="<?= asset('app/controllers/mycalendar.js') ?>"></script>
<script type="text/javascript" src="<?= asset('app/lib/angular/angular-ui-calendar/src/calendar.js') ?>"></script>
<script type="text/javascript" src="<?= asset('app/lib/angular/fullcalendar/dist/fullcalendar.min.js') ?>"></script>
<script type="text/javascript" src="<?= asset('app/lib/angular/fullcalendar/dist/gcal.js') ?>"></script>

@endsection
