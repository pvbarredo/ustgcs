@extends('layouts.app')
@section('title')
<title>USTGCS - Cumulative</title>
@endsection

@section('css')
<link href="<?= asset('css/angucomplete-alt.css') ?>" rel="stylesheet">
@endsection

@section('app')
<div ng-app="cumulative" ng-controller="cumulativeController">
@endsection

@section('content')

<div class="container" >


    @if(Auth::user()->status == 'student')
   <!--  <button type="button" class="btn btn-primary" ng-click="populate()">GENERATE</button>
    <button type="button" class="btn btn-primary" ng-click="save(cumulative.cum_id)">Submit</button> -->
    @endif

    <div class="readonly">
        <div class="row">
        <br/>
        <br/>

        <h4>I. Personal Information</h4>
        
        <hr/>
           
            <!-- <div class="row">
              <div class="col-md-2">
                        <div class="form-group">

                                @if(Auth::user()->status == 'student')
                                <a href ="" ng-click="dpClicked()">
                                @endif

                                @if (File::exists(public_path('img/' . $user->code . '.JPG') ))
                                    <img src="<?= asset('img/' . $user->code . '.JPG') ?>" id="cumuPhoto" class="img-rounded center-block" width="150" height="150" alt="Responsive image" >
                                @else
                                    <img src="<?= asset('img/Human.png') ?>" id="cumuPhoto" class="img-rounded center-block" alt="Responsive image" width="150" height="150" >
                                @endif

                                @if(Auth::user()->status == 'student')
                                </a> 
                                @endif  
                         
                            
                        </div>
                    </div> 
            </div>   -->


            <div class="modal fade" id="uploadPictureModal">
              <div class="modal-dialog">
                <div class="modal-content">
                   <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <!-- <h4 class="modal-title">PICTURE</h4> -->
                   </div> 
                   <div class="modal-body">
                         @if (File::exists(public_path('img/' . $user->code . '.JPG') ))
                            <img src="<?= asset('img/' . $user->code . '.JPG') ?>"  id="cumuPhoto" class="img-rounded" alt="Responsive image"  width="550" height="550">
                        @else
                            <img src="<?= asset('img/Human.png') ?>" id="cumuPhoto" class="img-rounded" alt="Responsive image"  width="550" height="550" >
                        @endif

                         
                       
                  </div>
                  <div class="modal-header">
                    
                    <h4 class="modal-title">Upload New Picture</h4>
                  </div>

                  <div class="modal-body">
                    <form action="{{ url('webservice/uploadPicture') }}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <input type="file" class = "btn btn-default" name="dp" value="" />

                       
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     <input type="submit" class="btn btn-primary" name="submit" value="Upload" />
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

 
            <div class="row">
           
                

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group phot-adjust">
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                            <label for="firstName">First Name:</label>
                            <input type="text" class="form-control" id="firstName"   ng-model="cumulative.firstname" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <div class="form-group phot-adjust">
                            <label for="middleName">Middle Name:</label>
                            <input type="text" class="form-control" id="middleName"   ng-model="cumulative.middlename" readonly>
                        </div>
                    </div>
                    <div class="col-md-3">
                    <br/>
                        <br/>
                        <br/>
                        <br/>
                         <div class="form-group phot-adjust">
                            <label for="lastName">Last Name:</label>
                            <input type="text" class="form-control" id="lastName"   ng-model="cumulative.lastname" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                     <br/>
                        <br/>
                        <br/>
                        <br/>
                         <div class="form-group phot-adjust">
                            <label for="auxName">Auxiliary Name:</label>
                            <input type="text" class="form-control" id="auxName"   ng-model="cumulative.auxiliary_name">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">

                                @if(Auth::user()->status == 'student')
                                <a href ="" ng-click="dpClicked()">
                                @endif

                                @if (File::exists(public_path('img/' . $user->code . '.JPG') ))
                                    <img src="<?= asset('img/' . $user->code . '.JPG') ?>" id="cumuPhoto" class="img-rounded center-block" width="150" height="150" alt="Responsive image" >
                                @else
                                    <img src="<?= asset('img/Human.png') ?>" id="cumuPhoto" class="img-rounded center-block" alt="Responsive image" width="150" height="150" >
                                @endif

                                @if(Auth::user()->status == 'student')
                                </a> 
                                @endif  
                         
                            
                        </div>
                    </div> 
                   
                 </div>   
        
         
            <br/>


            <div class="row">
           
                <div class="col-md-2">
                     <div class="form-group">
                        <label for="nickName">Nickname:</label>
                        <input type="text" class="form-control" id="nickName"   ng-model="cumulative.nickname">
                    </div>
                </div>
                <div class="col-md-2">
                    
                    <label for="radGender">
                    Gender:
                    </label>
                     <div class="form-group">
                        <label class="radio-inline">
                            <input type="text" name="Gender" class="form-control"  ng-model="cumulative.gender"  readonly> 
                        </label>
                    </div>
                </div>
                    <div class="col-md-5">
                        <div class="row">
                            <div class = "form-group">
                              <label for ="month">Date of Birth:</label>
                                
                                <div class="form-inline">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                               <input type="text" name="Month" class="form-control" ng-model="cumulative.birthMonth"  readonly> 
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" name="Day" class="form-control" ng-model="cumulative.birthDay" readonly >
                                        </div>
                                    </div>  
                                
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" name="Year" class="form-control" ng-model="cumulative.birthYear" readonly >
                                        </div>
                                    </div> 
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="age">Age:</label>
                        <input type="text" class="form-control" id="age" ng-model="cumulative.age" readonly>
                    </div>
                </div>
             
                   
                
            </div>
            <br/>
            <div class="row">
                
                <div class="col-md-4">
                     <div class="form-group">
                        <label for="nationality">Nationality:</label>
                        <input type="text" class="form-control" id="nationality"   ng-model="cumulative.nationality">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="religion">Religion:</label>
                        <input type="text" class="form-control" id="religion"   ng-model="cumulative.religion">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="email">Email Address:</label>
                        <input type="text" class="form-control" id="email"   ng-model="cumulative.email">
                    </div>
                </div>
              
            </div>
            <br/>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="currAdd">Current Address:</label>
                        <input type="text" class="form-control" id="currAdd "   ng-model="cumulative.curr_address">
                    </div>
                </div>
                <div class="col-md-4">
                     <div class="form-group">
                        <label for="currCity">City:</label>
                        <input type="text" class="form-control" id="currCity"   ng-model="cumulative.curr_city" >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="mobNum">Mobile Number:</label>
                        <input type="text" class="form-control" id="mobNum" placeholder="Mobile #"  ng-model="cumulative.mobile_no">
                    </div>
                </div>
                  
                    
                   
            </div>
            <br/>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="permAdd">Permanent Address:</label>
                        <input type="text" class="form-control" id="permAdd"   ng-model="cumulative.perm_address">
                    </div>
                </div>
                <div class="col-md-4">
                     <div class="form-group">
                        <label for="permCity">City:</label>
                        <input type="text" class="form-control" id="permCity"   ng-model="cumulative.perm_city">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="homeNum">Home Phone Number:</label>
                        <input type="text" class="form-control" id="homeNum" placeholder="Home #"  ng-model="cumulative.home_phone_no">
                    </div>
                </div>
                  
                   
                   
            </div>
            <br/>
             <div class="row">
               <div class="col-md-12">
                    
             
                    <label for="radLiving">
                    Current Living Arrangement:
                    </label>
                    <div class="form-group">
                        <label class = "radio-inline">
                        <input type="radio" name="radLiving" id="radFam" ng-model="cumulative.living_arrangement" value="Living with Immediate Family" checked>Living with Immediate Family
                        </label>
                    </div>
                     <div class="form-group">
                        <label class = "radio-inline">
                        <input type="radio" name="radLiving" id="radRel" ng-model="cumulative.living_arrangement" value="Living with relatives only">Living with relatives only
                        </label>
                    </div>
                    <div class="form-group">
                        <label class = "radio-inline">
                        <input type="radio" name="radLiving" id="radBoard" ng-model="cumulative.living_arrangement" value="Staying in dormitory or boarding house">Staying in dormitory or boarding house
                        </label>
                    </div>
                 
                    <div class="form-group">
                        <label class = "radio-inline">  
                        <input type="radio" name="radLiving" id="radFamRel" ng-model="cumulative.living_arrangement" value="Living with family and relatives">Living with family and relatives
                        </label>
                    </div>
                    <div class="form-group">
                        <label class = "radio-inline">
                        <input type="radio" name="radLiving" id="radOwn" ng-model="cumulative.living_arrangement" value="Living on my own">Living on my own
                        </label>
                    </div>
                
                 </div> 
            </div>
                   
            </div>
            <br/>
            <hr/>
            <br/>
            <div class="row">
                <h4>II. Physical/Mental Health Information</h4>
                <hr/>
   
                <h5>A. Do you have any health problems or physical limitations which may hinder you from performing well in your: </h5>
                <div class="form-inline">
                    <div class="col-md-6">
                    
                            <label for="radHealthAcad">
                                1. Academics
                                </label><br>
                    
                    <div class="form-group">
                        
                        <label class = "radio-inline">
                            <input type="radio" 
                            ng-model="x" 
                            value=""
                            ng-change="acadHealthProblemsChanged('Yes', false)"
                            ng-checked="cumulative.acad_health_prob !=='No'"
                            >Yes
                        </label>
                    </div>
                    <div class="form-group">
                            <label class = "radio-inline">
                            <input type="radio" 
                            ng-model="x"
                            value="No"
                            ng-change="acadHealthProblemsChanged('No', false)"
                            ng-checked="cumulative.acad_health_prob ==='No'"
                            >No
                            </label>
                    </div>
                    <br>
                
                    <div class="form-group">
                            <label for="specAcadHealth">Please Specify:</label>
                            <input type="text" 
                            class="form-control" 
                              
                            ng-model='acadZ'
                            ng-change="acadHealthProblemsChanged('', true)"
                            ng-disabled="cumulative.acad_health_prob === 'No'">
                    </div>
                    
                    </div>
                <div class="col-md-6">

                    <label for="radHealthExt">
                    2. Extra Curricular
                    </label><br>
                    <div class="form-group">
                        
                        <label class = "radio-inline">
                            <input type="radio"
                            ng-model="y" 
                            value="" 
                            ng-change="curricularHealthProblemsChanged('Yes', false)"
                            ng-checked="cumulative.curricular_health_prob !=='No'"
                             >Yes
                        </label>
                    </div>
                    <div class="form-group">
                            <label class = "radio-inline">
                            <input type="radio"
                            ng-model="y"
                            value="No"
                            ng-change="curricularHealthProblemsChanged('No', false)"
                            ng-checked="cumulative.curricular_health_prob ==='No'" 
                            >No
                            </label>
                    </div>
                    </div>
                        <div class="form-group">
                            <label for="specExtHealth">Please Specify:</label>
                            <input type="text" 
                            class="form-control"
                             ng-model='currZ'
                             ng-change="curricularHealthProblemsChanged('', true)"
                             ng-disabled="cumulative.curricular_health_prob === 'No'"
                             >
                        </div>

            </div>
                        <br/>
                        <h5>B. Have you ever sought psychiatric help? </h5>
                        <div class="form-inline">
                            <div class="form-group">
                                <label class = "radio-inline">
                                    <input type="radio" name="radHealthPsych" id="radYesPsych" ng-model="cumulative.psychiatric_help" ng-value="true" value="Yes" checked>Yes
                                </label>
                            </div>
                            <div class="form-group">
                                <label class = "radio-inline">
                                    <input type="radio" name="radHealthPsych" id="radNoPsych" ng-model="cumulative.psychiatric_help" ng-value="false" value="No">No
                                </label>
                            </div>
                        </div>
                        
                        <h5>C. Have you ever undergone counseling? </h5>
                        <div class="form-inline">
                            <div class="form-group">
                                <label class = "radio-inline">
                                    <input type="radio" name="radHealthCoun" id="radYesCoun" ng-model="cumulative.counseling" ng-value="true" value="Yes" checked>Yes
                                </label>
                            </div>
                            <div class="form-group">
                                <label class = "radio-inline">
                                    <input type="radio" name="radHealthCoun" id="radNoCoun" ng-model="cumulative.counseling" ng-value="false" value="No">No
                                </label>
                            </div>
                        </div>

                    </div>

                    <br/>
                    <hr/>
                    <br/>

                    <h4>III. Family Information</h4>
                    <hr/>

                    <div class="row">
          
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nameFather">Father's Name</label>
                                <input type="text" class="form-control" id="nameFather" placeholder="Father's name"  ng-model="cumulative.father_name">
                            </div>
                            <div class="form-group">
                                <label for="addressFather">Address</label>
                                <input type="text" class="form-control" id="addressFather" placeholder="Father's address"  ng-model="cumulative.father_address">
                             </div>
                            <div class="form-group">
                                <label for="occupationFather">Occupation</label>
                                <input type="text" class="form-control" id="occupationFather" placeholder="Father's work"  ng-model="cumulative.father_occupation">
                            </div>
                            <div class="form-group">
                                <label for="contactFather">Contact No.</label>
                                <input type="text" class="form-control" id="contactFather" placeholder="Father's #"  ng-model="cumulative.father_contact">
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="nameMother">Mother's Name</label>
                                <input type="text" class="form-control" id="nameFather" placeholder="Mother's name"  ng-model="cumulative.mother_name">
                            </div>
                            <div class="form-group">
                                <label for="addressMother">Address</label>
                                <input type="text" class="form-control" id="addressMother" placeholder="Mother's address"  ng-model="cumulative.mother_address">
                            </div>
                            <div class="form-group">
                                <label for="occupationMother">Occupation</label>
                                <input type="text" class="form-control" id="occupationMother" placeholder="Mother's work"  ng-model="cumulative.mother_occupation">
                            </div>
                            <div class="form-group">
                                <label for="contactMother">Contact No.</label>
                                <input type="text" class="form-control" id="contactMother" placeholder="Mother's #"  ng-model="cumulative.mother_contact">
                            </div>

                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-4">
                            <h5>Marital Status of Parents</h5>
                            <div class="radio">
                                <label>
                                <input type="radio" name="marStatPar" id="marStat1" ng-model="cumulative.parent_marital_status" value="marStat1" checked>
                                Married and living together
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                <input type="radio" name="marStatPar" id="marStat2" ng-model="cumulative.parent_marital_status" value="marStat2">
                                Married, not living together
                                </label>

                            </div>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label>
                                    <input type="radio" name="marStatParNot" id="marStatParNot1" ng-model="cumulative.parent_marital_status" value="marStatParNot1">
                                    Working abroad
                                    </label>
                                 </div>
                                 <div class="form-group">
                                    <label>
                                    <input type="radio" name="marStatParNotCho1" id="marStatParNotFat1" ng-model="cumulative.parent_marital_status_spec1"  value="Father" ng-disabled="cumulative.parent_marital_status != 'marStatParNot1'">
                                    Father
                                    </label>
                                 </div>
                                  <div class="form-group">
                                    <label>
                                    <input type="radio" name="marStatParNotCho1" id="marStatParNotFat2" ng-model="cumulative.parent_marital_status_spec1"  value="Mother" ng-disabled="cumulative.parent_marital_status != 'marStatParNot1'">
                                    Mother
                                    </label>
                                 </div>
                            </div>

                             <div class="form-inline">
                                <div class="form-group">
                                    <label>
                                    <input type="radio" name="marStatParNot" id="marStatParNot2" ng-model="cumulative.parent_marital_status" value="marStatParNot2">
                                    Deceased
                                    </label>
                                 </div>
                                 <div class="form-group">
                                    <label>
                                    <input type="radio" name="marStatParNotCho2" id="marStatParNotMot1" ng-model="cumulative.parent_marital_status_spec2"  value="Father" ng-disabled="cumulative.parent_marital_status != 'marStatParNot2'">
                                    Father
                                    </label>
                                 </div>
                                  <div class="form-group">
                                    <label>
                                    <input type="radio" name="marStatParNotCho2" id="marStatParNotMot2" ng-model="cumulative.parent_marital_status_spec2"  value="Mother" ng-disabled="cumulative.parent_marital_status != 'marStatParNot2'">
                                    Mother
                                    </label>
                                 </div>
                            </div>

                            <div class="radio">
                                <label>
                                <input type="radio" name="marStatPar" id="marStat3" ng-model="cumulative.parent_marital_status" value="marStat3">
                                Not married, single parent
                                </label>
                               
                            </div>
                            
                            <div class="form-inline">
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                        <input type="radio" name="marStatPar" id="marStat4" ng-model="cumulative.parent_marital_status" value="marStat4">
                                        Others:
                                        </label>
                                       
                                        <input type="text" class="form-control" id="othersMarStat4" ng-model="cumulative.parent_marital_status_other"   ng-disabled="cumulative.parent_marital_status != 'marStat4'">
                                    </div>
                                </div>
                            </div>
                    
                            </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="guardianName">Guardian's Name</label>
                                <input type="text" class="form-control" id="guardianName" placeholder="Guardians's Name" ng-model="cumulative.guardian_name" >
                            </div>
                            <div class="form-group">
                                <label for="guardianRelation">Relation</label>
                                <input type="text" class="form-control" id="guardianRelation"  ng-model="cumulative.guardian_relation" >
                            </div>
                            <div class="form-group">
                                <label for="guardianContact">Contact #</label>
                                <input type="text" class="form-control" id="guardianContact" placeholder="Guardian's #" ng-model="cumulative.guardian_contact" >
                            </div>

                        </div>

                        <div class="col-md-4">
                            <h5>How do you perceive your family life?</h5>
                            <div class="radio">
                                <label>
                                <input type="radio" name="famLife" id="famLife1" ng-model="cumulative.family_life_perception" value="Very Good" checked>
                                Very Good
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                <input type="radio" name="famLife" id="famLife2" ng-model="cumulative.family_life_perception" value="Somewhat Good">
                                Somewhat Good
                                </label>

                            </div>
                            <div class="radio">
                                <label>
                                <input type="radio" name="famLife" id="famLife3" ng-model="cumulative.family_life_perception" value="Not as Good">
                                Not as Good
                                </label>

                            </div>
                            
                            <h5>In my family, I am close with my</h5>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkMom" ng-model="cumulative.close_mother" ng-change="closeFamilyCheckboxChanged('Mother')" ng-true-value="1" ng-false-value="0" > Mother
                            </label>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkFather" ng-model="cumulative.close_father" ng-change="closeFamilyCheckboxChanged('Father')" ng-true-value="1" ng-false-value="0" > Father
                            </label>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkOldSib" ng-model="cumulative.close_old_sibling" ng-change="closeFamilyCheckboxChanged('oldSibling')" ng-true-value="1" ng-false-value="0" > Older Sibling
                            </label>
                            <br/>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkYoungSib" ng-model="cumulative.close_young_sibling" ng-change="closeFamilyCheckboxChanged('youngSibling')" ng-true-value="1" ng-false-value="0" > Younger Sibling
                            </label>
                            <div class="form-inline">
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkOther" ng-model="cumulative.close_other" ng-change="closeFamilyCheckboxChanged('Other')" ng-true-value="1" ng-false-value="0" > Other
                            <input type="text" class="form-control" id="othersCheckPar"  ng-change="closeFamilyCheckboxChanged('Spec')" ng-model="cumulative.close_other_spec" ng-disabled="!cumulative.close_other">
                            </label>
                            </div>
                            
                        </div>
                    </div>
                    <br/>
                    <hr/>
                    <br/>
                    <div class="row">
                    <h4>IV. Vocational Academic Placement</h4>
                    <hr/>
                   
                        <div class="col-md-12">
                        <h5>I am/will be motivated to finish my program because of (Check Top 3 that applies)</h5>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkMotivation[0]" ng-disabled="checkMotivation[0]" ng-change="checkboxChanged(0)" ng-model="cumulative.motivate_family" ng-true-value="1" ng-false-value="0" value="checkFam"> Family
                            </label>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkMotivation[1]" ng-disabled="checkMotivation[1]" ng-change="checkboxChanged(1)" ng-model="cumulative.motivate_significant_other" ng-true-value="1" ng-false-value="0" value="checkSig"> Significant Other
                            </label>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkMotivation[2]" ng-disabled="checkMotivation[2]" ng-change="checkboxChanged(2)" ng-model="cumulative.motivate_prestige" ng-true-value="1" ng-false-value="0" value="checkPrest"> Prestige
                            </label>
                            <br/>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkMotivation[3]" ng-disabled="checkMotivation[3]" ng-change="checkboxChanged(3)" ng-model="cumulative.motivate_friends" ng-true-value="1" ng-false-value="0" value="checkFriends"> Friends
                            </label>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkMotivation[4]" ng-disabled="checkMotivation[4]" ng-change="checkboxChanged(4)" ng-model="cumulative.motivate_career" ng-true-value="1" ng-false-value="0" value="checkCareer"> Career Goals
                            </label>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkMotivation[5]" ng-disabled="checkMotivation[5]" ng-change="checkboxChanged(5)" ng-model="cumulative.motivate_passion" ng-true-value="1" ng-false-value="0" value="checkPassion"> Passion
                            </label>
                            <br/>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkMotivation[6]" ng-disabled="checkMotivation[6]" ng-change="checkboxChanged(6)" ng-model="cumulative.motivate_job" ng-true-value="1" ng-false-value="0" value="checkJob"> Job Opportunities
                            </label>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkMotivation[7]" ng-disabled="checkMotivation[7]" ng-change="checkboxChanged(7)" ng-model="cumulative.motivate_fulfillment" ng-true-value="1" ng-false-value="0" value="checkSelfFul"> Self-Fulfillment
                            </label>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkMotivation[8]" ng-disabled="checkMotivation[8]" ng-change="checkboxChanged(8)" ng-model="cumulative.motivate_interest" ng-true-value="1" ng-false-value="0" value="checkInterest"> Interest
                            </label>
                            <br/>
                            <br/>
                        <h5>In the future, I might seek the help of counselor because of</h5>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkInfo" ng-model="cumulative.counselor_info" ng-true-value="1" ng-false-value="0" value="checkInfo"> Information (Policies, etc)
                            </label>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkAcad" ng-model="cumulative.counselor_academics" ng-true-value="1" ng-false-value="0" value="checkAcad"> Academic Difficulties
                            </label>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkRelCon" ng-model="cumulative.counselor_relationships" ng-true-value="1" ng-false-value="0" value="checkRelCon"> Relationship Concerns (family, peers, significant other, relatives)
                            </label>
                            <br/>
                            <label class="checkbox-inline">
                            <input type="checkbox" id="checkPerCon" ng-model="cumulative.counselor_personal" ng-true-value="1" ng-false-value="0" value="checkPerCon"> Personal Concerns
                            </label>
                            <div class="form-inline">
                                <label class="checkbox-inline">
                                <input type="checkbox" id="checkCounOther" ng-model="cumulative.counselor_others" ng-true-value="1" ng-false-value="0" value="checkCounOther"> Others
                                </label>
                                <input type="text" class="form-control" id="othersCheckCoun"  ng-model="cumulative.counselor_others_spec" ng-disabled="!cumulative.counselor_others">
                            </div>
                        </div>
                    </div>
                    <br/>
                    </div> <!-- end readonly -->

                    <!-- query first and display -->
                    <div class="row">
                    <h4>Added Cumulative Question</h4>
                    <table class="table table-striped table-condensed">
                        <tr ng-repeat="addedCumulativeField in addedCumulativeFields">
                            <td>@{{ addedCumulativeField.field }}</td>
                            <td><input type="text" class="form-control"  id="questionAdditional1" ng-model="addedCumulativeField.value" ng-focus="field_focus(addedCumulativeField)" > </td>

                            @if(Auth::user()->status == 'guidance')
                            <td><button class="btn btn-info glyphicon glyphicon-pencil" ng-click="editAddedField(addedCumulativeField)"> </button> <button class="btn btn-danger glyphicon glyphicon-minus" ng-click="removeAddedField(addedCumulativeField)"> </button></td>
                            @endif
                        </tr>
                    </table>
                    </div>


                    
                    @if(Auth::user()->status == 'guidance')
                         <button class="btn btn-primary" ng-click="addQuestionClicked()"> Add Question </button>
                    @endif

                    <div class="modal fade" id="add_question_modal">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">Add Question</h4>
                          </div>
                          <div class="modal-body">
                            <input type="text" class="form-control" id="questionAdditional"  ng-model='added_question_field'>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" ng-click="add_question()">Add Question</button>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                    <div class="modal fade" id="edit_question_modal">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">Edit Question</h4>
                          </div>
                          <div class="modal-body">
                            <input type="text" class="form-control" ng-model='edit_question.field' id='edit_question_input' name="edit_question_input">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" ng-click="updateAddedField(edit_question)">Update</button>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->


                    <div class="modal fade" id="answer_question_modal">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">Answer Question</h4>
                          </div>
                          <div class="modal-body">
                            <h4>@{{answerField.field}}</h4>
                            <input type="text" class="form-control" ng-model="answerField.value">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" ng-click="answer_clicked()">Answer</button>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                    @if(Auth::user()->status == 'student')
                    <button type="button" class="btn btn-primary" ng-click="save(cumulative.cum_id)">Submit</button>
                    @endif
                </div>

                  
                

            </div>
           
    
        </div>
        
    

@endsection

@section('controller')
<script src="<?= asset('app/controllers/cumulative.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angucomplete-alt.js') ?>"></script>
@endsection       