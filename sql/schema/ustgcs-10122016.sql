-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2016 at 01:19 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ustgcs`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE IF NOT EXISTS `announcement` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `title`, `message`, `created_by`, `created_at`, `updated_at`) VALUES
(3, 'Hi Guidance Counselors!', 'Welcome to the our new website!', '2010000001', '2016-05-13 04:11:03', '2016-05-13 04:11:03'),
(4, 'Hi 1st year IICS!', 'Hi 1st year IICS!', '2010000001', '2016-05-13 04:15:48', '2016-05-13 04:15:48'),
(5, 'Hi 2nd year CS IICS Students!', 'Welcome cs iics students!', '2011000001', '2016-05-13 04:25:23', '2016-05-13 04:25:23'),
(6, 'Hi eng counselors!', 'welcome', '2010000001', '2016-05-13 04:33:50', '2016-05-13 04:33:50'),
(7, 'Hi 2nd year CS IICS Students! (Part2)', 'welcome (part2)', '2011000001', '2016-05-13 04:37:55', '2016-05-13 04:37:55'),
(8, 'Hi to all 1st year IS', 'welcome', '2011000001', '2016-05-13 10:15:00', '2016-05-13 10:15:00'),
(9, 'Final exam', 'tomorrow', '2011000001', '2016-05-13 10:16:13', '2016-05-13 10:16:13');

-- --------------------------------------------------------

--
-- Table structure for table `announcement_flag`
--

CREATE TABLE IF NOT EXISTS `announcement_flag` (
  `id` int(10) unsigned NOT NULL,
  `announcement_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `announcement_flag`
--

INSERT INTO `announcement_flag` (`id`, `announcement_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, '3', '2011000003', '2016-05-13 04:12:47', '2016-05-13 04:12:47'),
(3, '9', '654322', '2016-10-08 13:01:34', '2016-10-08 13:01:34');

-- --------------------------------------------------------

--
-- Table structure for table `announcement_permission`
--

CREATE TABLE IF NOT EXISTS `announcement_permission` (
  `id` int(10) unsigned NOT NULL,
  `announcement_id` int(11) NOT NULL,
  `year` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `college` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `professor` tinyint(1) NOT NULL,
  `guidance` tinyint(1) NOT NULL,
  `director` tinyint(1) NOT NULL,
  `is_all` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `announcement_permission`
--

INSERT INTO `announcement_permission` (`id`, `announcement_id`, `year`, `college`, `course`, `professor`, `guidance`, `director`, `is_all`, `created_at`, `updated_at`) VALUES
(3, 3, '', '', '', 0, 1, 1, 0, '2016-05-13 04:11:03', '2016-05-13 04:11:03'),
(4, 4, '1', '3', '', 0, 0, 1, 0, '2016-05-13 04:15:48', '2016-05-13 04:15:48'),
(5, 5, '2', '3', '3,2', 0, 1, 0, 0, '2016-05-13 04:25:23', '2016-05-13 04:25:23'),
(6, 6, '', '', '1', 0, 1, 1, 0, '2016-05-13 04:33:50', '2016-05-13 04:33:50'),
(7, 7, '2', '3', '3', 0, 1, 0, 0, '2016-05-13 04:37:55', '2016-05-13 04:37:55'),
(8, 8, '1', '', '2', 0, 1, 0, 0, '2016-05-13 10:15:00', '2016-05-13 10:15:00'),
(9, 9, '1,2', '3', '2,3', 0, 1, 0, 0, '2016-05-13 10:16:13', '2016-05-13 10:16:13');

-- --------------------------------------------------------

--
-- Table structure for table `colleges`
--

CREATE TABLE IF NOT EXISTS `colleges` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `colleges`
--

INSERT INTO `colleges` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Engineering', '2016-04-24 08:08:00', '2016-04-24 08:08:00'),
(2, 'Science', '2016-04-24 08:08:12', '2016-05-13 02:39:50'),
(3, 'Institute of Information and Computing Sciences', '2016-04-24 08:08:22', '2016-05-13 02:41:59'),
(4, 'Commerce', '2016-05-13 02:42:25', '2016-05-13 02:42:43');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `max_years` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `college_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `max_years`, `college_id`, `created_at`, `updated_at`) VALUES
(1, 'BS Civil Engineering', '5', 1, '2016-04-24 08:08:47', '2016-04-24 08:08:47'),
(2, 'BS Information System', '4', 3, '2016-04-24 08:09:02', '2016-04-24 08:09:02'),
(3, 'BS Computer Science', '4', 3, '2016-04-24 08:09:23', '2016-04-24 08:09:23'),
(4, 'BS Biology', '4', 2, '2016-04-24 08:09:56', '2016-04-24 08:09:56');

-- --------------------------------------------------------

--
-- Table structure for table `cumulative_record`
--

CREATE TABLE IF NOT EXISTS `cumulative_record` (
  `cum_id` int(10) unsigned NOT NULL,
  `student_id` int(11) NOT NULL,
  `auxiliary_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `religion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `curr_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `curr_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perm_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perm_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_phone_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `living_arrangement` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `acad_health_prob` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `curricular_health_prob` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `psychiatric_help` tinyint(1) NOT NULL,
  `counseling` tinyint(1) NOT NULL,
  `father_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `father_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `father_occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `father_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mother_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mother_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mother_occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mother_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_marital_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_marital_status_spec1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_marital_status_spec2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_marital_status_other` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guardian_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guardian_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `family_life_perception` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `close_family` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `motivate_factor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `counselor_help` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cumulative_record`
--

INSERT INTO `cumulative_record` (`cum_id`, `student_id`, `auxiliary_name`, `nickname`, `nationality`, `religion`, `email`, `curr_address`, `curr_city`, `perm_address`, `perm_city`, `mobile_no`, `home_phone_no`, `living_arrangement`, `acad_health_prob`, `curricular_health_prob`, `psychiatric_help`, `counseling`, `father_name`, `father_address`, `father_occupation`, `father_contact`, `mother_name`, `mother_address`, `mother_occupation`, `mother_contact`, `parent_marital_status`, `parent_marital_status_spec1`, `parent_marital_status_spec2`, `parent_marital_status_other`, `guardian_name`, `guardian_relation`, `guardian_contact`, `family_life_perception`, `close_family`, `motivate_factor`, `counselor_help`, `created_at`, `updated_at`) VALUES
(1, 1234, 'x', 'Frederique', 'Filipino', 'christian', 'sample@student.com', '479 Pagac Route Suite 478\r\nBinsmouth, CT 71474', 'South Ariel', '630 Flavio Knoll Apt. 041\r\nLaurentown, HI 67757', 'Beulahhaven', '(072)081-5849', '160-613-3768x7025', 'Living with family and relatives', 'No', 'HIKA', 0, 0, 'Vernon Buckridge', '48893 Hintz Corners Suite 760\r\nNew Brandon, FL 52061', 'Driver', '965-396-0480', 'Sigmund Kling', '85222 Pansy Track\r\nLake Rolandofurt, WA 17024', 'Housewife', '515.240.6293x680', 'marStat1', '', '', '', 'Sincere Bradtke', 'Elisha Yundt', 'Mathilde Strosin', 'Somewhat Good', 'Mother,Father,Older Sibling,WEAK', 'Family,Prestige', 'Information,Academics', '2016-05-04 13:35:17', '2016-05-13 03:06:24'),
(3, 2012000001, '', 'mark', 'Filipino', 'Roman Catholic', '2012000001@ust-ics.mygbiz.com', '12 apple street', 'Marikina', '2312 apple', 'Marikina', '987654', '12324', 'Living with Immediate Family', 'No', 'No', 0, 0, 'mark abloa', '23132 marikina', 'businessman', '6343297', 'martha abloa', '423122 marikina', 'housewife', '091312', 'marStat2', '', '', '', 'martha', 'mother', '0997312', 'Somewhat Good', 'Young Sibling', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:06:54', '2016-05-13 16:15:27'),
(4, 2012000003, '', 'isabela', 'Filipino', 'INC', '2012000003@ust-ics.mygbiz.com', '121 gumamela street', 'Caloocan', '121 gumamela street', 'Quezon', '0942312', '4123512', 'Living with Immediate Family', 'No', '', 0, 0, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStatParNot2', '', 'Father', '', 'nora', 'mother', '42123', 'Very Good', 'Mother,Young Sibling', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(5, 2012000004, '', 'Jego', 'Filipino', 'Roman Catholic', '2012000004@ust-ics.mygbiz.com', '121-b malanday', 'Valenzuela', '121-b malanday', 'Valenzuela', '0942312', '4123512', 'Living with family and relatives', 'No', 'Asthma', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat2', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother,Young Sibling', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 15:53:16'),
(6, 2012000005, '', 'pon', 'Filipino', 'Roman Catholic', '2012000005@ust-ics.mygbiz.com', '132-C 1st street rizal ave', 'Bulacan', '132-C 1st street rizal ave', 'Bulacan', '0942312', '4123512', 'Living with Immediate Family', 'No', 'No', 1, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Somewhat Good', 'Mother,Young Sibling', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(7, 2012000007, '', 'gelo', 'Filipino', 'Roman Catholic', '2012000007@ust-ics.mygbiz.com', '12 1st st 2nd ave', 'Manila', '12 1st st 2nd ave', 'Manila', '0942312', '4123512', 'Living with Immediate Family', 'Depression', 'No', 0, 0, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Older Sibling', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(8, 2012000009, '', 'Peter', 'Filipino', 'Catholic', '2012000009@ust-ics.mygbiz.com', 'Pacific dapitan sampaloc', 'Manila', '98 los angeles 3rd st', 'Pampanga', '0942312', '4123512', 'Living with Immediate Family', 'no', 'No', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Father', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(9, 2012000010, '', 'Bert', 'Filipino', 'INC', '2012000010@ust-ics.mygbiz.com', 'BF Homes', 'Paranaque', 'BF Homes', 'Paranaque', '0942312', '4123512', 'Living with family and relatives', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Father', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(10, 2012000011, '', 'Danielle', 'Filipino', 'Roman Catholic', '2012000011@ust-ics.mygbiz.com', 'Sun residences', 'Muntinlupa', 'Sun residences', 'Muntinlupa', '0942312', '4123512', 'Living with family and relatives', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Younger Sibling', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(11, 2012000012, '', 'Natasha', 'Filipino', 'Roman Catholic', '2012000012@ust-ics.mygbiz.com', 'Sun residences', 'Muntinlupa', 'Sun residences', 'Muntinlupa', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 1, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat2', '', '', '', 'nora', 'mother', '42123', 'Somewhat Good', 'Younger Sibling,Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(12, 2012000013, '', 'Benj', 'Filipino', 'Roman Catholic', '2012000013@ust-ics.mygbiz.com', '5th ave', 'Caloocan', '5th ave', 'Caloocan', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 0, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(13, 2012000015, '', 'Joy', 'Filipino', 'Catholic', '2012000015@ust-ics.mygbiz.com', '93 Espanya marzan St', 'Manila', 'Malolos', 'Bulacan', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(14, 2012000016, '', 'Jade', 'Filipino', 'Roman Catholic', '2012000016@ust-ics.mygbiz.com', '10th ave 6th street', 'Caloocan', '10th ave 6th street', 'Caloocan', '0942312', '4123512', 'Living with Immediate Family', 'no', 'Asthma', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(15, 2012000017, 'jr', 'jr', 'Filipino', 'Roman Catholic', '2012000017@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(16, 2012000018, '', 'Anna', 'Filipino', 'Roman Catholic', '2012000018@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(19, 2012000020, '', 'Jerico', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Staying in dormitory or boarding house', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(20, 2012000021, '', 'Miko', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Staying in dormitory or boarding house', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(21, 2012000024, '', 'Paul', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Paranaque', '10th ave 6th street', 'Paranaque', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(22, 2012000025, '', 'Drea', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Pampanga', '10th ave 6th street', 'Pampanga', '0942312', '4123512', 'Staying in dormitory or boarding house', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(23, 2012000026, '', 'Marco', 'Chinese', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(24, 2012000028, '', 'Shaun', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Staying in dormitory or boarding house', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(25, 2012000029, '', 'Gerald', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(26, 2012000030, '', 'Bien', 'Indian', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Caloocan', '10th ave 6th street', 'Caloocan', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(44, 2012000031, '', 'Pau', 'Filipino', 'INC', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Staying in dormitory or boarding house', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(45, 2012000032, '', 'Jau', 'Filipino', 'Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Quezon', '0942312', '4123512', 'Staying in dormitory or boarding house', 'no', 'no', 1, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStatParNot2', '', 'Father', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(46, 2012000033, '', 'Kim', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Paranaque', '10th ave 6th street', 'Paranaque', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(47, 2012000037, '', 'Leo', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Pampanga', '10th ave 6th street', 'Pampanga', '0942312', '4123512', 'Staying in dormitory or boarding house', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Older Sibling', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(48, 2012000039, '', 'Miguel', 'Chinese', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 1, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat2', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(49, 2012000040, '', 'Kurt', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Cagayan', '0942312', '4123512', 'Staying in dormitory or boarding house', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat2', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother,Younger Sibling', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(50, 2012000041, '', 'Kiara', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 1, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(51, 2012000042, '', 'Andre', 'Indian', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Caloocan', '10th ave 6th street', 'Caloocan', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(52, 2012000043, '', 'Paul', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStatParNot2', '', 'Mother', '', 'nora', 'mother', '42123', 'Very Good', 'Mother,Father', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(53, 2012000048, '', 'Sarah', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Quezon', '10th ave 6th street', 'Bulacan', '0942312', '4123512', 'Staying in dormitory or boarding house', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat2', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(54, 2012000049, '', 'Carlo', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Somewhat Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(55, 2012000050, '', 'Joeph', 'Chinese', 'INC', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(56, 2012000052, 'III', 'Ferdi', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Pampanga', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat2', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Younger Sibling', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(57, 2012000053, '', 'Edwin', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 1, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Not as Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(58, 2012000054, '', 'Carl', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with relatives only', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Father', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(59, 2012000056, '', 'John', 'Filipino', 'Roman Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Pangasinan', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 0, 0, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat2', '', '', '', 'nora', 'mother', '42123', 'Not as Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(60, 2012000058, '', 'Roca', 'Filipino', 'INC', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Muntinlupa', '0942312', '4123512', 'Living with Immediate Family', 'no', 'no', 1, 0, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Mother', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26'),
(61, 2012000059, '', 'Pai;', 'Filipino', 'Catholic', '2012000019@ust-ics.mygbiz.com', '10th ave 6th street', 'Manila', '10th ave 6th street', 'Manila', '0942312', '4123512', 'Living with relatives only', 'no', 'no', 0, 1, 'Nick', '121 gumamela street', 'businessman', '092312', 'Nora Agulaa', '121 gumamela street', 'businesswoman', '31230912', 'marStat1', '', '', '', 'nora', 'mother', '42123', 'Very Good', 'Father', 'Family,Prestige', 'Information,Academics', '2016-05-13 05:14:52', '2016-05-13 09:42:26');

-- --------------------------------------------------------

--
-- Table structure for table `cumulative_record_additional`
--

CREATE TABLE IF NOT EXISTS `cumulative_record_additional` (
  `id` int(10) unsigned NOT NULL,
  `user_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cumulative_record_additional`
--

INSERT INTO `cumulative_record_additional` (`id`, `user_code`, `field`, `value`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '2012000001', 'additional question', 'test', '123', '2016-10-10 14:11:21', '2016-10-10 14:11:22'),
(2, '2012000001', 'asdasdsad', '12312', '654322', '2016-10-11 15:27:33', '2016-10-11 15:27:33'),
(3, '2012000001', 'asdasdasdasdsade32333', 'tesedras', '654322', '2016-10-11 15:28:50', '2016-10-11 15:28:50'),
(4, '2012000001', 'test122', 'teewras', '654322', '2016-10-11 15:30:07', '2016-10-11 15:30:07'),
(5, '2012000001', 'asdasdsad2322', 'asdasdqwe', '654322', '2016-10-11 15:30:31', '2016-10-11 15:30:31');

-- --------------------------------------------------------

--
-- Table structure for table `director`
--

CREATE TABLE IF NOT EXISTS `director` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `director`
--

INSERT INTO `director` (`id`, `code`, `created_at`, `updated_at`) VALUES
(1, '2010000000', '2016-04-12 13:32:30', '2016-04-12 13:32:30');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(10) unsigned NOT NULL,
  `referral_id` int(11) NOT NULL,
  `guidance_id` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_read` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedbackchat`
--

CREATE TABLE IF NOT EXISTS `feedbackchat` (
  `id` int(10) unsigned NOT NULL,
  `referral_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feedbackchat`
--

INSERT INTO `feedbackchat` (`id`, `referral_id`, `user_id`, `user_status`, `message`, `created_at`, `updated_at`) VALUES
(1, 11, 2011000003, 'guidance', 'Noted', '2016-05-13 10:07:55', '2016-05-13 10:07:55'),
(2, 11, 2011000003, 'guidance', 'already talkd to him', '2016-05-13 10:08:08', '2016-05-13 10:08:08'),
(3, 11, 2009000001, 'professor', 'Okay', '2016-05-13 10:08:46', '2016-05-13 10:08:46'),
(4, 11, 654322, 'guidance', 'test', '2016-10-08 08:42:44', '2016-10-08 08:42:44'),
(5, 11, 654322, 'guidance', 'test', '2016-10-08 08:43:58', '2016-10-08 08:43:58'),
(6, 11, 654322, 'guidance', 'test', '2016-10-08 08:44:43', '2016-10-08 08:44:43'),
(7, 11, 654322, 'guidance', '1234', '2016-10-08 08:45:14', '2016-10-08 08:45:14'),
(8, 11, 654322, 'guidance', '13456', '2016-10-08 09:05:02', '2016-10-08 09:05:02');

-- --------------------------------------------------------

--
-- Table structure for table `guidance_councelor`
--

CREATE TABLE IF NOT EXISTS `guidance_councelor` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `college_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `guidance_councelor`
--

INSERT INTO `guidance_councelor` (`id`, `code`, `college_id`, `created_at`, `updated_at`) VALUES
(2, '654322', 3, '2016-04-24 08:48:53', '2016-04-24 08:48:53'),
(3, '2011000001', 3, '2016-05-13 03:44:47', '2016-05-13 03:44:47'),
(4, '2011000002', 3, '2016-05-13 03:44:47', '2016-05-13 03:44:47'),
(5, '2011000003', 1, '2016-05-13 03:44:47', '2016-05-13 03:44:47'),
(6, '2011000004', 1, '2016-05-13 03:44:48', '2016-05-13 03:44:48'),
(7, '2011000005', 1, '2016-05-13 03:44:48', '2016-05-13 03:44:48'),
(8, '2011000006', 1, '2016-05-13 03:44:48', '2016-05-13 03:44:48');

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE IF NOT EXISTS `inbox` (
  `id` int(11) NOT NULL,
  `student_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_read` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inbox`
--

INSERT INTO `inbox` (`id`, `student_id`, `created_by`, `title`, `message`, `is_read`, `created_at`, `updated_at`) VALUES
(1, '2012000014', '654322', 'Hi', 'Hi Wazzup Hello', 1, '2016-10-08 07:47:42', '2016-10-08 07:49:48');

-- --------------------------------------------------------

--
-- Table structure for table `inbox_reply`
--

CREATE TABLE IF NOT EXISTS `inbox_reply` (
  `id` int(10) unsigned NOT NULL,
  `inbox_id` int(11) NOT NULL,
  `sender_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inbox_reply`
--

INSERT INTO `inbox_reply` (`id`, `inbox_id`, `sender_code`, `status`, `message`, `created_at`, `updated_at`) VALUES
(1, 1, '1234', 'student', 'pogi ko', '2016-10-08 09:34:40', '2016-10-08 09:34:41'),
(2, 1, '654322', 'guidance', 'uuuuuu', '2016-10-08 09:55:55', '2016-10-08 09:55:55'),
(3, 1, '654322', 'guidance', 'test', '2016-10-08 12:42:52', '2016-10-08 12:42:52'),
(4, 1, '654322', 'guidance', 'hello hi', '2016-10-08 12:45:14', '2016-10-08 12:45:14'),
(5, 1, '654322', 'guidance', 'POG', '2016-10-08 12:46:22', '2016-10-08 12:46:22'),
(6, 1, '654322', 'guidance', '2354235326', '2016-10-08 12:46:41', '2016-10-08 12:46:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_04_24_165610_create_announcement_flag_table', 1),
('2016_04_24_165610_create_announcement_permission', 1),
('2016_04_24_165610_create_announcement_table', 1),
('2016_04_24_165610_create_colleges_table', 1),
('2016_04_24_165610_create_courses_table', 1),
('2016_04_24_165610_create_cumulative_record_additional_table', 1),
('2016_04_24_165610_create_cumulative_record_table', 1),
('2016_04_24_165610_create_director_table', 1),
('2016_04_24_165610_create_feedback_table', 1),
('2016_04_24_165610_create_feedbackchat', 1),
('2016_04_24_165610_create_guidance_councelor_table', 1),
('2016_04_24_165610_create_inbox_table', 1),
('2016_04_24_165610_create_non_compliance_notification_table', 1),
('2016_04_24_165610_create_notification_badge', 1),
('2016_04_24_165610_create_organization_table', 1),
('2016_04_24_165610_create_password_resets_table', 1),
('2016_04_24_165610_create_professor_table', 1),
('2016_04_24_165610_create_referral_table', 1),
('2016_04_24_165610_create_student_table', 1),
('2016_04_24_165610_create_users_table', 1),
('2016_10_08_163443_create_inbox_reply_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `non_compliance_notification`
--

CREATE TABLE IF NOT EXISTS `non_compliance_notification` (
  `id` int(10) unsigned NOT NULL,
  `user_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification_badge`
--

CREATE TABLE IF NOT EXISTS `notification_badge` (
  `id` int(10) unsigned NOT NULL,
  `user_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `announcement` int(11) NOT NULL,
  `inbox` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification_badge`
--

INSERT INTO `notification_badge` (`id`, `user_code`, `announcement`, `inbox`, `created_at`, `updated_at`) VALUES
(6, '1234', 0, NULL, '2016-05-13 03:05:42', '2016-05-13 03:05:42'),
(7, '90909090', 0, NULL, '2016-05-13 03:11:46', '2016-05-13 03:11:46'),
(8, '2012000001', 0, 0, '2016-05-13 03:36:17', '2016-10-09 02:22:16'),
(9, '2011000001', 6, NULL, '2016-05-13 03:45:44', '2016-05-13 10:16:13'),
(10, '2010000001', 3, NULL, '2016-05-13 04:07:27', '2016-05-13 04:33:50'),
(11, '2011000003', 3, NULL, '2016-05-13 04:12:22', '2016-05-13 04:38:12'),
(12, '2012000031', 0, NULL, '2016-05-13 04:16:37', '2016-05-13 04:16:37'),
(13, '2012000054', 0, NULL, '2016-05-13 04:17:35', '2016-05-13 04:17:35'),
(14, '2012000025', 1, NULL, '2016-05-13 04:18:09', '2016-05-13 04:18:09'),
(15, '2012000033', 2, NULL, '2016-05-13 04:26:52', '2016-05-13 10:15:38'),
(16, '2012000003', 3, NULL, '2016-05-13 05:07:59', '2016-05-13 16:12:20'),
(17, '2012000002', 1, NULL, '2016-05-13 05:26:11', '2016-05-13 05:26:11'),
(18, '2012000010', 3, NULL, '2016-05-13 10:16:33', '2016-05-13 10:16:33'),
(19, '2012000035', 3, NULL, '2016-05-13 10:16:48', '2016-05-13 10:16:48'),
(20, '2012000004', 3, NULL, '2016-05-13 15:52:00', '2016-05-13 15:52:00'),
(21, '654322', 5, NULL, '2016-05-13 18:39:29', '2016-10-08 13:01:34'),
(22, '2012000014', 3, 0, '2016-10-08 07:43:47', '2016-10-08 07:49:48');

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `professor`
--

CREATE TABLE IF NOT EXISTS `professor` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `college_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `professor`
--

INSERT INTO `professor` (`id`, `code`, `college_id`, `created_at`, `updated_at`) VALUES
(3, '784512', 2, '2016-05-03 13:23:08', '2016-05-03 13:23:08'),
(4, '2009000001', 3, '2016-05-13 05:22:30', '2016-05-13 05:22:30'),
(5, '2009000002', 3, '2016-05-13 05:22:30', '2016-05-13 05:22:30'),
(6, '2009000003', 3, '2016-05-13 05:23:50', '2016-05-13 05:23:50');

-- --------------------------------------------------------

--
-- Table structure for table `referral`
--

CREATE TABLE IF NOT EXISTS `referral` (
  `id` int(10) unsigned NOT NULL,
  `professor_id` int(11) NOT NULL,
  `college_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `student_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_read` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `referral`
--

INSERT INTO `referral` (`id`, `professor_id`, `college_id`, `student_id`, `message`, `description`, `is_read`, `created_at`, `updated_at`) VALUES
(7, 2009000001, '1', '2012000006', 'Absenteeism/Tardiness', '2 weeks absent', 0, '2016-05-13 06:00:08', '2016-05-13 06:00:08'),
(8, 2009000001, '1', '2012000002', 'Course Indecision', 'He''s not motivated anymore to go to school', 0, '2016-05-13 06:01:41', '2016-05-13 06:01:41'),
(9, 2009000001, '1', '2012000005', 'Absenteeism/Tardiness', 'He''s FA already', 0, '2016-05-13 06:02:13', '2016-05-13 06:02:13'),
(10, 2009000001, '1', '2012000010', 'Course Indecision', 'not motivated', 0, '2016-05-13 06:04:40', '2016-05-13 06:04:40'),
(11, 2009000001, '1', '2012000002', 'Absenteeism/Tardiness', '1 week absent', 0, '2016-05-13 10:06:16', '2016-05-13 10:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `college_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `code`, `year`, `college_id`, `course_id`, `created_at`, `updated_at`) VALUES
(2, '1234', 3, 2, 4, '2016-04-24 08:43:06', '2016-04-24 08:43:06'),
(3, '2012000001', 1, 1, 1, '2016-05-13 03:30:43', '2016-05-13 03:30:43'),
(4, '2012000002', 1, 3, 2, '2016-05-13 03:30:43', '2016-05-13 03:30:43'),
(5, '2012000003', 1, 3, 2, '2016-05-13 03:30:43', '2016-05-13 03:30:43'),
(6, '2012000004', 1, 3, 2, '2016-05-13 03:30:44', '2016-05-13 03:30:44'),
(7, '2012000005', 1, 3, 2, '2016-05-13 03:30:44', '2016-05-13 03:30:44'),
(8, '2012000006', 1, 3, 2, '2016-05-13 03:30:44', '2016-05-13 03:30:44'),
(9, '2012000007', 1, 3, 2, '2016-05-13 03:30:44', '2016-05-13 03:30:44'),
(10, '2012000008', 1, 3, 2, '2016-05-13 03:30:44', '2016-05-13 03:30:44'),
(11, '2012000009', 1, 3, 2, '2016-05-13 03:30:45', '2016-05-13 03:30:45'),
(12, '2012000010', 1, 3, 2, '2016-05-13 03:30:45', '2016-05-13 03:30:45'),
(13, '2012000011', 1, 3, 2, '2016-05-13 03:30:45', '2016-05-13 03:30:45'),
(14, '2012000012', 1, 3, 2, '2016-05-13 03:30:45', '2016-05-13 03:30:45'),
(15, '2012000013', 1, 3, 2, '2016-05-13 03:30:45', '2016-05-13 03:30:45'),
(16, '2012000014', 1, 3, 2, '2016-05-13 03:30:45', '2016-05-13 03:30:45'),
(17, '2012000015', 1, 3, 2, '2016-05-13 03:30:46', '2016-05-13 03:30:46'),
(18, '2012000016', 1, 3, 2, '2016-05-13 03:30:46', '2016-05-13 03:30:46'),
(19, '2012000017', 1, 3, 2, '2016-05-13 03:30:46', '2016-05-13 03:30:46'),
(20, '2012000018', 1, 3, 2, '2016-05-13 03:30:46', '2016-05-13 03:30:46'),
(21, '2012000019', 1, 3, 2, '2016-05-13 03:30:47', '2016-05-13 03:30:47'),
(22, '2012000020', 1, 3, 2, '2016-05-13 03:30:47', '2016-05-13 03:30:47'),
(23, '2012000021', 1, 3, 2, '2016-05-13 03:30:47', '2016-05-13 03:30:47'),
(24, '2012000022', 1, 3, 2, '2016-05-13 03:30:47', '2016-05-13 03:30:47'),
(25, '2012000023', 1, 3, 2, '2016-05-13 03:30:47', '2016-05-13 03:30:47'),
(26, '2012000024', 1, 3, 2, '2016-05-13 03:30:48', '2016-05-13 03:30:48'),
(27, '2012000025', 1, 3, 2, '2016-05-13 03:30:48', '2016-05-13 03:30:48'),
(28, '2012000026', 1, 3, 2, '2016-05-13 03:30:48', '2016-05-13 03:30:48'),
(29, '2012000027', 1, 3, 2, '2016-05-13 03:30:48', '2016-05-13 03:30:48'),
(30, '2012000028', 1, 3, 2, '2016-05-13 03:30:48', '2016-05-13 03:30:48'),
(31, '2012000029', 1, 3, 2, '2016-05-13 03:30:48', '2016-05-13 03:30:48'),
(32, '2012000030', 1, 3, 2, '2016-05-13 03:30:49', '2016-05-13 03:30:49'),
(33, '2012000031', 2, 3, 3, '2016-05-13 03:34:33', '2016-05-13 03:34:33'),
(34, '2012000032', 2, 3, 3, '2016-05-13 03:34:33', '2016-05-13 03:34:33'),
(35, '2012000033', 2, 3, 3, '2016-05-13 03:34:34', '2016-05-13 03:34:34'),
(36, '2012000034', 2, 3, 3, '2016-05-13 03:34:34', '2016-05-13 03:34:34'),
(37, '2012000035', 2, 3, 3, '2016-05-13 03:34:34', '2016-05-13 03:34:34'),
(38, '2012000036', 2, 3, 3, '2016-05-13 03:34:34', '2016-05-13 03:34:34'),
(39, '2012000037', 2, 3, 3, '2016-05-13 03:34:34', '2016-05-13 03:34:34'),
(40, '2012000038', 2, 3, 3, '2016-05-13 03:34:35', '2016-05-13 03:34:35'),
(41, '2012000039', 2, 3, 3, '2016-05-13 03:34:35', '2016-05-13 03:34:35'),
(42, '2012000040', 2, 3, 3, '2016-05-13 03:34:35', '2016-05-13 03:34:35'),
(43, '2012000041', 2, 3, 3, '2016-05-13 03:34:36', '2016-05-13 03:34:36'),
(44, '2012000042', 2, 3, 3, '2016-05-13 03:34:36', '2016-05-13 03:34:36'),
(45, '2012000043', 2, 3, 3, '2016-05-13 03:34:37', '2016-05-13 03:34:37'),
(46, '2012000044', 2, 3, 3, '2016-05-13 03:34:37', '2016-05-13 03:34:37'),
(47, '2012000045', 2, 3, 3, '2016-05-13 03:34:37', '2016-05-13 03:34:37'),
(48, '2012000046', 2, 3, 3, '2016-05-13 03:34:37', '2016-05-13 03:34:37'),
(49, '2012000047', 2, 3, 3, '2016-05-13 03:34:37', '2016-05-13 03:34:37'),
(50, '2012000048', 2, 3, 3, '2016-05-13 03:34:38', '2016-05-13 03:34:38'),
(51, '2012000049', 2, 3, 3, '2016-05-13 03:34:38', '2016-05-13 03:34:38'),
(52, '2012000050', 2, 3, 3, '2016-05-13 03:34:38', '2016-05-13 03:34:38'),
(53, '2012000051', 2, 3, 3, '2016-05-13 03:34:38', '2016-05-13 03:34:38'),
(54, '2012000052', 2, 3, 3, '2016-05-13 03:34:38', '2016-05-13 03:34:38'),
(55, '2012000053', 2, 3, 3, '2016-05-13 03:34:39', '2016-05-13 03:34:39'),
(56, '2012000054', 2, 3, 3, '2016-05-13 03:34:39', '2016-05-13 03:34:39'),
(57, '2012000055', 2, 3, 3, '2016-05-13 03:34:39', '2016-05-13 03:34:39'),
(58, '2012000056', 2, 3, 3, '2016-05-13 03:34:39', '2016-05-13 03:34:39'),
(59, '2012000057', 2, 3, 3, '2016-05-13 03:34:39', '2016-05-13 03:34:39'),
(60, '2012000058', 2, 3, 3, '2016-05-13 03:34:40', '2016-05-13 03:34:40'),
(61, '2012000059', 2, 3, 3, '2016-05-13 03:34:40', '2016-05-13 03:34:40'),
(62, '2012000060', 2, 3, 3, '2016-05-13 03:34:40', '2016-05-13 03:34:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middlename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_first` tinyint(1) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `creation_mode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `code`, `firstname`, `middlename`, `lastname`, `email`, `birthday`, `gender`, `status`, `is_active`, `is_first`, `password`, `created_by`, `creation_mode`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '1234', 'Juan', 'Dela', 'Cruz', 'sample@student.com', '1993-12-03', 0, 'student', 1, 1, '$2y$10$7UjxxlTOGHoGiSTfePifpubaah2b6DLheEJ0YT83bTzOCA3tUTeuW', 0, '', 'akCWpvNtXfs5Fzqf1C3WGvAPFeBo8ZcbBqNwRvl6KnupkxS2WOLz9dzOOtFK', '2016-04-04 07:24:27', '2016-05-13 15:39:44'),
(2, '784512', 'Sir Juan', 'Dela', 'Cruz', 'sample@professor.com', '1993-06-16', 1, 'professor', 1, 1, '$2y$10$n4qQwwfzZBDWN/xn33vEheY6EJ7sy4JJCWkbX8qPV5wiLt8/cCkMO', 0, '', 'L9gDKsYnTyvChHyriLBNSXadBXxVr16KvkgbhJdigxskuChnWEFgUtqJ8Rf8', '2016-04-09 06:47:14', '2016-05-13 03:11:33'),
(3, '654322', 'Guidance Jun', 'Dela', 'Cruz', 'sample@guidance.com', '1994-03-09', 0, 'guidance', 1, 1, '$2y$10$wRXY71HeEPS08.HCrvO8hOvjH9Y2XZ4I989Dn0W5N5XcBsO3NgVPa', 0, '', 'gsjSsyknU7SpfmN2109uBvhcDlhJJOPuQKyGpvvJ9UckySkUKFwPAEOunDsO', '2016-04-10 09:37:23', '2016-10-09 05:30:48'),
(4, '2010000001', 'Lucila', 'O', 'Bance', 'lobance@ust.mygbiz.com', '1994-03-27', 0, 'director', 1, 1, '$2y$10$7jInoTchSNA2Iou5QICxDepVol6YRzBnTFjxqE3EdpZx9FK35ooue', 0, '', 'g4JwZejzdnrufQUW3IS3YVJA3mCW8YI9OUSunogHBrCGQxx26SJ8cL9Zo9zH', '2016-04-12 13:31:47', '2016-05-13 15:51:44'),
(10, '2012000001', 'Mark Alejandro Kevin', 'Telan', 'Abloa', '2012000001@ust-ics.mygbiz.com', '1994-04-13', 0, 'student', 1, 1, '$2y$10$iVtaUDGbTgVXySUx9EsjJO5oCPlx9dJb8sX.bWkgYfyMpfdCkNE8a', 90909090, 'Batch', 'VAUIPZS9B94VnFh6HzFYKiwK4ljj3SwqgMN8IIU6Tz6AN5Wqa4hVWLKMNNXc', '2016-05-13 03:30:43', '2016-10-09 05:26:31'),
(11, '2012000002', 'Francis', 'Manongsong', 'Agbnga', '2012000002@ust-ics.mygbiz.com', '1993-05-18', 1, 'student', 1, 1, '$2y$10$3PLKa9J9XRtu9.yVAgDsLO28HVdKBZeo6l1c7P4TrZSFMUCkQqh/O', 90909090, 'Batch', 'ZfvgS7ByTlCxfZfuPLqLD1XtsV2cCteEqok4GmfJD9ru20jjd75mXB7odvW6', '2016-05-13 03:30:43', '2016-05-13 05:59:40'),
(12, '2012000003', 'Isabela Kaye', 'Layacan', 'Agulaa', '2012000003@ust-ics.mygbiz.com', '1993-09-19', 1, 'student', 1, 1, '$2y$10$m/pvMShP9L2ZJqxjGb.The2ioD2HeUc1z5Pb0faMIfrp4Kg.U7tba', 90909090, 'Batch', 'M3PkjiN8EZAb2KUv6JAKzQxrNLWoji1qfjcCTcuQVY6wsuN6qxVjCOdjlkpx', '2016-05-13 03:30:43', '2016-05-13 16:13:30'),
(13, '2012000004', 'Martin Jego', 'Sales', 'Albrtoa', '2012000004@ust-ics.mygbiz.com', '1994-02-15', 0, 'student', 1, 1, '$2y$10$9YPfXfXef9BFnY1G5iHl3eDG4nBeYOkOsGUcNhc4lW/MmE9bZsP32', 90909090, 'Batch', 'jQyUCTicJIHFVXAK4CUEXLVpusgccrouvFLCfY8QXpmLi34hC75DAlTt2Usn', '2016-05-13 03:30:44', '2016-05-13 16:12:10'),
(14, '2012000005', 'Alphonnso', 'Ramon', 'Allrdea', '2012000005@ust-ics.mygbiz.com', '1994-02-21', 0, 'student', 1, 1, '$2y$10$afvP7cNMFHZlJdshZy1gSOTODUoj3fqPRnnRM1wXMCUauguDAMB8O', 90909090, 'Batch', NULL, '2016-05-13 03:30:44', '2016-05-13 03:30:44'),
(15, '2012000006', 'Anne Shanette', 'Concepcion', 'Anatacioa', '2012000006@ust-ics.mygbiz.com', '1994-01-05', 0, 'student', 1, 1, '$2y$10$Wf14N4k5Mmmd7N73RXDXh.Kk91McZAjQSHtAbzdfIcLrMd3oMMBja', 90909090, 'Batch', NULL, '2016-05-13 03:30:44', '2016-05-13 03:30:44'),
(16, '2012000007', 'Mark Angelo', 'Manalaysay', 'Ancanoa', '2012000007@ust-ics.mygbiz.com', '1994-04-21', 0, 'student', 1, 1, '$2y$10$vgRjr60.37OOXKJYVxcuzOEVdItOPaHn6eNEVqFaqhJzoA1cMkEUG', 90909090, 'Batch', NULL, '2016-05-13 03:30:44', '2016-05-13 03:30:44'),
(17, '2012000008', 'Charles Baron', 'Go', 'Angga', '2012000008@ust-ics.mygbiz.com', '1994-02-24', 0, 'student', 1, 1, '$2y$10$tP8C1FkWoK3iNOW4khjI7eAjL7V8JGQIWyHEobRa2vE0T8.V6vHe2', 90909090, 'Batch', NULL, '2016-05-13 03:30:44', '2016-05-13 03:30:44'),
(18, '2012000009', 'Peter Paul', 'Aguas', 'Aqunoa', '2012000009@ust-ics.mygbiz.com', '1993-07-08', 1, 'student', 1, 1, '$2y$10$c1cJq.d7Knhjnb8eyHcVBeBLRI7cW/smHfp3V0zibWAQCwkewRyWq', 90909090, 'Batch', NULL, '2016-05-13 03:30:45', '2016-05-13 03:30:45'),
(19, '2012000010', 'Albert Mari', 'Credo', 'Araona', '2012000010@ust-ics.mygbiz.com', '1993-10-19', 1, 'student', 1, 1, '$2y$10$fay9F7g6s8t8UnfBHfizg.V75SN5lQWCUFbGkMe3lg24XUQDlBN1y', 90909090, 'Batch', 'Sk4OwrD6uNmILXeiKAdNwYXUCsHE58gfK5tOBZ5AFeqkkB7vZFgWwPtt9PJb', '2016-05-13 03:30:45', '2016-05-13 10:16:37'),
(20, '2012000011', 'Khristiane Danielle', 'Balite', 'Arretaa', '2012000011@ust-ics.mygbiz.com', '1994-02-15', 0, 'student', 1, 1, '$2y$10$5alfHz5QHl08lfwHUlrDt.jocZn6Iku8yTfYK5r.GxqcSjA4ArM7C', 90909090, 'Batch', NULL, '2016-05-13 03:30:45', '2016-05-13 03:30:45'),
(21, '2012000012', 'Natasha Michaela', 'Guzon', 'Avea', '2012000012@ust-ics.mygbiz.com', '1993-11-23', 0, 'student', 1, 1, '$2y$10$yz1F25YX69K.5NNyUStea.KKksl5gbAPY78u/EA33e0AQph2dvskm', 90909090, 'Batch', NULL, '2016-05-13 03:30:45', '2016-05-13 03:30:45'),
(22, '2012000013', 'Benjamin John', 'Buniel', 'Aysna', '2012000013@ust-ics.mygbiz.com', '1993-10-10', 1, 'student', 1, 1, '$2y$10$Ont.cP61suxXVOFrjci/VuliNmqDMpH.8ZGv.Ekg0XLRCgObnemQG', 90909090, 'Batch', NULL, '2016-05-13 03:30:45', '2016-05-13 03:30:45'),
(23, '2012000014', 'Desiree Joy', 'Garcia', 'Bacnib', '2012000014@ust-ics.mygbiz.com', '1993-11-08', 0, 'student', 1, 1, '$2y$10$lJIj.P9ofo6.4pS/LIl6TOTMoKJsvKxwogyRNZGijxU1y5W/CZvP2', 90909090, 'Batch', '54T7KZMVI3SQ9SFFI7cCXShBmFHFwR2PBsXY9NoWjXv0u3I2aIYPWiE6kJm3', '2016-05-13 03:30:45', '2016-10-08 07:45:23'),
(24, '2012000015', 'Jade Mirel', 'Dioquino', 'Balloyb', '2012000015@ust-ics.mygbiz.com', '1993-08-17', 1, 'student', 1, 1, '$2y$10$Ku0.ceHHOZ3rEi1lTBdgPeBjrzZOGDmhZWC7lWcN2vTDfjDbMOR1a', 90909090, 'Batch', NULL, '2016-05-13 03:30:46', '2016-05-13 03:30:46'),
(25, '2012000016', 'Rosa Angela Mikaela', 'Ramos', 'Bat', '2012000016@ust-ics.mygbiz.com', '1994-03-29', 0, 'student', 1, 1, '$2y$10$x2R9OlzgpuYYXfUCcw3I/Oy7fZdKSVMfpB5uSj4wyNv5fyloreNb2', 90909090, 'Batch', NULL, '2016-05-13 03:30:46', '2016-05-13 03:30:46'),
(26, '2012000017', 'Leoncio Jr', 'Gimeno', 'Bauistab', '2012000017@ust-ics.mygbiz.com', '1993-12-30', 0, 'student', 1, 1, '$2y$10$Q2SSJJg222RUajHZnEuOAORy4XeXsqS0JpsRPEc9VXvhXU4816TKK', 90909090, 'Batch', NULL, '2016-05-13 03:30:46', '2016-05-13 03:30:46'),
(27, '2012000018', 'Anna Francheska', 'Diaz', 'Bueob', '2012000018@ust-ics.mygbiz.com', '1993-12-04', 0, 'student', 1, 1, '$2y$10$zRrU/lyQ/eMvY3khgnscr.OAptJNjeElJb0nnAbNktq9uYoi.Et02', 90909090, 'Batch', NULL, '2016-05-13 03:30:46', '2016-05-13 03:30:46'),
(28, '2012000019', 'Jerico', 'Acordon', 'Bugrinb', '2012000019@ust-ics.mygbiz.com', '1994-04-26', 0, 'student', 1, 1, '$2y$10$4fRjkfWgKblNwArw83RKOeWTMPG7oLi/lpDJwPGdYyVLaDyw0d6au', 90909090, 'Batch', NULL, '2016-05-13 03:30:46', '2016-05-13 03:30:46'),
(29, '2012000020', 'Rwoell Mikko', 'Quintana', 'Cablloc', '2012000020@ust-ics.mygbiz.com', '1993-06-25', 1, 'student', 1, 1, '$2y$10$klp.grJRftJru3bwkZetD.px5C6G5bj5OecL6GPt8AKlex2Fiudy2', 90909090, 'Batch', NULL, '2016-05-13 03:30:47', '2016-05-13 03:30:47'),
(30, '2012000021', 'Renz Andrew', 'Ala', 'Calwagc', '2012000021@ust-ics.mygbiz.com', '1994-02-17', 0, 'student', 1, 1, '$2y$10$dbEJFMLBfjhs2ZHLBnx8Dun5sbOrpkewMxdbBX0wSHYG8RiM4ruFi', 90909090, 'Batch', NULL, '2016-05-13 03:30:47', '2016-05-13 03:30:47'),
(31, '2012000022', 'Elaine', 'Delos Santos', 'Caplic', '2012000022@ust-ics.mygbiz.com', '1993-11-16', 0, 'student', 1, 1, '$2y$10$qyCu4h/9J8CTrlntTPIFNe05t6jn3YpRcn8.pG6toAc29EqGjuMNO', 90909090, 'Batch', NULL, '2016-05-13 03:30:47', '2016-05-13 03:30:47'),
(32, '2012000023', 'Paulo', 'Operario', 'Capstranoc', '2012000023@ust-ics.mygbiz.com', '1993-08-27', 1, 'student', 1, 1, '$2y$10$tmyiWw8XN0zoyNMAVweSXOKsApf549cJ5vonKoR1LVPnMFd6gMqCa', 90909090, 'Batch', NULL, '2016-05-13 03:30:47', '2016-05-13 03:30:47'),
(33, '2012000024', 'Jean Paul', 'Danico', 'Carndangc', '2012000024@ust-ics.mygbiz.com', '1994-04-29', 0, 'student', 1, 1, '$2y$10$GLGxffPdiZua9jcRWFd37OQKPYxE8UYFWRATKYQcXAb6DBf14dm4i', 90909090, 'Batch', NULL, '2016-05-13 03:30:47', '2016-05-13 03:30:47'),
(34, '2012000025', 'Andrea', 'Agbayani', 'Carnanc', '2012000025@ust-ics.mygbiz.com', '1993-04-30', 1, 'student', 1, 1, '$2y$10$Gt9Vtsq.cYArjyT.m.pryeO3tc95HPqYXT2qXqZDqIk5YaLiaD9mO', 90909090, 'Batch', 'mpNlbnjsQ1fh6ZYHS5JqFjrIkCHq87DyCG8mcAZ7ZW5DjnmLbFoN0c7y174o', '2016-05-13 03:30:48', '2016-05-13 04:18:38'),
(35, '2012000026', 'Paolo Marco', 'Malapitan', 'Carioc', '2012000026@ust-ics.mygbiz.com', '1993-05-04', 1, 'student', 1, 1, '$2y$10$wRAzZGSo6HfsEiPuVJUZzOSQH7b1DKCDxFSnZHsTdYi2EOE8PYGGe', 90909090, 'Batch', NULL, '2016-05-13 03:30:48', '2016-05-13 03:30:48'),
(36, '2012000027', 'Alain Shaun Mitchel', 'Quinto', 'Carizoc', '2012000027@ust-ics.mygbiz.com', '1993-05-22', 1, 'student', 1, 1, '$2y$10$v6A0fAeQQwOMhPoUNx6z.OiLL4Qd7CQDyonqhEaqSKmcoacPMCdkK', 90909090, 'Batch', NULL, '2016-05-13 03:30:48', '2016-05-13 03:30:48'),
(37, '2012000028', 'Geraldine', 'Del Castillo', 'Casa', '2012000028@ust-ics.mygbiz.com', '1993-08-04', 1, 'student', 1, 1, '$2y$10$5zrEsEFNnk9Xr20aGlk5Meoulw5ZXG.4JDZamD8iCuaG4qXq1MDQa', 90909090, 'Batch', NULL, '2016-05-13 03:30:48', '2016-05-13 03:30:48'),
(38, '2012000029', 'Bien Bernice', 'Orijuela', 'Cat', '2012000029@ust-ics.mygbiz.com', '1993-06-18', 1, 'student', 1, 1, '$2y$10$FHSSvIFrBR.K5V8QkNrNM.bhVtPvq0w7nI.9hSPDX7IvA/C78Y49q', 90909090, 'Batch', NULL, '2016-05-13 03:30:48', '2016-05-13 03:30:48'),
(39, '2012000030', 'Loren Abinezra', 'Castro', 'Asioa', '2012000030@ust-ics.mygbiz.com', '1994-03-19', 0, 'student', 1, 1, '$2y$10$S/hFGBQyOr./d7XFalmMoOBBG2G94EBym2wKL8.QZvlAFsb.rRAMO', 90909090, 'Batch', NULL, '2016-05-13 03:30:49', '2016-05-13 03:30:49'),
(40, '', '', '', '', '', '1993-05-10', 1, '', 0, 0, '$2y$10$el1QPCJTcnB/ZH1J4YokpOrxN8ITpCrPW3Bu8/RWO/.MI.l/uZiIS', 90909090, 'Batch', NULL, '2016-05-13 03:30:49', '2016-05-13 03:44:52'),
(41, '2012000031', 'Rizzia Pauline', 'Rodil', 'Abrantes', '2012000031@ust-ics.mygbiz.com', '1993-10-20', 1, 'student', 1, 1, '$2y$10$xJw.TEtUDjfLsNY8VskkQesSkGkt9oKQJOjKXyyeQmaOgTGghOUTG', 90909090, 'Batch', 'LFHqpFBuR2kHp4qnXWMucCWBXow2li7uNMWu1fS9yMAJPRJ0qisvjNClZsWO', '2016-05-13 03:34:33', '2016-05-13 04:17:16'),
(42, '2012000032', 'Mikael Jau Gregamo', 'Egbalic', 'Acu', '2012000032@ust-ics.mygbiz.com', '1993-08-12', 1, 'student', 1, 1, '$2y$10$42jJ3.ZOOyGE/fiYsNCSMekX/xiQbyBI7s2nWdeSv2jlgwqkS819y', 90909090, 'Batch', NULL, '2016-05-13 03:34:33', '2016-05-13 03:34:33'),
(43, '2012000033', 'Adrian Kim', 'Gumapac', 'Adame', '2012000033@ust-ics.mygbiz.com', '1993-05-02', 1, 'student', 1, 1, '$2y$10$5Qf6f75G1Bl4ziDUXp3lV.G2wkTQHGyRFBmGp09JPe4.i0iTxDbBW', 90909090, 'Batch', 'E3d9TL8KpqmE5UPvitkVhXdJIehAh14ytsHB9x1dPCCd06R4aKlurpVTb25R', '2016-05-13 03:34:34', '2016-05-13 10:15:44'),
(44, '2012000034', 'Paolo Raffaelle', 'Baltero', 'Agor', '2012000034@ust-ics.mygbiz.com', '1993-07-04', 1, 'student', 1, 1, '$2y$10$DV6uFSTfhIe9.OiZQ0Yv..GroeBPO6EDJrOjwOil0mhvlVLD37MJK', 90909090, 'Batch', NULL, '2016-05-13 03:34:34', '2016-05-13 03:34:34'),
(45, '2012000035', 'Rosslynn-Anne', 'Medina', 'Agulto', '2012000035@ust-ics.mygbiz.com', '1994-03-13', 0, 'student', 1, 1, '$2y$10$ikZNgiceXjPL0gUUIcdR4.G7.Z6MDA9LGfuvuRhncnfhALPXw.via', 90909090, 'Batch', 'gpDLcKuO6Ptndw69lTFLYLLFoVVXJbb2gWK6MQ49oqYWVyAj3NpXVQaycqdf', '2016-05-13 03:34:34', '2016-05-13 10:17:54'),
(46, '2012000036', 'Lance Paolo', 'Ramos', 'Aldeosa', '2012000036@ust-ics.mygbiz.com', '1994-02-18', 0, 'student', 1, 1, '$2y$10$7PCxYTWXAlD/6enVmjPDnOIZMLiWIOPla873mMOHjDIrTP.kwqf16', 90909090, 'Batch', NULL, '2016-05-13 03:34:34', '2016-05-13 03:34:34'),
(47, '2012000037', 'Leomar Amiel', 'Ting', 'Alejar', '2012000037@ust-ics.mygbiz.com', '1993-09-30', 1, 'student', 1, 1, '$2y$10$WGZ0zR6TEIxdLHn6G6pQIefEcn3wpmvQ1ggr5ei9DvbGWvx6ozLcW', 90909090, 'Batch', NULL, '2016-05-13 03:34:34', '2016-05-13 03:34:34'),
(48, '2012000038', 'Christian', 'Resos', 'Antioquia', '2012000038@ust-ics.mygbiz.com', '1994-01-05', 0, 'student', 1, 1, '$2y$10$j2e9a.KQ1W9GkYxrREHrD.o3AsjOcr6CC0ikV9gD.TqKyBEbYifyi', 90909090, 'Batch', NULL, '2016-05-13 03:34:35', '2016-05-13 03:34:35'),
(49, '2012000039', 'Lawrence Miguel', 'Gomez', 'Antonio', '2012000039@ust-ics.mygbiz.com', '1993-06-30', 1, 'student', 1, 1, '$2y$10$OjlHxrTYGDDzzRtcU4u9zedyvCpDyxnj0lMvTqDBuOACqd3IJgnVi', 90909090, 'Batch', NULL, '2016-05-13 03:34:35', '2016-05-13 03:34:35'),
(50, '2012000040', 'Kurt Louis', 'Tadifa', 'Aquino', '2012000040@ust-ics.mygbiz.com', '1994-02-10', 0, 'student', 1, 1, '$2y$10$smlmUTknUMZJybjmRWeG5e0WxhRFySC4CUmUxBrhemoPlyNSortZa', 90909090, 'Batch', NULL, '2016-05-13 03:34:35', '2016-05-13 03:34:35'),
(51, '2012000041', 'Kiara Maricon', 'Sta. Catalina', 'Arriola', '2012000041@ust-ics.mygbiz.com', '1993-09-26', 1, 'student', 1, 1, '$2y$10$YHZ7bmSTrLSew3KFu8/Wj.JXtmodXSoCU5SXreWueHEVCvaGyX.FO', 90909090, 'Batch', NULL, '2016-05-13 03:34:35', '2016-05-13 03:34:35'),
(52, '2012000042', 'Andre Lorenzo', 'Buenaventura', 'Ayaquil', '2012000042@ust-ics.mygbiz.com', '1994-01-10', 0, 'student', 1, 1, '$2y$10$HF6FnDaG14GnqnbLxgnileSrejjxxTcYpZnXsHkaNKRlSih7GAE9C', 90909090, 'Batch', NULL, '2016-05-13 03:34:36', '2016-05-13 03:34:36'),
(53, '2012000043', 'Paul Alecxis', 'Bardelosa', 'Banag', '2012000043@ust-ics.mygbiz.com', '1993-08-07', 1, 'student', 1, 1, '$2y$10$0uzE7JMy4Wb1tsFKIcUG0OIv27h2kk06TLg0qr1yWhao2Y./.AiMa', 90909090, 'Batch', NULL, '2016-05-13 03:34:36', '2016-05-13 03:34:36'),
(54, '2012000044', 'Urielle', 'Lucio', 'Banzon', '2012000044@ust-ics.mygbiz.com', '1993-07-31', 1, 'student', 1, 1, '$2y$10$mODaUrFxh3AH1qdQLzzLve3d/R3Gi/lpzJVbXht9BrJu1KiKHyFhu', 90909090, 'Batch', NULL, '2016-05-13 03:34:37', '2016-05-13 03:34:37'),
(55, '2012000045', 'Frances Anne', 'Coronado', 'Barroga', '2012000045@ust-ics.mygbiz.com', '1993-10-12', 1, 'student', 1, 1, '$2y$10$tKRYFRHXkLoNWHHlNRYiw.I2gHiFWGnzIOvbe/Qfkq9MP0MjLu83S', 90909090, 'Batch', NULL, '2016-05-13 03:34:37', '2016-05-13 03:34:37'),
(56, '2012000046', 'Jezreel Mae', 'Viado', 'Bien', '2012000046@ust-ics.mygbiz.com', '1993-10-29', 1, 'student', 1, 1, '$2y$10$U/tKbM.fZS7/OHSBXdv2y.eROEju2BH3T8F0KRfcqTqzbg2T2/WDO', 90909090, 'Batch', NULL, '2016-05-13 03:34:37', '2016-05-13 03:34:37'),
(57, '2012000047', 'Nico Miguel', 'Solomon', 'Blanco', '2012000047@ust-ics.mygbiz.com', '1993-06-20', 1, 'student', 1, 1, '$2y$10$Per8mwCjnkUXmKVQ726PZOLcjAtLuy5UOmTyPhkiRjUaxRI.1rZ0S', 90909090, 'Batch', NULL, '2016-05-13 03:34:37', '2016-05-13 03:34:37'),
(58, '2012000048', 'Christianne Sarah Abigail', 'Marcelino', 'Boydon', '2012000048@ust-ics.mygbiz.com', '1993-07-12', 1, 'student', 1, 1, '$2y$10$8FYvDcVfdW6V6gMzq3K12euPQCHRLuPNcc3ZUdnpaF8GX3n14N7ua', 90909090, 'Batch', NULL, '2016-05-13 03:34:38', '2016-05-13 03:34:38'),
(59, '2012000049', 'Bien Carlo', 'Alforque', 'Budomo', '2012000049@ust-ics.mygbiz.com', '1993-11-30', 0, 'student', 1, 1, '$2y$10$13pGKH6mDeSBp5X3nh5d8.9rOOPYf6xzhpQf5eLLNys/wdTB7EBR6', 90909090, 'Batch', NULL, '2016-05-13 03:34:38', '2016-05-13 03:34:38'),
(60, '2012000050', 'Reymond Joseph', 'Carig', 'Cabrera', '2012000050@ust-ics.mygbiz.com', '1993-08-30', 1, 'student', 1, 1, '$2y$10$Lr.tZR/xzmuWYVvuBH.W8egXVTt3uVjp8KHYsMpxZ7GzhIItYIb3S', 90909090, 'Batch', NULL, '2016-05-13 03:34:38', '2016-05-13 03:34:38'),
(61, '2012000051', 'Yasi', 'Dizon', 'Cai', '2012000051@ust-ics.mygbiz.com', '1994-03-27', 0, 'student', 1, 1, '$2y$10$dEVCAd7OcsylDCSZZAsykOHT0t.mfyfMgOeBQ/KQCRLJYEfxP4mSK', 90909090, 'Batch', NULL, '2016-05-13 03:34:38', '2016-05-13 03:34:38'),
(62, '2012000052', 'Mariano Ferdinand Iii', 'Pagarigan', 'Camarillo', '2012000052@ust-ics.mygbiz.com', '1993-11-10', 0, 'student', 1, 1, '$2y$10$csyysLngN8vrjZGC7NYITuonHyt70eLD6rjCVb.lh76H.MvTy/tI6', 90909090, 'Batch', NULL, '2016-05-13 03:34:38', '2016-05-13 03:34:38'),
(63, '2012000053', 'Manrick Edwin', 'Pascual', 'Capotolan', '2012000053@ust-ics.mygbiz.com', '1994-04-09', 0, 'student', 1, 1, '$2y$10$sWJHiiSdAK.BnAuWQGmqj.7aQT/84JnETmklwTjql1/DlUtj60yPq', 90909090, 'Batch', NULL, '2016-05-13 03:34:39', '2016-05-13 03:34:39'),
(64, '2012000054', 'Tristan Karl', 'Torres', 'Carpio', '2012000054@ust-ics.mygbiz.com', '1993-06-12', 1, 'student', 1, 1, '$2y$10$N.WlNnkOEXmNF11ZaM6ZRuVxE21dNln0L/Ls3dAq4.dfebdnxj5Yy', 90909090, 'Batch', 'drtel49MisMwiYoQyXabFiqz3tsWUAW3LKmiuW8NkqLGE5MPBAqCZKnTKvqH', '2016-05-13 03:34:39', '2016-05-13 04:17:51'),
(65, '2012000055', 'Martin Adrian', 'Castro', 'Casas', '2012000055@ust-ics.mygbiz.com', '1994-01-31', 0, 'student', 1, 1, '$2y$10$PxT3UD4gUhXJpArvYLPfSu61KdZj3OhNPjL52HKdkY5ScjjqXRslO', 90909090, 'Batch', NULL, '2016-05-13 03:34:39', '2016-05-13 03:34:39'),
(66, '2012000056', 'John Louigie', 'Rodriguez', 'Castillo', '2012000056@ust-ics.mygbiz.com', '1993-10-06', 1, 'student', 1, 1, '$2y$10$Xegjr64sH8OtdP1wxCnbiemk/97rog7dBRYiwpRdnDWv4aJMdeBce', 90909090, 'Batch', NULL, '2016-05-13 03:34:39', '2016-05-13 03:34:39'),
(67, '2012000057', 'Rapunzel Maripaz', 'Punzalan', 'Castro', '2012000057@ust-ics.mygbiz.com', '1994-03-24', 0, 'student', 1, 1, '$2y$10$wCSkUYzOTHy7Yjp4f3c.Zuo4XdKJxt1.L2lEStiN4FuQQXJDOcVIu', 90909090, 'Batch', NULL, '2016-05-13 03:34:39', '2016-05-13 03:34:39'),
(68, '2012000058', 'Juelv Roca', 'Ko', 'Cayago', '2012000058@ust-ics.mygbiz.com', '1993-07-10', 1, 'student', 1, 1, '$2y$10$7jNO910cQAHN27hYWCJpFOJ0deY.pC8Tx0OxeTapDl3QZTF2iUj1G', 90909090, 'Batch', NULL, '2016-05-13 03:34:40', '2016-05-13 03:34:40'),
(69, '2012000059', 'Alfred John Paul', 'Orijuela', 'Cayman', '2012000059@ust-ics.mygbiz.com', '1993-08-10', 1, 'student', 1, 1, '$2y$10$9GRzvzJG.xeYWOyJaJGfqObLz.4B50vPPW3u62uTeCAlPS2LIWPn2', 90909090, 'Batch', NULL, '2016-05-13 03:34:40', '2016-05-13 03:34:40'),
(70, '2012000060', 'Louise Megan', 'Castro', 'Chan', '2012000060@ust-ics.mygbiz.com', '1994-02-20', 0, 'student', 1, 1, '$2y$10$u0owDmRDbIsxF7gScz0ZLeTwmqYJgbwn03mzcMBMANPrNFNz.96C2', 90909090, 'Batch', NULL, '2016-05-13 03:34:40', '2016-05-13 03:34:40'),
(71, '2011000001', 'Claudine', 'S.', 'Chua', '2011000001@ust-ics.mygbiz.com', '1993-07-18', 1, 'guidance', 1, 1, '$2y$10$QSmZs2c3bl1AIxsr6ZT4quuyldtqzzb8hwn/QnFDltP2JtB/ySILG', 90909090, 'Batch', 'oyzfXfq3mGflSy2lIrSqMUV3SYGobn6feqTx6fW176Kld2x8ChHekkkMGGHI', '2016-05-13 03:44:47', '2016-05-13 10:16:19'),
(72, '2011000002', 'Via Katrina', 'G.', 'Portera', '2011000002@ust-ics.mygbiz.com', '1993-12-25', 0, 'guidance', 1, 1, '$2y$10$eezBZdoR1w8cGJa5ZhZSvO72lQFZDsYqbt8n5Zw4cLB5psNbazIYW', 90909090, 'Batch', NULL, '2016-05-13 03:44:47', '2016-05-13 03:44:47'),
(73, '2011000003', 'Kristin', 'B.', 'Baura', '2011000003@ust-eng.mygbiz.com', '1993-12-12', 0, 'guidance', 1, 1, '$2y$10$pMOekJdTkDcVgw2BEShZWO5QxUPjXA/xNf6cRDlKgoKWXw05FhAxO', 90909090, 'Batch', 'nSTvQZqhnQGGf9PRz1DGaYdO1YyyFGWRcc6qoWJwJIj2wNedygE9GVLW7BLO', '2016-05-13 03:44:47', '2016-05-13 10:08:20'),
(74, '2011000004', 'Khristine Lorraine', 'C.', 'Lim', '2011000004@ust-eng.mygbiz.com', '1993-06-19', 1, 'guidance', 1, 1, '$2y$10$qkveTpvI42E.0t5rqBx8YeHGrOh9FU2hnTf.kJ5UEHgCcLiU9UUQ2', 90909090, 'Batch', NULL, '2016-05-13 03:44:48', '2016-05-13 03:44:48'),
(75, '2011000005', 'Diovie', 'C.', 'Navarra', '2011000005@ust-eng.mygbiz.com', '1994-02-23', 0, 'guidance', 1, 1, '$2y$10$ChOEARDQaP4tTkMIbpJNwuu8XOne7iJ/qMJ2qxUEPZqO3/2.hOvV2', 90909090, 'Batch', NULL, '2016-05-13 03:44:48', '2016-05-13 03:44:48'),
(76, '2011000006', 'Marissa', 'S.', 'Nicasio', '2011000006@ust-eng.mygbiz.com', '1994-01-07', 0, 'guidance', 1, 1, '$2y$10$aDc8hoEDIjQdiDYKdMwL3OLn5xrlidUBXh.fb5UuKnn37AHQN6nV.', 90909090, 'Batch', NULL, '2016-05-13 03:44:48', '2016-05-13 03:44:48'),
(81, '2009000001', 'Mildred', 'C.', 'Duran', '2009000001@ust-ics.mygbiz.com', '1994-04-26', 0, 'professor', 1, 1, '$2y$10$DrenAe1mmFbz/uCIykX9ROb3Pem6JP9P1d1Qwc/IoUhVhxVt6xUC6', 2010000001, 'Batch', '44O4ihCV6K6fCoufi1PrLWclSKt2rg2F8EbtA2kUnZQwdz2z5DlpjmHyiHBO', '2016-05-13 05:22:30', '2016-05-13 10:08:58'),
(82, '2009000002', 'Divina Gracia', 'L.', 'Mariano', '2009000002@ust-ics.mygbiz.com', '1994-03-18', 0, 'professor', 1, 1, '$2y$10$KKCo2nSNoVb30jlnVuCFNOSvz6MVKQsfYkTfPlqHGf6dLuO.cLaYi', 2010000001, 'Batch', NULL, '2016-05-13 05:22:30', '2016-05-13 05:23:49'),
(83, '2009000003', 'Christopher', 'O.', 'Ladao', '2009000003@ust-ics.mygbiz.com', '1993-10-07', 1, 'professor', 1, 1, '$2y$10$HJrmqfimotm5XihtoFPhQOyTpGsQ1HdFuhhBBvAQ3dEcd8ITjUFf.', 2010000001, 'Batch', NULL, '2016-05-13 05:23:50', '2016-05-13 05:23:50'),
(84, '1111111111', 'Admin', 'admin', 'Admin', 'admin@admin.com', '1993-11-17', 0, 'admin', 1, 1, '$2y$10$n4qQwwfzZBDWN/xn33vEheY6EJ7sy4JJCWkbX8qPV5wiLt8/cCkMO', 11111, 'single', NULL, '2016-05-23 13:26:59', '2016-05-23 13:26:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement_flag`
--
ALTER TABLE `announcement_flag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement_permission`
--
ALTER TABLE `announcement_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colleges`
--
ALTER TABLE `colleges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cumulative_record`
--
ALTER TABLE `cumulative_record`
  ADD PRIMARY KEY (`cum_id`);

--
-- Indexes for table `cumulative_record_additional`
--
ALTER TABLE `cumulative_record_additional`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `director`
--
ALTER TABLE `director`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbackchat`
--
ALTER TABLE `feedbackchat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guidance_councelor`
--
ALTER TABLE `guidance_councelor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inbox_reply`
--
ALTER TABLE `inbox_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `non_compliance_notification`
--
ALTER TABLE `non_compliance_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_badge`
--
ALTER TABLE `notification_badge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `professor`
--
ALTER TABLE `professor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral`
--
ALTER TABLE `referral`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_code_unique` (`code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `announcement_flag`
--
ALTER TABLE `announcement_flag`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `announcement_permission`
--
ALTER TABLE `announcement_permission`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `colleges`
--
ALTER TABLE `colleges`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cumulative_record`
--
ALTER TABLE `cumulative_record`
  MODIFY `cum_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `cumulative_record_additional`
--
ALTER TABLE `cumulative_record_additional`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `director`
--
ALTER TABLE `director`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `feedbackchat`
--
ALTER TABLE `feedbackchat`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `guidance_councelor`
--
ALTER TABLE `guidance_councelor`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `inbox`
--
ALTER TABLE `inbox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inbox_reply`
--
ALTER TABLE `inbox_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `non_compliance_notification`
--
ALTER TABLE `non_compliance_notification`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification_badge`
--
ALTER TABLE `notification_badge`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `professor`
--
ALTER TABLE `professor`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `referral`
--
ALTER TABLE `referral`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=85;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
