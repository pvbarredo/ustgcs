--get all guidance 
SELECT u.code, u.email, gc.college_id, col.name
FROM USERS u
INNER JOIN GUIDANCE_COUNCELOR gc
INNER JOIN COLLEGES col
WHERE u.CODE = gc.CODE
AND u.STATUS = 'guidance'
AND col.id = gc.college_id